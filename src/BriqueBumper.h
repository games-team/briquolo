/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _BRIQUEBUMPER
#define _BRIQUEBUMPER

#include <MOGL_Objet.h>

#include <MOGL_GestionnaireTexture.h>
#include "ElementTableau.h"

class BriqueBumper: public ElementTableau
{
  protected:
    static MOGL_Objet * _ObjetSource;

  protected:
    float _Z, _VitesseZ;
    bool _Right;
    bool _Bumper;
    float _Rotation;

  protected:
    void _AnalyserLigne();

  public:
    BriqueBumper(TableauBase * p_TableauBase);
    BriqueBumper(const BriqueBumper & p_BriqueBumper);
    ~BriqueBumper();

    Generateur * CreerGenerateurParticules();

    void Maj();
    void InitialiserPosition();
    void SetPosition(double p_X, double p_Y);

    Struct_Collision TesterCollision(Balle * p_Balle);
    bool Toucher(Balle * p_Balle, double p_Temps);

    static void PreCharger();

};

#endif
