/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _BALLE
#define _BALLE

#include "ElementCollision.h"
#include <MOGL_Objet.h>
#include <MOGL_GestionnaireTexture.h>
#include <MOGL_MatriceTransformation.h>
#include <MOGL_Temporisation.h>

class TraceBalle;

class Balle: public ElementCollision, public MOGL_Action
{
  protected:
    static MOGL_Objet * _ObjetSource;
    TraceBalle * _TraceBalle;
    double _Vitesse;
    double _Rayon;
    double _FacteurTaille;
    const double _RayonInit;
    MOGL_MatriceTransformation _MatriceRotation;
    bool _SuperPuissance;
    MOGL_Temporisation _TempoSuperPuissance;
    int _CanalSonSuperPuissance;
    double _TempsMax;
    double _DirectionX;
    double _DirectionY;
    double _FacteurAcceleration;

  protected: //ACTIONS
    void _OnFinSuperPuissance();

  public:

    int val;

    Balle();
    Balle(const Balle & p_Balle);
    
    virtual ~Balle();

    //void PreparerDeplacement(double p_Facteur);
    void Deplacer(double p_Temps);
    void SetDirection(double p_DirectionX, double p_DirectionY);
    double GetDirectionX() const;
    double GetDirectionY() const;

    void SetVitesseTotale(double p_VitesseX, double p_VitesseY);
    double GetVitesseTotaleX() const;
    double GetVitesseTotaleY() const;

    double GetVitesseBase() const;
    void SetVitesseBase(double p_Vitesse);

    double GetVitesseTotale() const;
    void SetVitesseTotale(double p_Vitesse);

    double GetRayon() const;

    void SetPosition(double p_X, double p_Y);    
    void SetTaille(double p_Facteur);

    bool GetSuperPuissance() const;
    void SetSuperPuissance(bool p_Actif);

    double GetTempsMax() const;
    void SetTempsMax(double p_TempsMax);

    void MajVitesse();

    static void PreCharger(MOGL_GestionnaireTexture * p_GestionnaireTexture);

    void AddAcceleration(double p_Facteur);
    void SetFacteurAcceleration(double p_Facteur);
    double GetFacteurAcceleration() const;

    Struct_Collision TesterCollision(Balle * p_Balle);
};

#endif
