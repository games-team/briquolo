/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _ELEMENTGRAPHIQUE
#define _ELEMENTGRAPHIQUE

#include <MOGL_ElementArbre.h>
#include <MOGL_GestionnaireTexture.h>
#include "Texture.h"
#include "Son.h"


class ElementGraphique
{
  protected:
    double _X, _Y;
    MOGL_ElementArbre * _ElementArbre;

  public:
    ElementGraphique();
    ElementGraphique(const ElementGraphique & p_ElementGraphique);
    virtual ~ElementGraphique();
    
    void SetPosition(double p_X, double p_Y);
    double GetPositionX() const;
    double GetPositionY() const;

    MOGL_ElementArbre * GetElementArbreMOGL() const;
    
};

#endif
