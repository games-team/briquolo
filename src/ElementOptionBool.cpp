/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ElementOptionBool.h"
#include <iostream>

ElementOptionBool::ElementOptionBool(const string & p_Nom, bool p_ParametreDefaut)
  : ElementOption(p_Nom), _Parametre(p_ParametreDefaut)
{
}

bool ElementOptionBool::TraiterLigneFichier(const string & pLigne)
{
  string st = GetDroiteLigne(pLigne);
  if (st != "")
  {
    _Parametre = (st == "1");
    return true;
  }
  else
  {
    return false;
  }
}

string ElementOptionBool::GenererLigneFichier()
{
  string res(_Nom);
  res += ' ';
  if (_Parametre)
  {
    res += "1";
  }
  else
  {
    res += "0";
  }
  return res;
}


bool ElementOptionBool::GetValeur() const
{
  return _Parametre;
}

void ElementOptionBool::SetValeur(bool p_Valeur)
{
  _Parametre=p_Valeur;
}
