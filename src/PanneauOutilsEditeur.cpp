/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "PanneauOutilsEditeur.h"
#include "Gestionnaire.h"
#include "Constante.h"

PanneauOutilsEditeur::PanneauOutilsEditeur(MOGL_Fenetre * p_Fenetre): MOGL_Panneau(p_Fenetre, _("Tools"), 200, 110),
                                                                      _Police(10), _Pause(false)
{
  _AlignementHorizontal=MAX;
  _AlignementVertical=MIN;

  _Police.Charger((Constante::GetGlobalDir()+Constante::GetDataDir()+_("/DejaVuSans.ttf")).c_str());


  SetPolice(&_Police);

  _ChoixAction=new MOGL_Choix(this, _("Action"));
  _ChoixAction->SetPolice(&_Police);
  _ChoixAction->AjouterElementChoix(_("Add"));
  _ChoixAction->AjouterElementChoix(_("Edit/Delete"));

  _ChoixElement=new MOGL_Choix(this, _("Element"));
  _ChoixElement->SetPolice(&_Police);
  for(unsigned int i=0; i<Gestionnaire::GetNombreElementTableau(); i++)
  {
    _ChoixElement->AjouterElementChoix(Gestionnaire::GetNomElementTableau(i));
  }

  _ChoixVariation=new MOGL_Choix(this, _("Variation"));
  _ChoixVariation->SetPolice(&_Police);
  _ChoixVariation->ChangementElement.Connecter(this, &PanneauOutilsEditeur::_OnChangementVariation);
  _ChoixVariation->AjouterElementChoix(_("Custom"));
  _ChoixVariation->AjouterElementChoix(_("Brick"));
  _ChoixVariation->AjouterElementChoix(_("Small brick"));

  _VariationX=new MOGL_ChoixNombre(this, _("  X variation"), 0.1f, 0.05f);
  _VariationX->SetPolice(&_Police);

  _VariationY=new MOGL_ChoixNombre(this, _("  Y variation"), 0.1f, 0.05f);
  _VariationY->SetPolice(&_Police);

  _LabelSup = new MOGL_Label(this, STACKED_BRICK);
  _LabelSup->SetPolice(&_Police);
  _LabelSup->SetCacher(true);

  _ChoixVariation->SetIndiceCourant(1);

  SetUtiliserEscape(false);

  p_Fenetre->ChangementMode.Connecter(this, &PanneauOutilsEditeur::_OnChangementResolution);
}

float PanneauOutilsEditeur::GetVariationX() const
{
  if (_ChoixVariation->GetIndiceCourant()==0)
  {
    return _VariationX->GetNombre();
  }
  else
  {
    return _VarX;
  }
}

float PanneauOutilsEditeur::GetVariationY() const
{
  if (_ChoixVariation->GetIndiceCourant()==0)
  {
    return _VariationY->GetNombre();
  }
  else
  {
    return _VarY;
  }
}

unsigned int PanneauOutilsEditeur::GetIdentifiantElement()
{
  return _ChoixElement->GetIndiceCourant();
}

void PanneauOutilsEditeur::SetPause(bool p_Pause)
{
  _Pause=p_Pause;
}

bool PanneauOutilsEditeur::GetPause() const
{
  return _Pause;
}

void PanneauOutilsEditeur::ClavierUp(SDL_keysym * key)
{
  if (!_Pause)
  {
    MOGL_Panneau::ClavierUp(key);
  }
}

void PanneauOutilsEditeur::_OnChangementResolution(int p_ResolutionX, int p_ResolutionY, int p_BitParPixel)
{
  _Police.Recharger();
}


void PanneauOutilsEditeur::_OnChangementVariation(unsigned int p_Indice)
{
  if (p_Indice==0)
  {
    _VariationX->SetCacher(false);
    _VariationY->SetCacher(false);
  }
  else
  {
    _VariationX->SetCacher(true);
    _VariationY->SetCacher(true);
  }
  switch(p_Indice)
  {
    case 1:
    {
      _VarX=3.5;
      _VarY=2.5;
      break;
    }
    case 2:
    {
      _VarX=1.75;
      _VarY=1.25;
      break;
    }
  }
}
