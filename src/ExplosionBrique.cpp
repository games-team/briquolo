/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ExplosionBrique.h"
#include "MOGL_Temps.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif


ExplosionBrique::ExplosionBrique(ElementTableau * p_Element)
: Generateur(50, false, Texture::GetGestionnaireTexture()->GetTexture("MorceauBrique"))
{
  _Couleur.r=1;
  _Couleur.g=.5f;
  _Couleur.b=.1f;

  Point p=p_Element->GetVisualCenter();
  _X=p.x;
  _Y=p.y;

  CreerParticules(50);

  _Taille=3;
  _DureeVie=0.3f;
  _Vitesse=15;
}

ExplosionBrique::ExplosionBrique(ElementTableau * p_Element,
                                 const MOGL_Struct_Couleur & p_Couleur, float p_Taille, float p_DureeVie, float p_Vitesse)
  : Generateur(50, false, Texture::GetGestionnaireTexture()->GetTexture("MorceauBrique")),
    _Taille(p_Taille), _DureeVie(p_DureeVie), _Vitesse(p_Vitesse), _Couleur(p_Couleur)
{
  _X=p_Element->GetPositionX();
  _Y=p_Element->GetPositionY();
  CreerParticules(50);
}

void ExplosionBrique::CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                         MOGL_Struct_Vecteur & Velocite,
                                         MOGL_Struct_Couleur & Couleur,
                                         GLfloat & DureeVie,
                                         GLfloat & Taille)
{
  Position.x=_X;
  Position.y=_Y;
  Position.z=0;

  float angle=Random()*M_PI*2;
  float angle2=RandomNeg()*M_PI;

  Velocite.x=_Vitesse*cos(angle)*cos(angle2);
  Velocite.y=_Vitesse*sin(angle)*cos(angle2);
  Velocite.z=_Vitesse*sin(angle2);

  Couleur=_Couleur;

  DureeVie=_DureeVie*Random();
  
  Taille=_Taille;
}

void ExplosionBrique::MajParticule(MOGL_Struct_Vecteur & p_Position,
                                   MOGL_Struct_Vecteur & p_Velocite,
                                   MOGL_Struct_Couleur & p_Couleur)
{
  MOGL_Struct_Vecteur vec=MOGL_Temps::GetVariationVecteurDistance(p_Velocite);
  
  p_Position.x+=vec.x;
  p_Position.y+=vec.y;
  p_Position.z+=vec.z;
}
