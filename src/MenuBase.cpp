/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MenuBase.h"
#include "I18n.h"

MenuBase::MenuBase(MOGL_Fenetre * p_Fenetre): MOGL_Panneau(p_Fenetre, _("Options"), 300, 220), _DemandeQuitter(false)
{
  MOGL_Struct_Couleur coulTitre;
  coulTitre.r=0.84f;
  coulTitre.g=0.88f;
  coulTitre.b=1.0f;
  coulTitre.a=1;

  MOGL_Struct_Couleur coulFond;
  coulFond.r=0.0f;
  coulFond.g=0.0f;
  coulFond.b=.4f;
  coulFond.a=.6f;

  SetCouleurLabel(coulTitre);
  SetCouleurFond(coulFond);
}


void MenuBase::SetActivation(bool p_Activation)
{
  MOGL_Panneau::SetActivation(p_Activation);
  if (p_Activation!=_Activation && p_Activation==false)
  {
    FinMenuBase.Emettre();
  }
}

bool MenuBase::GetQuitter() const
{
  return _DemandeQuitter;
}
