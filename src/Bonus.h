/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _BONUS
#define _BONUS

#include "ElementGraphique.h"
#include "Plateau.h"

#include <MOGL_MatriceTransformation.h>

class TableauJeu;

class Bonus: public ElementGraphique
{
  protected:
    float _TailleX, _TailleY;
    float _VitesseY;

    MOGL_MatriceTransformation _MatriceRotation;

  public:
    Bonus();
    Bonus(const Bonus & p_Bonus);
    
    virtual bool Mouvement(Plateau * p_Plateau, TableauJeu * p_Tableau)=0;
    void SetPosition(float p_X, float p_Y);
    float GetTailleX() const;
    float GetTailleY() const;
    
    bool CollisionPlateau(Plateau * p_Plateau);
};

#endif
