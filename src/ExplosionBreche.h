/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _EXPLOSIONBRECHE
#define _EXPLOSIONBRECHE

#include "BriqueBase.h"
#include <MOGL_GestionnaireTexture.h>
#include "Generateur.h"

class ExplosionBreche: public Generateur
{
  protected:
    float _X, _Y;

  protected:
    void CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                            MOGL_Struct_Vecteur & Velocite,
                            MOGL_Struct_Couleur & Couleur,
                            GLfloat & DureeVie,
                            GLfloat & Taille);

    void MajParticule(  MOGL_Struct_Vecteur & p_Position,
                        MOGL_Struct_Vecteur & p_Velocite,
                        MOGL_Struct_Couleur & p_Couleur);

                

  public:
    ExplosionBreche(BriqueBase * p_Brique);
};


#endif
