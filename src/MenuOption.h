/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _MENUOPTION
#define _MENUOPTION

#include <MOGL_Panneau.h>
#include <MOGL_Choix.h>
#include <MOGL_Bouton.h>
#include <MOGL_Separateur.h>
#include <MOGL_ChoixNombre.h>

class MenuOption: public MOGL_Panneau
{
  protected:
    MOGL_Panneau * _PanneauVideo;
    MOGL_Panneau * _PanneauCommande;
    MOGL_Choix * _Resolution;
    MOGL_Choix * _Fullscreen;
    MOGL_Choix * _ChoixAfficherFPS;
    MOGL_Separateur * _SepVideo;
    MOGL_Separateur * _SepCommande;
    MOGL_Separateur * _SepOpt1;
    MOGL_Bouton * _RetourOption;
    MOGL_Bouton * _OkVideo;
    MOGL_Bouton * _AnnulerVideo;
    MOGL_Bouton * _OkCommande;
    MOGL_Bouton * _AnnulerCommande;
    MOGL_Bouton * _Video;
    MOGL_Bouton * _Commande;
    MOGL_Choix * _ChoixLimiterFPS;
    MOGL_ChoixNombre * _NombreFPSMax;
    MOGL_ChoixNombre * _SensibiliteClavier;
    MOGL_ChoixNombre * _SensibiliteSouris;
    MOGL_Fenetre::MOGL_Set_Resolution _SetRes;

  protected:
    void _InitialiserVideo();
    void _InitialiserCommande();

  protected: // ACTIONS
    void _OnVideo();
    void _OnCommande();
    void _OnOkVideo();
    void _OnAnnulerVideo();
    void _OnOkCommande();
    void _OnAnnulerCommande();
    void _OnRetourOption();
    void _OnChangementLimiterFPS(unsigned int p_Indice);

  public:
    MenuOption(MOGL_Panneau * p_Panneau);
    ~MenuOption();

    void SetActivation(bool p_Activation);
};

#endif
