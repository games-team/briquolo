/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifdef WIN32
#undef HAVE_CONFIG_H
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#else
#define PACKAGE "briquolo"
#endif

#include "CasseBrique.h"
#include "MOGL_Structure.h"
#include "Constante.h"
//#include <unistd.h>
#include "I18n.h"

int main(int argc, char* argv[])
{
  DefinirLanguage();

  CasseBrique t;
  t.main();

  return 0;
}

