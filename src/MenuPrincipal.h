/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _MENUPRINCIPAL
#define _MENUPRINCIPAL

#include <MOGL_Panneau.h>
#include "MenuOption.h"
#include <MOGL_Choix.h>
#include <MOGL_Bouton.h>

class MenuPrincipal: public MOGL_Panneau
{
  public:
    MOGL_Bouton * BoutonJeu;
    MOGL_Bouton * BoutonEditeur;
    MOGL_Bouton * BoutonOptions;
    MOGL_Bouton * BoutonQuitter;
    MenuOption * PanneauOption;

    MOGL_Panneau * PanneauChoixTableau;
    MOGL_Choix * ChoixGroupe;
    MOGL_Choix * ChoixTableau;
    MOGL_Separateur * Sep;
    MOGL_Bouton * Ok;
    MOGL_Bouton * Annuler;

  protected:
    void _OnOptions();
    void _OnChoisirTableauDepart();
    void _OnAnnuler();
    void _OnChangementGroupe(unsigned int p_NumGrp);
  public:
    MenuPrincipal(MOGL_Fenetre * p_Fentre);
    ~MenuPrincipal();

    void SetActivation(bool p_Activation);
};

#endif
