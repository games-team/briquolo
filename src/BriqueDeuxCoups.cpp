/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BriqueDeuxCoups.h"
#include "ExplosionBrique.h"
#include "ExplosionBreche.h"
#include "Constante.h"
#include <MOGL_GestionnaireObjet.h>
#include <MOGL_Noeud.h>
#include <iostream>
#include "BibliothequeSon.h"

MOGL_Objet * BriqueDeuxCoups::_ObjetSource=NULL;
MOGL_Objet * BriqueDeuxCoups::_ObjetSourceBreche=NULL;

BriqueDeuxCoups::BriqueDeuxCoups(TableauBase * p_TableauBase): BriqueBase(p_TableauBase), _NombreTouche(0)
{
  if (_ObjetSource==NULL)
  {
    PreCharger();
  }
  _ElementArbre=new MOGL_Noeud();
  _Objet=new MOGL_Objet(*_ObjetSource);
  static_cast<MOGL_Noeud *>(_ElementArbre)->AjouterElement(_Objet);
}

BriqueDeuxCoups::BriqueDeuxCoups(const BriqueDeuxCoups & p_BriqueDeuxCoups): BriqueBase(p_BriqueDeuxCoups)
{
  _ElementArbre=new MOGL_Noeud();
  _Objet=new MOGL_Objet(*_ObjetSource);
  static_cast<MOGL_Noeud *>(_ElementArbre)->AjouterElement(_Objet);
}

BriqueDeuxCoups::~BriqueDeuxCoups()
{
//  static_cast<MOGL_Noeud *>(_ElementArbre)->RetirerElement(_Objet);
  delete _Objet;
  delete _ElementArbre;
}

void BriqueDeuxCoups::PreCharger()
{
  _ObjetSource=new MOGL_Objet;
  _ObjetSourceBreche=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_breche.tri").c_str(),
                                                  *Texture::GetGestionnaireTexture(), *_ObjetSourceBreche);

  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_deuxcoups.tri").c_str(),
                                                  *Texture::GetGestionnaireTexture(), *_ObjetSource);

}

Struct_Collision BriqueDeuxCoups::TesterCollision(Balle * p_Balle)
{
  Struct_Collision col=BriqueBase::TesterCollision(p_Balle);
  return col;
}

Generateur * BriqueDeuxCoups::CreerGenerateurParticules()
{
  if (_NombreTouche<2)
  {
    return new ExplosionBreche(this);
  }
  else
  {
    return new ExplosionBrique(this);
  }
}

bool BriqueDeuxCoups::Toucher(Balle * p_Balle, double p_Temps)
{
  static_cast<MOGL_Noeud *>(_ElementArbre)->RetirerElement(_Objet);
  delete _Objet;
  _Objet=new MOGL_Objet(*_ObjetSourceBreche);
  static_cast<MOGL_Noeud *>(_ElementArbre)->AjouterElement(_Objet);

  _NombreTouche++;

  if (_NombreTouche<2)
  {
    BibliothequeSon::SonBreche->Jouer(false);
    return false;
  }
  else
  {
    BibliothequeSon::SonExplosion->Jouer(false);
    return true;
  }
}

