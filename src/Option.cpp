/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Option.h"
#include "Constante.h"
#include <fstream>
#include <iostream>

using namespace std;

ElementOptionInt Option::ResolutionX("ResolutionX", 800);
ElementOptionInt Option::ResolutionY("ResolutionY", 600);
ElementOptionBool Option::Fullscreen("Fullscreen", false);
ElementOptionBool Option::AfficherFPS("FPS", false);
ElementOptionInt Option::MaxFPS("MaxFPS", 0);
ElementOptionInt Option::VitesseClavier("KeyboardSpeed", 5);
ElementOptionInt Option::VitesseSouris("MouseSpeed", 5);
FichierConfig Option::_FichierConfig;
bool Option::_Init(true);

void Option::_InitOption()
{
  string chem(Constante::GetLocalDir());
  chem+='/';
  chem+=Constante::GetNomConfig();
  _FichierConfig.SetNomFichier(chem);

  _FichierConfig.AddElementOption(&ResolutionX);
  _FichierConfig.AddElementOption(&ResolutionY);
  _FichierConfig.AddElementOption(&Fullscreen);
  _FichierConfig.AddElementOption(&AfficherFPS);
  _FichierConfig.AddElementOption(&MaxFPS);
  _FichierConfig.AddElementOption(&VitesseClavier);
  _FichierConfig.AddElementOption(& VitesseSouris);
  _Init = false;
}

bool Option::Sauvegarder()
{
  if (_Init)
  {
    _InitOption();
  }
  return _FichierConfig.Enregistrer();
}

bool Option::Charger()
{
    bool bRet;
    if (_Init)
    {
        _InitOption();
    }
    bRet = _FichierConfig.Charger();
    if (!bRet)
    {
        cout << "Default options will be used" << endl;
    }
    return bRet;
}
