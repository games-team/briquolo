/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ExplosionBriqueSouffle.h"
#include "MOGL_Temps.h"
#include "TableauJeu.h"

#define VIBRATION_X   1
#define VIBRATION_Y   1

#ifndef M_PI
#define M_PI 3.14159265359
#endif


ExplosionBriqueSouffle::ExplosionBriqueSouffle(BriqueBase * p_Brique, float p_RayonSouffle)
  : Generateur(50, false, Texture::GetGestionnaireTexture()->GetTexture("MorceauBrique")),
    _GenerateurFini(false), _TempoFini(false),
    _Tempo(0.3f), _RayonSouffle(p_RayonSouffle)
{
  _Couleur.r=1;
  _Couleur.g=.5f;
  _Couleur.b=.1f;

  Point p = p_Brique->GetVisualCenter();
  _X=p.x;
  _Y=p.y;

  _Tableau=static_cast<TableauJeu *>(p_Brique->GetTableau());
  _Tableau->SetVibration(VIBRATION_X, VIBRATION_Y);

  CreerParticules(50);

  _Taille=3;
  _DureeVie=0.3f;
  _Vitesse=15;

  // On fabrique la temporisation qui est utilis�e avant de d�truire les objets qu'il y a autour
  AjouterElement(&_Tempo);
  _Tempo.FinTemporisation.Connecter(this, &ExplosionBriqueSouffle::_FinTempo);
}

ExplosionBriqueSouffle::ExplosionBriqueSouffle(BriqueBase * p_Brique,
                                               const MOGL_Struct_Couleur & p_Couleur, float p_Taille,
                                               float p_DureeVie, float p_Vitesse,
                                               float p_RayonSouffle)
  : Generateur(50, false, Texture::GetGestionnaireTexture()->GetTexture("MorceauBrique")),
    _Taille(p_Taille), _DureeVie(p_DureeVie), _Vitesse(p_Vitesse), _Couleur(p_Couleur),
    _GenerateurFini(false), _TempoFini(false),
    _Tempo(0.3f), _RayonSouffle(p_RayonSouffle)
{
  _X=p_Brique->GetPositionX();
  _Y=p_Brique->GetPositionY();
  CreerParticules(50);

  _Tableau=static_cast<TableauJeu *>(p_Brique->GetTableau());
  _Tableau->SetVibration(VIBRATION_X, VIBRATION_Y);

  // On fabrique la temporisation qui est utilis�e avant de d�truire les objets qu'il y a autour
  AjouterElement(&_Tempo);
  _Tempo.FinTemporisation.Connecter(this, &ExplosionBriqueSouffle::_FinTempo);
}

void ExplosionBriqueSouffle::CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                         MOGL_Struct_Vecteur & Velocite,
                                         MOGL_Struct_Couleur & Couleur,
                                         GLfloat & DureeVie,
                                         GLfloat & Taille)
{
  Position.x=_X;
  Position.y=_Y;
  Position.z=0;

  float angle=Random()*M_PI*2;
  float angle2=RandomNeg()*M_PI;

  Velocite.x=_Vitesse*cos(angle)*cos(angle2);
  Velocite.y=_Vitesse*sin(angle)*cos(angle2);
  Velocite.z=_Vitesse*sin(angle2);

  Couleur=_Couleur;

  DureeVie=_DureeVie*Random();
  
  Taille=_Taille;
}

void ExplosionBriqueSouffle::MajParticule(MOGL_Struct_Vecteur & p_Position,
                                   MOGL_Struct_Vecteur & p_Velocite,
                                   MOGL_Struct_Couleur & p_Couleur)
{
  MOGL_Struct_Vecteur vec=MOGL_Temps::GetVariationVecteurDistance(p_Velocite);
  
  p_Position.x+=vec.x;
  p_Position.y+=vec.y;
  p_Position.z+=vec.z;
}

void ExplosionBriqueSouffle::GenerateurInactif()
{
  _GenerateurFini=true;
  if (_TempoFini && _GenerateurFini)
  {
      Generateur::GenerateurInactif();  
  }
}

void ExplosionBriqueSouffle::_FinTempo()
{
  _TempoFini=true;
  _Souffle();
  if (_TempoFini && _GenerateurFini)
  {
      Generateur::GenerateurInactif();  
  }
}

void ExplosionBriqueSouffle::_Souffle()
{
  TableauBase::Set_ElementTableau elemDetruire;

  TableauBase::Set_ElementTableau::iterator it;
  for(it=_Tableau->GetSetElementTableau()->begin(); it!=_Tableau->GetSetElementTableau()->end(); it++)
  {
    float x=(*it)->GetPositionX();
    float y=(*it)->GetPositionY();

    if (sqrt((x-_X)*(x-_X)+(y-_Y)*(y-_Y)) < _RayonSouffle)
    {
      // On fait apparaitre un syst�me de particules ?
      Generateur * generateur=(*it)->CreerGenerateurParticules();
      if (generateur!=NULL)
      {
        _Tableau->AjouterGenerateur(generateur);
      }

      if ((*it)->Toucher(NULL, 0))
      {
        _Tableau->CreerBonus((*it)->GetPositionX(), (*it)->GetPositionY());
        // On efface l'�l�ment        
        elemDetruire.insert(*it);
      }

    }
  }
  for(it=elemDetruire.begin(); it!=elemDetruire.end(); it++)
  {
    _Tableau->RetirerElementTableau((*it));
    delete (*it);    
  }
}
