/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BriquePetite.h"
#include "ExplosionBriqueSouffle.h"
#include "Constante.h"
#include <MOGL_GestionnaireObjet.h>
#include <iostream>
#include "BibliothequeSon.h"

MOGL_Objet * BriquePetite::_ObjetSource=NULL;

BriquePetite::BriquePetite(TableauBase * p_TableauBase): BriqueBase(p_TableauBase)
{
  _TailleX=1.5;
  _TailleY=1;
  if (_ObjetSource==NULL)
  {
    PreCharger();
  }
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BriquePetite::BriquePetite(const BriquePetite & p_BriquePetite): BriqueBase(p_BriquePetite)
{
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BriquePetite::~BriquePetite()
{
  delete _ElementArbre;
}

void BriquePetite::PreCharger()
{
  _ObjetSource=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/petite_brique.tri").c_str(),
                                                  *Texture::GetGestionnaireTexture(), *_ObjetSource);
}

Struct_Collision BriquePetite::TesterCollision(Balle * p_Balle)
{
  Struct_Collision col=BriqueBase::TesterCollision(p_Balle);
  return col;
}

Generateur * BriquePetite::CreerGenerateurParticules()
{
  MOGL_Struct_Couleur coul;
  coul.r=0.3f;
  coul.g=0.6f;
  coul.b=0.05f;
  return new ExplosionBriqueSouffle(this, coul, 8, 0.6f, 30, 4);
}

bool BriquePetite::Toucher(Balle * p_Balle)
{
  BibliothequeSon::SonExplosion->Jouer(false);
  return true;
}
