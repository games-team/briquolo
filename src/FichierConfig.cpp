/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include <fstream>
#include <iostream>
#include "I18n.h"
#include "FichierConfig.h"

using namespace std;

FichierConfig::FichierConfig(const string & pNomFichier): _NomFichier(pNomFichier)
{
}

void FichierConfig::SetNomFichier(const string & pNomFichier)
{
  _NomFichier = pNomFichier;
}

bool FichierConfig::Charger()
{
  ifstream fichier(_NomFichier.c_str());
  if (!fichier.good())
  {
    cerr<<"There is no configuration file"<<endl;
    return false;
  }

  string nom, param;
  while (!fichier.eof())
  {
    string ligne;
    getline(fichier, ligne);

    bool bTrouve = false;
    for(Set_ElementOption::iterator it = _SetElementOption.begin(); it != _SetElementOption.end() && !bTrouve; it++)
    {
      bTrouve = (*it)->TraiterLigneFichier(ligne);
    }
  }
  return true;
}

bool FichierConfig::Enregistrer()
{
  ofstream fichier(_NomFichier.c_str());
  if (!fichier.good())
  {
    return false;
  }
  for(Set_ElementOption::iterator it = _SetElementOption.begin(); it != _SetElementOption.end(); it++)
  {
    fichier<<(*it)->GenererLigneFichier()<<endl;
  }

  return true;
}

void FichierConfig::AddElementOption(ElementOption * pElement)
{
  _SetElementOption.insert(pElement);
}
