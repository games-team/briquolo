/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Charger.h"
#include "TableauBase.h"
#include "I18n.h"

Charger::Charger(MOGL_Panneau * p_Panneau): MOGL_Panneau(p_Panneau, _("Load"), 300, 220)
{
  MOGL_Struct_Couleur coulSel;
  coulSel.r=.32f;
  coulSel.g=.74f;
  coulSel.b=1.0f;
  coulSel.a=.6f;

  MOGL_Struct_Couleur coulTitre;
  coulTitre.r=0.84f;
  coulTitre.g=0.88f;
  coulTitre.b=1.0f;
  coulTitre.a=1;

  SetCouleurLabel(coulTitre);

  _ChoixGroupe=new MOGL_Choix(this, _("Group"));
  _ChoixGroupe->SetCouleurFondFocus(coulSel);
  _ChoixGroupe->ChangementElement.Connecter(this, &Charger::_OnChangementGroupe);

  _ChoixTableau=new MOGL_Choix(this, _("Name"));
  _ChoixTableau->SetCouleurFondFocus(coulSel);

  _Sep=new MOGL_Separateur(this);

  _Ok=new MOGL_Bouton(this, _("Ok"));
  _Ok->SetCouleurFondFocus(coulSel);
  _Ok->Activation.Connecter(this, &Charger::_OnOk);

  _Annuler=new MOGL_Bouton(this, _("Cancel"));
  _Annuler->SetCouleurFondFocus(coulSel);
  _Annuler->Activation.Connecter(this, &Charger::_OnAnnuler);
}

Charger::~Charger()
{
  delete _Annuler;
  delete _Ok;
  delete _Sep;
  delete _ChoixTableau;
}

void Charger::_OnChangementGroupe(unsigned int p_NumGroupe)
{
  _ChoixTableau->ViderElementsChoix();
  set<string> liste=TableauBase::GetListeTableau(TableauBase::GetRepGroupeFromId(p_NumGroupe));
  for(set<string>::iterator it=liste.begin(); it!=liste.end(); it++)
  {
    _ChoixTableau->AjouterElementChoix(*it);
  }
  _ChoixTableau->SetIndiceCourant(0);

}

void Charger::_OnOk()
{
  DemanderChargement.Emettre(TableauBase::GetRepGroupeFromId(_ChoixGroupe->GetIndiceCourant()), _ChoixTableau->GetElementChoixCourant());
  SetActivation(false);
}

void Charger::_OnAnnuler()
{
  SetActivation(false);
}

void Charger::SetActivation(bool p_Activation)
{
  _ChoixGroupe->ViderElementsChoix();
  list<string> liste=TableauBase::GetListeGroupeTableau();
  for(list<string>::iterator it=liste.begin(); it!=liste.end(); it++)
  {
    _ChoixGroupe->AjouterElementChoix(*it);
  }

  _ChoixTableau->ViderElementsChoix();
  liste.clear();
  set<string> ensemble=TableauBase::GetListeTableau(TableauBase::GetRepGroupeFromId(_ChoixGroupe->GetIndiceCourant()));
  for(set<string>::iterator it=ensemble.begin(); it!=ensemble.end(); it++)
  {
    _ChoixTableau->AjouterElementChoix(*it);
  }

  MOGL_Panneau::SetActivation(p_Activation);
}
