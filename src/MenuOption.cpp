/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "MenuOption.h"
#include "Texture.h"
#include "Option.h"
#include <MOGL_Temps.h>
#include <sstream>

using namespace std;

MenuOption::MenuOption(MOGL_Panneau * p_Panneau): MOGL_Panneau(p_Panneau, _("Settings"), 320, 220)
{
  MOGL_Struct_Couleur coulSel;
  coulSel.r=.32f;
  coulSel.g=.74f;
  coulSel.b=1.0f;
  coulSel.a=.6f;

  MOGL_Struct_Couleur coulTitre;
  coulTitre.r=0.84f;
  coulTitre.g=0.88f;
  coulTitre.b=1.0f;
  coulTitre.a=1;

  // Panneau de video
  SetCouleurLabel(coulTitre);
  _PanneauVideo=new MOGL_Panneau(this, _("Video"), 320,220);
  _Resolution=new MOGL_Choix(_PanneauVideo, _("Resolution"));
  _Resolution->SetCouleurFondFocus(coulSel);
  _SetRes = _Fenetre->GetAvailableResolution();
  for(MOGL_Fenetre::MOGL_ItSet_Resolution it = _SetRes.begin(); it != _SetRes.end(); it++)
  {
    stringstream st;
    st<<it->w<<"x"<<it->h;
    
    _Resolution->AjouterElementChoix(st.str());
  }

  _Fullscreen=new MOGL_Choix(_PanneauVideo, _("Fullscreen"));
  _Fullscreen->SetCouleurFondFocus(coulSel);
  _Fullscreen->AjouterElementChoix(_("no"));
  _Fullscreen->AjouterElementChoix(_("yes"));

  _ChoixAfficherFPS=new MOGL_Choix(_PanneauVideo, _("Show FPS"));
  _ChoixAfficherFPS->SetCouleurFondFocus(coulSel);
  _ChoixAfficherFPS->AjouterElementChoix(_("no"));
  _ChoixAfficherFPS->AjouterElementChoix(_("yes"));

  _ChoixLimiterFPS=new MOGL_Choix(_PanneauVideo, _("Limit FPS"));
  _ChoixLimiterFPS->SetCouleurFondFocus(coulSel);
  _ChoixLimiterFPS->AjouterElementChoix(_("no"));
  _ChoixLimiterFPS->AjouterElementChoix(_("yes"));
  _ChoixLimiterFPS->ChangementElement.Connecter(this, &MenuOption::_OnChangementLimiterFPS);

  _NombreFPSMax=new MOGL_ChoixNombre(_PanneauVideo, _("  FPS Max"), 60, 1);
  _NombreFPSMax->SetCouleurFondFocus(coulSel);
  _NombreFPSMax->SetCacher(true);

  _SepVideo=new MOGL_Separateur(_PanneauVideo);
  _OkVideo=new MOGL_Bouton(_PanneauVideo, _("Ok"));
  _OkVideo->SetCouleurFondFocus(coulSel);
  _OkVideo->Activation.Connecter(this, &MenuOption::_OnOkVideo);
  _AnnulerVideo=new MOGL_Bouton(_PanneauVideo, _("Cancel"));
  _AnnulerVideo->SetCouleurFondFocus(coulSel);
  _AnnulerVideo->Activation.Connecter(this, &MenuOption::_OnAnnulerVideo);

  // Panneau de commandes
  _PanneauCommande=new MOGL_Panneau(this, _("Controls"), 320,220);
  _SensibiliteClavier=new MOGL_ChoixNombre(_PanneauCommande, _("Keyboard speed"), 5, 1);
  _SensibiliteClavier->SetCouleurFondFocus(coulSel);
  _SensibiliteClavier->SetMin(1);
  _SensibiliteSouris=new MOGL_ChoixNombre(_PanneauCommande, _("Mouse speed"), 5, 1);
  _SensibiliteSouris->SetCouleurFondFocus(coulSel);
  _SensibiliteSouris->SetMin(1);

  _SepCommande=new MOGL_Separateur(_PanneauCommande);
  _OkCommande=new MOGL_Bouton(_PanneauCommande, _("Ok"));
  _OkCommande->SetCouleurFondFocus(coulSel);
  _OkCommande->Activation.Connecter(this, &MenuOption::_OnOkCommande);
  _AnnulerCommande=new MOGL_Bouton(_PanneauCommande, _("Cancel"));
  _AnnulerCommande->SetCouleurFondFocus(coulSel);
  _AnnulerCommande->Activation.Connecter(this, &MenuOption::_OnAnnulerCommande);

  // Panneau principale des options
  _Video=new MOGL_Bouton(this, _("Video"));
  _Video->SetCouleurFondFocus(coulSel);
  _Video->Activation.Connecter(this, &MenuOption::_OnVideo);
  _Commande=new MOGL_Bouton(this, _("Controls"));
  _Commande->SetCouleurFondFocus(coulSel);
  _Commande->Activation.Connecter(this, &MenuOption::_OnCommande);
  _SepOpt1=new MOGL_Separateur(this);
  _RetourOption=new MOGL_Bouton(this, _("Back"));
  _RetourOption->SetCouleurFondFocus(coulSel);
  _RetourOption->Activation.Connecter(this, &MenuOption::_OnRetourOption);



}

MenuOption::~MenuOption()
{
  delete _PanneauVideo;
  delete _Resolution;
  delete _Fullscreen;
  delete _SepVideo;
  delete _SepOpt1;
  delete _RetourOption;
  delete _OkVideo;
  delete _AnnulerVideo;
  delete _Video;

}

void MenuOption::_OnOkVideo()
{
  int anc=-1;
  if (Option::ResolutionX.GetValeur()==640 && Option::ResolutionY.GetValeur()==480)
  {
    anc=0;
  }
  if (Option::ResolutionX.GetValeur()==800 && Option::ResolutionY.GetValeur()==600)
  {
    anc=1;
  }
  if (Option::ResolutionX.GetValeur()==1024 && Option::ResolutionY.GetValeur()==768)
  {
    anc=2;
  }

  bool fullscreen=false;
  switch (_Fullscreen->GetIndiceCourant())
  {
    case 0:
    {
      fullscreen=false;
      break;
    }
    case 1:
    {
      fullscreen=true;
      break;
    }
  }

  if (anc!=static_cast<int>(_Resolution->GetIndiceCourant()))
  {
    MOGL_Fenetre::MOGL_ItSet_Resolution it = _SetRes.begin();
    for(unsigned int i = 0; i < _Resolution->GetIndiceCourant() && it != _SetRes.end(); i++)
    {
      it++;
    }

    if ( it != _SetRes.end())
    {
      _Fenetre->SetMode(it->w, it->h, fullscreen);
    }
    else
    {
      _Fenetre->SetMode(_SetRes.rbegin()->w, _SetRes.rbegin()->h, fullscreen);
    }
  }
  else
  {
    _Fenetre->SetFullscreen(fullscreen);
  }

  if (_ChoixLimiterFPS->GetIndiceCourant()==1 && _NombreFPSMax->GetNombre()>0)
  {
    MOGL_Temps::SetMaxFPS(_NombreFPSMax->GetNombre());
    Option::MaxFPS.SetValeur(static_cast<int>(_NombreFPSMax->GetNombre()));
  }
  else
  {
    MOGL_Temps::SetMaxFPS(0);
    Option::MaxFPS.SetValeur(0);
  }

  Option::ResolutionX.SetValeur(_Fenetre->GetResolutionX());
  Option::ResolutionY.SetValeur(_Fenetre->GetResolutionY());
  Option::Fullscreen.SetValeur(_Fenetre->GetFullscreen());
  Option::AfficherFPS.SetValeur(_ChoixAfficherFPS->GetIndiceCourant()==1);
  _PanneauVideo->SetActivation(false);

  _InitialiserVideo();
}

void MenuOption::_OnAnnulerVideo()
{
  _PanneauVideo->SetActivation(false);
  
  _InitialiserVideo();
}

void MenuOption::_OnOkCommande()
{
  Option::VitesseClavier.SetValeur(static_cast<int>(_SensibiliteClavier->GetNombre()));
  Option::VitesseSouris.SetValeur(static_cast<int>(_SensibiliteSouris->GetNombre()));
  _PanneauCommande->SetActivation(false);
  
  _InitialiserVideo();
}

void MenuOption::_OnAnnulerCommande()
{
  _PanneauCommande->SetActivation(false);
  
  _InitialiserVideo();
}

void MenuOption::_OnRetourOption()
{
  SetActivation(false);
}

void MenuOption::_OnVideo()
{
  _PanneauVideo->SetActivation(true);
}

void MenuOption::_OnCommande()
{
  _PanneauCommande->SetActivation(true);
}

void MenuOption::SetActivation(bool p_Activation)
{
  _InitialiserVideo();
  _InitialiserCommande();
  MOGL_Panneau::SetActivation(p_Activation);
}

void MenuOption::_OnChangementLimiterFPS(unsigned int p_Indice)
{
  if (p_Indice==1)
  {
    _NombreFPSMax->SetCacher(false);
  }
  else
  {
    _NombreFPSMax->SetCacher(true);
  }
}

void MenuOption::_InitialiserCommande()
{
  _SensibiliteClavier->SetNombre(Option::VitesseClavier.GetValeur());
  _SensibiliteSouris->SetNombre(Option::VitesseSouris.GetValeur());
}

void MenuOption::_InitialiserVideo()
{
  bool notTouve = true;
  unsigned int i = 0;
  for(MOGL_Fenetre::MOGL_ItSet_Resolution it = _SetRes.begin(); it != _SetRes.end() && notTouve; it++)
  {
    if (it->w == static_cast<unsigned int>(Option::ResolutionX.GetValeur()) 
        && it->h == static_cast<unsigned int>(Option::ResolutionY.GetValeur()))
    {
      _Resolution->SetIndiceCourant(i);
    }
    i++;
  }
  if (Option::Fullscreen.GetValeur())
  {
    _Fullscreen->SetIndiceCourant(1);
  }
  else
  {
    _Fullscreen->SetIndiceCourant(0);
  }

  if (Option::AfficherFPS.GetValeur())
  {
    _ChoixAfficherFPS->SetIndiceCourant(1);
  }
  else
  {
    _ChoixAfficherFPS->SetIndiceCourant(0);
  }

  if (Option::MaxFPS.GetValeur()==0)
  {
    _ChoixLimiterFPS->SetIndiceCourant(0);
    _NombreFPSMax->SetCacher(true);
    _NombreFPSMax->SetNombre(60);
  }
  else
  {
    _ChoixLimiterFPS->SetIndiceCourant(1);
    _NombreFPSMax->SetCacher(false);
    _NombreFPSMax->SetNombre(Option::MaxFPS.GetValeur());
  }

}
