/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _PANNEAULIGNEOPTIONEDITEUR
#define _PANNEAULIGNEOPTIONEDITEUR

#include <MOGL_Panneau.h>
#include <MOGL_Label.h>
#include <MOGL_PoliceTTF.h>
#include <MOGL_SaisieChaine.h>

class PanneauLigneOptionEditeur: public MOGL_Panneau
{
  protected:
    MOGL_PoliceTTF _Police;
    bool _Pause;
    string _LigneOption;
  public:
    MOGL_Label * _LabelLigne;
    MOGL_SaisieChaine * _SaisieLigne;

  protected:
    void _OnChangementResolution(int p_ResolutionX, int p_ResolutionY, int p_BitParPixel);
    void _OnChangementVariation(unsigned int p_Indice);

  public:
    PanneauLigneOptionEditeur(MOGL_Fenetre * p_Fenetre);
    void SetLigneOption(const string & p_LigneOption);

    void SetEditMode(bool p_EditMode);
    void SetPause(bool p_Pause);
    bool GetPause() const;
    void ClavierUp(SDL_keysym * key);
    void ValiderLigne();

};

#endif
