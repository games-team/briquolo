/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "MenuJeu.h"

MenuJeu::MenuJeu(MOGL_Fenetre * p_Fenetre): MenuBase(p_Fenetre)
{
  MOGL_Struct_Couleur coulSel;
  coulSel.r=.32f;
  coulSel.g=.74f;
  coulSel.b=1.0f;
  coulSel.a=.6f;

  _Reprendre=new MOGL_Bouton(this, _("Continue game"));
  _Reprendre->SetCouleurFondFocus(coulSel);
  _Reprendre->Activation.Connecter(this, &MenuJeu::_OnReprendre);
  _Sep1=new MOGL_Separateur(this);
  _Options=new MOGL_Bouton(this, _("Settings"));
  _Options->SetCouleurFondFocus(coulSel);
  _Options->Activation.Connecter(this, &MenuJeu::_OnOptions);
  _Sep2=new MOGL_Separateur(this);
  _Quitter=new MOGL_Bouton(this, _("Quit the game"));
  _Quitter->SetCouleurFondFocus(coulSel);
  _Quitter->Activation.Connecter(this, &MenuJeu::_OnQuitter);

  _MenuOption=new MenuOption(this);
}

MenuJeu::~MenuJeu()
{
  delete _MenuOption;
  delete _Quitter;
  delete _Sep2;
  delete _Options;
  delete _Sep1;
  delete _Reprendre;
}

void MenuJeu::_OnQuitter()
{
  _DemandeQuitter=true;
}

void MenuJeu::_OnReprendre()
{
  SetActivation(false);
}

void MenuJeu::_OnOptions()
{
  _MenuOption->SetActivation(true);
}
