/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _CONSTANTE
#define _CONTANSTE

#include <string>

using namespace std;

class Constante
{
  protected:
    static string _Local;
    static string _Global;
    static string _Data;
    static string _Tableau;
    static string _Config;
    static string _NomPackage;
    static string _DirLocale;
    static string _NumVersion;
    static bool _Init;

  protected:
    static void _InitialiserVariables();

  public:
    static string GetLocalDir();
    static string GetGlobalDir();
    static string GetDataDir();
    static string GetTableauDir();
    static string GetNomConfig();
    static string GetNomPackage();
    static string GetLocaleDir();
    static string GetVersion();
};

#endif
