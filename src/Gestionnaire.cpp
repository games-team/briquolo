/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Gestionnaire.h"
#include "BriqueDeuxCoups.h"
#include "Brique.h"
#include "BriquePetite.h"
#include "BriqueIncassable.h"
#include "BriqueBumper.h"
#include "I18n.h"

ElementTableau * Gestionnaire::GetElementTableau(unsigned int p_IdentifiantElement, TableauBase * p_TableauBase)
{
  switch (p_IdentifiantElement)
  {
    case 0 :
    {
      ElementTableau * elem=new Brique(p_TableauBase);
      elem->SetId(p_IdentifiantElement);
      return elem;
      break;
    }
    case 1 :
    {
      ElementTableau * elem=new BriqueDeuxCoups(p_TableauBase);
      elem->SetId(p_IdentifiantElement);
      return elem;
      break;
    }
    case 2 :
    {
      ElementTableau * elem=new BriqueIncassable(p_TableauBase);
      elem->SetId(p_IdentifiantElement);
      return elem;
      break;
    }
    case 3 :
    {
      ElementTableau * elem=new BriquePetite(p_TableauBase);
      elem->SetId(p_IdentifiantElement);
      return elem;
      break;
    }
    case 4 :
    {
      ElementTableau * elem=new BriqueBumper(p_TableauBase);
      elem->SetId(p_IdentifiantElement);
      return elem;
      break;
    }
    default:
    {
      return NULL;
      break;
    }
  }
}

string Gestionnaire::GetNomElementTableau(unsigned int p_IdentifiantElement)
{
  switch (p_IdentifiantElement)
  {
    case 0 :
    {
      return _("Normal Brick");
      break;
    }
    case 1 :
    {
      return _("2 Shots Brick");
      break;
    }
    case 2 :
    {
      return _("Unbreakable Brick");
      break;
    }
    case 3 :
    {
      return _("Small Brick");
      break;
    }
    case 4 :
    {
      return _("Bumper Brick");
      break;
    }
    default:
    {
      return "";
      break;
    }
  }
}

unsigned int Gestionnaire::GetNombreElementTableau()
{
  return 5;
}
