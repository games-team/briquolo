/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "Son.h"

bool Son::Initialiser()
{
  if(SDL_InitSubSystem(SDL_INIT_AUDIO)==-1)
  {
    cerr<<"Error initializing SDL Audio"<<endl;
    return false;
  }
  return true;
}

Son::Son(): MOGL_Action(), _Sample(NULL), _EnCours(false)
{
}

bool Son::ChargerWav(const char * p_NomFichier)
{
  unsigned int buf;
#ifdef WIN32
  // 1024 chez moi �a suffit pas
  buf=2048;
#else
  buf=1024;
#endif
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, buf)!=0)
  {
    cerr<<"Error initializing SDL-Mixer"<<endl;
    return false;
  }
  Mix_AllocateChannels(16);

  _Sample=Mix_LoadWAV(p_NomFichier);

  if (_Sample==NULL)
  {
    cerr<<"Error loading the wav file: "<<p_NomFichier<<endl;
    return false;
  }
  _EnCours=true;
  return true;
}

int Son::Jouer(bool p_Repeter)
{
  if (p_Repeter)
  {
    return Mix_PlayChannel(-1, _Sample, -1);
  }
  else
  {
    return Mix_PlayChannel(-1, _Sample, 0);
  }
}

void Son::ArreterCanal(unsigned int p_Canal)
{
  Mix_HaltChannel(p_Canal);
}

Son::~Son()
{
  if (_EnCours)
  {
    Mix_FreeChunk(_Sample);
    _Sample=NULL;
  
    Mix_CloseAudio();
    _EnCours=false;
  }

}
