/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Texture.h"
#include <iostream>
#include "Constante.h"

using namespace std;

MOGL_GestionnaireTexture * Texture::_GestionnaireTexture=NULL;
set<MOGL_Texture *> Texture::_SetTexture;

void Texture::ChargerTextures()
{
  if (_GestionnaireTexture==NULL)
  {
    _GestionnaireTexture=new MOGL_GestionnaireTexture();
  }

  MOGL_Texture * tex;

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.1.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env1", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.2.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env2", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.3.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env3", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.4.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env4", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.5.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env5", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->RepeterTexture(false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/Environment.6.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Env6", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_OPAQUE, true);
  tex->SetCouleurTrouOctet(255,255,255);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/grille.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Grille", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Brique", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_deuxcoups.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("BriqueDeuxCoups", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_incassable.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("BriqueIncassable", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/petite_brique.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("PetiteBrique", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_OPAQUE, true);
  tex->SetCouleurTrouOctet(0,0,0);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/breche.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("ParticuleBreche", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_SATURE);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/eau2.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Eau", tex);
  tex->SetAnimation(true);
  tex->SetVitesseTranslation(-0.05f,0);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/herbe.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Herbe", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_OPAQUE, false);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/pierre.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Pierre", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_OPAQUE);
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/mur1.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Mur1", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/fast.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Fast", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/slow.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Slow", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/petit.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Petit", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/gros.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Gros", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bonus_petitplateau.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("PetitPlateau", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bonus_grosplateau.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("GrosPlateau", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/double.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Double", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/superpuissance.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("SuperPuissance", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/vuesubjective.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("VueSubjective", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/balle.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Balle", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/plateau.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Plateau", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_SATURE);
  tex->ChargerBMP((Constante::GetGlobalDir()+Constante::GetDataDir()+"/flare.bmp").c_str());
  _GestionnaireTexture->AjouterTextureObjet("MorceauBrique", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_SATURE);
  tex->ChargerBMP((Constante::GetGlobalDir()+Constante::GetDataDir()+"/flare.bmp").c_str());
  _GestionnaireTexture->AjouterTextureObjet("TraceBalle", tex);
  _SetTexture.insert(tex);

  tex=new MOGL_Texture;
  tex->ChargerPNG((Constante::GetGlobalDir()+Constante::GetDataDir()+"/flame.png").c_str());
  _GestionnaireTexture->AjouterTextureObjet("Flip", tex);
  _SetTexture.insert(tex);
}

void Texture::RechargerTextures()
{
  for(set<MOGL_Texture *>::iterator it=_SetTexture.begin(); it!=_SetTexture.end(); it++)
  {
    (*it)->Recharger();
  }
}


void Texture::DetruireTextures()
{
  for(set<MOGL_Texture *>::iterator it=_SetTexture.begin(); it!=_SetTexture.end(); it++)
  {
    delete (*it);
  }
  _SetTexture.clear();
}

MOGL_GestionnaireTexture * Texture::GetGestionnaireTexture()
{
  return _GestionnaireTexture;
}
