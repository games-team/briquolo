/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Curseur.h"

Curseur::Curseur(TableauBase * p_Tableau): MOGL_NoeudOpenGL(), _Tableau(p_Tableau)
{
  _PositionX=(_Tableau->GetXMax() - _Tableau->GetXMin())/2;
  _PositionY=(_Tableau->GetYMax() - _Tableau->GetYMin())/2;
}

void Curseur::CodeOpenGL()
{
  /** @todo V�rifier ce probl�me sous WIN32*/
#ifndef WIN32
  // Ba ouais, sinon, �a fait un truc tout zarb, mais seuleument sous windows
  // Mais c'est peut �tre la faute d'un bug du driver de la Voodoo3 ?
  // De toute fa�on �a marche sans �a, donc c'est pas tr�s grave
  // (mais c'est moins propre)
  glPushAttrib(GL_ALL_ATTRIB_BITS);
#endif
  glColor4f(1,0,0,1);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);

  glLineWidth(3);
  glBegin(GL_LINES);
  // ligne horizontale
  glVertex2f(_Tableau->GetXMin(), _PositionY);
  glVertex2f(_Tableau->GetXMax(), _PositionY);

  // ligne verticales
  glVertex2f(_PositionX, _Tableau->GetYMin());
  glVertex2f(_PositionX, _Tableau->GetYMax());

  glEnd();
  
#ifndef WIN32
  // cf plus haut !
  glPopAttrib();  
#endif
}

void Curseur::SetPosition(float p_X, float p_Y)
{
  _PositionX=p_X;
  _PositionY=p_Y;
}

float Curseur::GetPositionX() const
{
  return _PositionX;
}

float Curseur::GetPositionY() const
{
  return _PositionY;
}

