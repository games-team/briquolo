/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Camera.h"
#include <MOGL_Temps.h>

Camera::Camera(Plateau * p_Plateau): MOGL_Action(), NB_VUE(2), _Plateau(p_Plateau), _VueSubjective(false),
                                     _TempoVueSubjective(10, false, false), _AllerSubjectif(false), _RetourSubjectif(false),
                                     _Fact(0), _OffsetX(0), _OffsetY(0)
{
  _VueHaut();
  _TempoVueSubjective.FinTemporisation.Connecter(this, &Camera::_OnFinVueSubjective);
  ((MOGL_Noeud *)_Plateau->GetElementArbreMOGL())->AjouterElement(&_TempoVueSubjective);
}

void Camera::_VueHaut()
{
  _VueCourante=HAUT;
  _Camera.SetFov(45);
  _Camera.SetHaut(0,0,1);
  _Camera.SetPosition(20,-13, 40);
  _Camera.SetDirection(0,1,-0.8f);
  _Camera.SetClipping(0.2f,200);
}

void Camera::_VueHaut2()
{
  _VueCourante=HAUT2;
  _Camera.SetFov(45);
  _Camera.SetHaut(0,0,1);
  _Camera.SetPosition(20,25, 70);
  //_Camera.SetDirection(0,0.000001,-1.0f);
  _Camera.SetDirection(0,0.2,-1.0f);
  _Camera.SetClipping(0.2f,200);
}

/*
void Camera::_VuePlateau()
{
  _Camera.SetHaut(0,0,1);
  _Camera.SetFov(100);
  _Camera.SetPosition(_Plateau->GetPositionX(),1, 1);
  _Camera.SetDirection(0,1,-0.3f);
  _Camera.SetClipping(0.2f,200);
}
*/

void Camera::_VuePlateau()
{
  _Camera.SetHaut(0,0,1); 
  
  switch (_VueCourante)
  {
    case HAUT2 :
    {
      float posY = _Fact*_Fact*70.47619 + _Fact*-94.4761 + 25;
      float posZ = _Fact*_Fact*62 + _Fact*-131 + 70;
      _Camera.SetPosition(20 + _Fact*(_Plateau->GetPositionX() - 20) ,posY, posZ);


      float dirZ = -1 + _Fact*_Fact*0.7;
      _Camera.SetDirection(0,0.0000001 + _Fact*1, dirZ );

      float fov = _Fact*_Fact*30 + _Fact*25 + 45;
      _Camera.SetFov(fov);
      break;
    }
    case HAUT:
    {
      _Camera.SetFov(45+55*_Fact);
      float y = _Fact*_Fact*30 + _Fact*-69 + 40;
      _Camera.SetPosition(20 + _Fact*(_Plateau->GetPositionX() - 20),-13 + _Fact*14, y);
      float dirY = _Fact*_Fact*0.714285714 + _Fact*-0.214285714 -0.8;
      _Camera.SetDirection(0,1, dirY);
      break;
    }
  }

  _Camera.SetClipping(0.2f,200);
}

void Camera::ChangerVue()
{
  if (!_VueSubjective)
  {
    if (static_cast<unsigned int>(_VueCourante)+1==NB_VUE)
    {
      ChangerVue(Enum_TypeVue(0));
    }
    else
    {
      ChangerVue(Enum_TypeVue(static_cast<unsigned int>(_VueCourante)+1));
    }
  }
}

void Camera::ChangerVue(Enum_TypeVue p_TypeVue)
{
  if (!_VueSubjective)
  {
    switch (p_TypeVue)
    {
      case HAUT :
      {
        _VueHaut();
        break;
      }
      case HAUT2 :
      {
        _VueHaut2();
        break;
      }
    }
  }
}

MOGL_Camera * Camera::GetMOGLCamera()
{
  return &_Camera;
}

void Camera::Maj()
{
  if (_VueSubjective)
  {
    //_Camera.SetPosition(_Plateau->GetPositionX(),1, 1);
    if (_AllerSubjectif)
    {
      _Fact+=MOGL_Temps::GetVariationDistance(1);
      if (_Fact > 1)
      {
        _Fact = 1;
      }
    }
    else if (_RetourSubjectif)
    {
      _Fact-=MOGL_Temps::GetVariationDistance(1);
      if (_Fact < 0)
      {
        _Fact = 0;
        SetVueSubjective(false);
      }
    }
    _VuePlateau();
  }
  else
  {
    switch (_VueCourante)
    {
      case HAUT2 :
      {
        float posY = _Fact*_Fact*70.47619 + _Fact*-94.4761 + 25;
        float posZ = _Fact*_Fact*62 + _Fact*-131 + 70;
        _Camera.SetPosition(20 + _Fact*(_Plateau->GetPositionX() - 20) ,posY, posZ);
        break;
      }
      case HAUT:
      {
        float y = _Fact*_Fact*30 + _Fact*-69 + 40;
        _Camera.SetPosition(20 + _Fact*(_Plateau->GetPositionX() - 20),-13 + _Fact*14, y);
        break;
      }
    }

  }
  _Camera.SetPosition(_Camera.GetPosition().x + _OffsetX, _Camera.GetPosition().y + _OffsetY, _Camera.GetPosition().z);
}

void Camera::SetVueSubjective(bool p_Actif)
{
  if (p_Actif)
  {
    _AllerSubjectif = true;
    _RetourSubjectif = false;
    _TempoVueSubjective.Arreter();
    _TempoVueSubjective.Demarrer();
    _VuePlateau();
  }
  else
  {
    _Fact = 0;
    _TempoVueSubjective.Arreter();
    if (_VueSubjective)
    {
      switch (_VueCourante)
      {
        case HAUT :
        {
          _VueHaut();
          break;
        }
        case HAUT2 :
        {
          _VueHaut2();
          break;
        }
      }
    }
  }
  _VueSubjective = p_Actif;
}

bool Camera::GetVueSubjective() const
{
  return _VueSubjective;
}

void Camera::_OnFinVueSubjective()
{
  _AllerSubjectif = false;
  _RetourSubjectif = true;
  //SetVueSubjective(false);
}

void Camera::SetOffset(float pOffsetX, float pOffsetY)
{
  _OffsetX = pOffsetX;
  _OffsetY = pOffsetY;
}

