/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BriqueIncassable.h"
#include "ExplosionBrique.h"
#include "Constante.h"
#include <MOGL_GestionnaireObjet.h>
#include <iostream>
#include "BibliothequeSon.h"

MOGL_Objet * BriqueIncassable::_ObjetSource=NULL;

BriqueIncassable::BriqueIncassable(TableauBase * p_TableauBase): BriqueBase(p_TableauBase)
{
  if (_ObjetSource==NULL)
  {
    PreCharger();
  }
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
  _Decors=true;
}

BriqueIncassable::BriqueIncassable(const BriqueIncassable & p_BriqueIncassable): BriqueBase(p_BriqueIncassable)
{
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BriqueIncassable::~BriqueIncassable()
{
  delete _ElementArbre;
}

void BriqueIncassable::PreCharger()
{
  _ObjetSource=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_incassable.tri").c_str(), *Texture::GetGestionnaireTexture(), *_ObjetSource);
}

Struct_Collision BriqueIncassable::TesterCollision(Balle * p_Balle)
{
  Struct_Collision col=BriqueBase::TesterCollision(p_Balle);
  return col;
}

Generateur * BriqueIncassable::CreerGenerateurParticules()
{
  return NULL;
}

bool BriqueIncassable::Toucher(Balle * p_Balle, double p_Temps)
{
  BibliothequeSon::SonBriqueRebond->Jouer(false);
  return false;
}
