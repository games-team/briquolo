/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BibliothequeSon.h"
#include "Constante.h"

Son * BibliothequeSon::SonExplosion=NULL;
Son * BibliothequeSon::SonRebond=NULL;
Son * BibliothequeSon::SonBriqueRebond=NULL;
Son * BibliothequeSon::SonBonus=NULL;
Son * BibliothequeSon::SonBreche=NULL;
Son * BibliothequeSon::SonSuperPuissance=NULL;
Son * BibliothequeSon::SonBumper=NULL;


void BibliothequeSon::Initialiser()
{
  if (SonExplosion==NULL)
  {
    SonExplosion=new Son;
    SonExplosion->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/explosion2.wav").c_str());
  }
  if (SonRebond==NULL)
  {
    SonRebond=new Son;
    SonRebond->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/rebond.wav").c_str());
  }
  if (SonBonus==NULL)
  {
    SonBonus=new Son;
    SonBonus->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bonus.wav").c_str());
  }
  if (SonBriqueRebond==NULL)
  {
    SonBriqueRebond=new Son;
    SonBriqueRebond->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique_rebond.wav").c_str());
  }
  if (SonBreche==NULL)
  {
    SonBreche=new Son;
    SonBreche->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/breche.wav").c_str());
  }
  if (SonSuperPuissance==NULL)
  {
    SonSuperPuissance=new Son;
    SonSuperPuissance->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/superpuissance.wav").c_str());
  }
  if (SonBumper==NULL)
  {
    SonBumper=new Son;
    SonBumper->ChargerWav((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bot_land.wav").c_str());
  }
}

void BibliothequeSon::Terminer()
{
  delete SonExplosion;
  delete SonRebond;
  delete SonBriqueRebond;
  delete SonBonus;
  delete SonBreche;
  delete SonBumper;

  SonExplosion=NULL;
  SonRebond=NULL;
  SonBonus=NULL;
  SonBriqueRebond=NULL;
  SonBreche=NULL;
  SonBumper=NULL;
}
