/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ElementOptionString.h"

ElementOptionString::ElementOptionString(const string & p_Nom, const string & p_ParametreDefaut)
  : ElementOption(p_Nom), _Parametre(p_ParametreDefaut)
{
}

bool ElementOptionString::TraiterLigneFichier(const string & pLigne)
{
  string st =GetDroiteLigne(pLigne);
  if (st != "")
  {
    _Parametre = st;
    return true;
  }
  else
  {
    return false;
  }
}

string ElementOptionString::GenererLigneFichier()
{
  string res(_Nom);
  res += ' ';
  res += _Parametre;
  return res;
}

string ElementOptionString::GetValeur() const
{
  return _Parametre;
}

void ElementOptionString::SetValeur(const string & p_Valeur)
{
  _Parametre=p_Valeur;
}
