/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BonusDoubleBalle.h"
#include "TableauJeu.h"
#include <MOGL_GestionnaireObjet.h>
#include "Constante.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

MOGL_Objet * BonusDoubleBalle::_ObjetSource=NULL;

BonusDoubleBalle::BonusDoubleBalle(): Bonus()
{
  if (_ObjetSource==NULL)
  {
    PreCharger(Texture::GetGestionnaireTexture());
  }
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}


BonusDoubleBalle::BonusDoubleBalle(const BonusDoubleBalle & p_Bonus): Bonus(p_Bonus)
{
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BonusDoubleBalle::~BonusDoubleBalle()
{
  delete _ElementArbre;
}

void BonusDoubleBalle::PreCharger(MOGL_GestionnaireTexture * p_GestionnaireTexture)
{
  _ObjetSource=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bonus_doubleballe.tri").c_str(),
                                                  *p_GestionnaireTexture, *_ObjetSource);
}

bool BonusDoubleBalle::Mouvement(Plateau * p_Plateau, TableauJeu * p_Tableau)
{
  if (CollisionPlateau(p_Plateau))
  {
    unsigned int i=0;
    Balle ** tabBalle;
    tabBalle=new Balle*[p_Tableau->GetSetBalle()->size()];

    for(TableauJeu::Set_Balle::iterator itBalle=p_Tableau->GetSetBalle()->begin(); itBalle!=p_Tableau->GetSetBalle()->end(); itBalle++)
    {
      float dirX=(*itBalle)->GetDirectionX();
      float dirY=(*itBalle)->GetDirectionY();
      //float vitesse=sqrt(vitX*vitX + vitY*vitY);
      float angle=atan2(dirY, dirX);

      tabBalle[i]=new Balle(*(*itBalle));

      (*itBalle)->SetDirection(cos(angle-20*M_PI/180), sin(angle-20*M_PI/180));
      tabBalle[i]->SetDirection(cos(angle+20*M_PI/180), sin(angle+20*M_PI/180));
      i++;
    }

    unsigned int total=p_Tableau->GetSetBalle()->size();
    for(i=0; i<total; i++)
    {
      p_Tableau->AjouterBalle(tabBalle[i]);
    }
    delete [] tabBalle;
    return true;
  }
  return false;
}

