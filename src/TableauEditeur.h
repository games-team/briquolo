/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _EDITEURTABLEAU
#define _EDITEURTABLEAU

#include <MOGL_Camera.h>
#include <vector>
#include "TableauBase.h"
#include "Curseur.h"
#include "PanneauOutilsEditeur.h"
#include "PanneauLigneOptionEditeur.h"
#include "ElementTableau.h"

class TableauEditeur: public TableauBase
{
  protected:
    typedef vector<ElementTableau *> T_Vector_ElemTableau;

  protected:
    Curseur * _Curseur;
    MOGL_Camera * _Camera;
    PanneauOutilsEditeur * _PanneauOutilsEditeur;
    PanneauLigneOptionEditeur * _PanneauLigneOptionEditeur;
    float _PasX, _PasY;
    ElementTableau * _ElementCourant;
    bool _Ajout;
    bool _SaisieLigne;
    T_Vector_ElemTableau _ListSup;
    unsigned int _NbBriqueSup;
    unsigned int _CurrentSup;

  protected: // Actions
    void _ChangementAction(unsigned int p_Action);


  public:
    TableauEditeur(MOGL_Fenetre * p_Fenetre);
    ~TableauEditeur();

    bool Charger(const char * p_NomFichier);

    void Activer(bool p_Activation);
    void SetPause(bool p_Pause);
    void ClavierUp(SDL_keysym * key);
};

#endif
