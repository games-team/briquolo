/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _MENUEDITEUR
#define _MENUEDITEUR

#include <MOGL_Panneau.h>
#include <MOGL_Choix.h>
#include <MOGL_Bouton.h>
#include <MOGL_Separateur.h>
#include "MenuOption.h"
#include "MenuBase.h"
#include "Sauvegarder.h"
#include "Charger.h"

class MenuEditeur: public MenuBase
{
  public:
    MOGL_Bouton * _Reprendre;
    MOGL_Bouton * _Quitter;
    MOGL_Bouton * _Options;
    MOGL_Bouton * _Sauvegarder;
    MOGL_Bouton * _Charger;
    MOGL_Separateur * _Sep1;
    MOGL_Separateur * _Sep2;
    MenuOption * _MenuOption;
    Sauvegarder * _PanneauSauvegarder;
    Charger * _PanneauCharger;

  protected: // ACTIONS
    void _OnReprendre();
    void _OnQuitter();
    void _OnOptions();
    void _OnSauvegarder();
    void _OnCharger();

  public:
    MenuEditeur(MOGL_Fenetre * p_Fenetre);
    ~MenuEditeur();
};

#endif
