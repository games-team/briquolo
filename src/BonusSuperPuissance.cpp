/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "BonusSuperPuissance.h"
#include "TableauJeu.h"
#include <MOGL_GestionnaireObjet.h>
#include "Constante.h"

MOGL_Objet * BonusSuperPuissance::_ObjetSource=NULL;

BonusSuperPuissance::BonusSuperPuissance(): Bonus()
{
  if (_ObjetSource==NULL)
  {
    PreCharger(Texture::GetGestionnaireTexture());
  }
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BonusSuperPuissance::BonusSuperPuissance(const BonusSuperPuissance & p_Bonus): Bonus(p_Bonus)
{
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

BonusSuperPuissance::~BonusSuperPuissance()
{
  delete _ElementArbre;
}

void BonusSuperPuissance::PreCharger(MOGL_GestionnaireTexture * p_GestionnaireTexture)
{
  _ObjetSource=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/bonus_superpuissance.tri").c_str(), *p_GestionnaireTexture, *_ObjetSource);
}

bool BonusSuperPuissance::Mouvement(Plateau * p_Plateau, TableauJeu * p_Tableau)
{
  if (CollisionPlateau(p_Plateau))
  {
    for(TableauJeu::Set_Balle::iterator itBalle=p_Tableau->GetSetBalle()->begin(); itBalle!=p_Tableau->GetSetBalle()->end(); itBalle++)
    {
      (*itBalle)->SetSuperPuissance(true);
    }
    return true;
      
  }
  else
  {
    return false;
  }
}

