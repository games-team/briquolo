/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _PLATEAU
#define _PLATEAU

#include <MOGL_Objet.h>
#include <MOGL_GestionnaireTexture.h>
#include "Balle.h"
#include "ElementCollision.h"

class Plateau: public ElementCollision
{
  protected:
    const double _TailleInitX, _TailleInitY;
    double _TailleX, _TailleY;
    double _FacteurTaille;
    double _MinX, _MaxX;
    double _Vitesse;

    static MOGL_Objet * _ObjetSourceMilieu;
    static MOGL_Objet * _ObjetSourceDroite;
    static MOGL_Objet * _ObjetSourceGauche;

    MOGL_Objet * _ObjetMilieu;
    MOGL_Objet * _ObjetDroite;
    MOGL_Objet * _ObjetGauche;
    /*
      bool _IntersectionCoin(Balle * p_Balle, double p_CentreX, double p_CentreY,
      double & p_T1, double & p_ResX1, double & p_ResY1,
      double & p_T2, double & p_ResX2, double & p_ResY2);

      void _SetCollisionCoin(Balle * p_Balle, double p_CentreX, double p_CentreY,
      double p_T, double p_ResX, double p_ResY,
      Struct_Collision & p_Collision);
    */
    bool _IntersectionDeplacementPlateau(Balle * p_Balle, double p_CentreX, double p_CentreY,
                                         double p_Deplacement,
                                         double & p_T1, double & p_ResX1,
                                         double & p_T2, double & p_ResX2);
    void _SetBalleDeplacementPlateau(Balle * p_Balle, double p_CentreX, double p_CentreY);

    void _Copier(const ElementGraphique & p_ElementGraphique);
    

  public:
    Plateau();
    Plateau(const Plateau & p_Plateau);
    ~Plateau();

    Struct_Collision TesterCollision(Balle * p_Balle);
    //void CollisionDeplacement(double p_Distance, Balle * p_Balle);
    void SetVitesse(double p_Vitesse);
    void SetMinX(double p_Valeur);
    void SetMaxX(double p_Valeur);

    void SetTaille(double p_TailleX);
    void SetTailleFacteur(double p_Facteur);
    double GetTailleX() const;
    double GetTailleY() const;

    void Deplacer(double p_Temps);

    static void PreCharger();
};

#endif
