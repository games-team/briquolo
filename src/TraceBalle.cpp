/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "TraceBalle.h"
#include "MOGL_Temps.h"


TraceBalle::TraceBalle(Balle * p_Balle) 
  : Generateur(20, true, Texture::GetGestionnaireTexture()->GetTexture("TraceBalle")), _Balle(p_Balle), _SuperPuissance(false)
{
  CreerParticules(20);
}

void TraceBalle::CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                    MOGL_Struct_Vecteur & Velocite,
                                    MOGL_Struct_Couleur & Couleur,
                                    GLfloat & DureeVie,
                                    GLfloat & Taille)
{
  Position.x=RandomNeg()*.2;
  Position.y=RandomNeg()*.2;
  Position.z=0;
  Velocite.x=0;
  Velocite.y=0;
  Velocite.z=0;
  if (_SuperPuissance)
  {
    Couleur.r=.9f;
    Couleur.g=.9f;
    Couleur.b=.1f;
    DureeVie=.6*Random();
    Taille=4;
  }
  else
  {
    Couleur.r=.4f;
    Couleur.g=.1f;
    Couleur.b=.1f;
    DureeVie=.3*Random();
    Taille=2;
  }
}

void TraceBalle::MajParticule(MOGL_Struct_Vecteur & p_Position,
                                   MOGL_Struct_Vecteur & p_Velocite,
                                   MOGL_Struct_Couleur & p_Couleur)
{

  MOGL_Struct_Vecteur vec=MOGL_Temps::GetVariationVecteurDistance(p_Velocite);
  
  p_Position.x+=vec.x;
  p_Position.y+=vec.y;
  p_Position.z+=vec.z;
}


bool TraceBalle::GetSuperPuissance() const
{
  return _SuperPuissance;
}

void TraceBalle::SetSuperPuissance(bool p_Actif)
{
  _SuperPuissance=p_Actif;
}

