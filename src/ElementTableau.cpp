/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ElementTableau.h"

ElementTableau::ElementTableau(TableauBase * p_TableauBase) : ElementCollision(), _Decors(false), _TableauBase(p_TableauBase)
{
}

ElementTableau::~ElementTableau()
{
}

void ElementTableau::SetId(unsigned int p_Id)
{
  _Id=p_Id;
}

unsigned int ElementTableau::GetId() const
{
  return _Id;
}
/*
void ElementTableau::SetDerniereCollision(ElementTableau * p_Element, Balle * p_Balle)
{
  _DerniereCollision=p_Element;
  _DerniereBalle=p_Balle;
}
*/
bool ElementTableau::EstDecors() const
{
  return _Decors;
}
/*
bool ElementTableau::Toucher(Balle * p_Balle)
{
  return true;
}
*/

void ElementTableau::Maj()
{
}

void ElementTableau::InitialiserPosition()
{
  Maj();
}

TableauBase * ElementTableau::GetTableau() const
{
  return _TableauBase;
}

void ElementTableau::SetSelectionner(bool p_Selectionner)
{
  MOGL_Objet * objelem = dynamic_cast<MOGL_Objet *>(GetElementArbreMOGL());
  if (objelem != NULL)
  {
    objelem->SetDessinerBord(p_Selectionner);
  }
}

