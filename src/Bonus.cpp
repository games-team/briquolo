/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Bonus.h"
#include "BibliothequeSon.h"
#include <MOGL_Temps.h>

Bonus::Bonus()
: ElementGraphique(), _TailleX(3), _TailleY(2),_VitesseY(-5)
{
}

Bonus::Bonus(const Bonus & p_Bonus): ElementGraphique(p_Bonus)
{
  _TailleX=p_Bonus._TailleX;
  _TailleY=p_Bonus._TailleY;
  _VitesseY=p_Bonus._VitesseY;

  _MatriceRotation=p_Bonus._MatriceRotation;
}

bool Bonus::CollisionPlateau(Plateau * p_Plateau)
{
  float distance=MOGL_Temps::GetVariationDistance(_VitesseY);
  
  SetPosition(_X, _Y+distance);
  bool val=(_Y-_TailleY/2<p_Plateau->GetPositionY()+p_Plateau->GetTailleY()/2 &&
           _Y+_TailleY/2>p_Plateau->GetPositionY()-p_Plateau->GetTailleY()/2 &&
           _X>p_Plateau->GetPositionX()-p_Plateau->GetTailleX()/2-_TailleX/2-_TailleY/2 && 
           _X<p_Plateau->GetPositionX()+p_Plateau->GetTailleX()/2+_TailleX/2+_TailleY/2);

  if (val)
  {
    BibliothequeSon::SonBonus->Jouer(false);
  }
  return val;
}

void Bonus::SetPosition(float p_X, float p_Y)
{
  MOGL_MatriceTransformation mat=MOGL_MatriceTransformation::FabriqueRotation(MOGL_Temps::GetVariationAngle(-50),0,0);
  
  _MatriceRotation.Multiplier(mat);
  
  _X=p_X;
  _Y=p_Y;
  if (_ElementArbre!=NULL)
  {
    _ElementArbre->ReinitialiserPosition();
    _ElementArbre->AjouteTransformation(_MatriceRotation);
    
    _ElementArbre->AjouterTranslation(_X,_Y,0);
  }
}

float Bonus::GetTailleX() const
{
  return _TailleX;
}

float Bonus::GetTailleY() const
{
  return _TailleY;
}

