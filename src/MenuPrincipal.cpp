/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "MenuPrincipal.h"
#include "TableauBase.h"
#include "Constante.h"

#ifdef WIN32
#else
  #include <dirent.h>
  #include <sys/stat.h>
  #include <sys/types.h>
  #include <unistd.h>
#endif


MenuPrincipal::MenuPrincipal(MOGL_Fenetre * p_Fenetre): MOGL_Panneau(p_Fenetre, ("BRIQUOLO v"+Constante::GetVersion()).c_str(), 300,220)
{
  MOGL_Struct_Couleur coulTitre;
  coulTitre.r=1.0f;
  coulTitre.g=0.0f;
  coulTitre.b=0.0f;
  coulTitre.a=1;

  MOGL_Struct_Couleur coulFond;
  coulFond.r=0.0f;
  coulFond.g=0.0f;
  coulFond.b=.4f;
  coulFond.a=.3f;
  MOGL_Struct_Couleur coulSel;
  coulSel.r=.32f;
  coulSel.g=.74f;
  coulSel.b=1.0f;
  coulSel.a=.6f;

  SetCouleurFond(coulFond);
  SetCouleurLabel(coulTitre);
  BoutonJeu=new MOGL_Bouton(this,_("Play !"));
  BoutonJeu->SetCouleurFondFocus(coulSel);

  BoutonEditeur=new MOGL_Bouton(this, _("Level editor"));
  BoutonEditeur->SetCouleurFondFocus(coulSel);

  BoutonOptions=new MOGL_Bouton(this, _("Settings"));
  BoutonOptions->SetCouleurFondFocus(coulSel);
  BoutonOptions->Activation.Connecter(this, &MenuPrincipal::_OnOptions);

  BoutonQuitter=new MOGL_Bouton(this, _("Quit"));
  BoutonQuitter->SetCouleurFondFocus(coulSel);

  PanneauOption=new MenuOption(this);
  SetUtiliserEscape(false);


  PanneauChoixTableau=new MOGL_Panneau(this, _("Choose starting level"), 300, 200);
  BoutonJeu->Activation.Connecter(this, &MenuPrincipal::_OnChoisirTableauDepart);
  

  coulTitre.r=0.84f;
  coulTitre.g=0.88f;
  coulTitre.b=1.0f;
  coulTitre.a=1;
  PanneauChoixTableau->SetCouleurLabel(coulTitre);
  PanneauChoixTableau->SetCouleurFond(coulFond);

  ChoixGroupe=new MOGL_Choix(PanneauChoixTableau, _("Group"));
  ChoixGroupe->SetCouleurFondFocus(coulSel);
  ChoixGroupe->ChangementElement.Connecter(this, &MenuPrincipal::_OnChangementGroupe);

  ChoixTableau=new MOGL_Choix(PanneauChoixTableau, _("Name"));
  ChoixTableau->SetCouleurFondFocus(coulSel);

  Sep=new MOGL_Separateur(PanneauChoixTableau);

  Ok=new MOGL_Bouton(PanneauChoixTableau, _("Ok"));
  Ok->SetCouleurFondFocus(coulSel);;

  Annuler=new MOGL_Bouton(PanneauChoixTableau, _("Cancel"));
  Annuler->Activation.Connecter(this, &MenuPrincipal::_OnAnnuler);
  Annuler->SetCouleurFondFocus(coulSel);
}

MenuPrincipal::~MenuPrincipal()
{
  delete BoutonJeu;
  delete BoutonEditeur;
  delete BoutonOptions;
  delete BoutonQuitter;
  delete PanneauOption;

  delete PanneauChoixTableau;
  delete ChoixTableau;
  delete ChoixGroupe;
  delete Sep;
  delete Ok;
  delete Annuler;
}

void MenuPrincipal::_OnOptions()
{
  PanneauOption->SetActivation(true);
}

void MenuPrincipal::_OnChoisirTableauDepart()
{
  PanneauChoixTableau->SetActivation(true);
}

void MenuPrincipal::_OnAnnuler()
{
  PanneauChoixTableau->SetActivation(false);
}

void MenuPrincipal::_OnChangementGroupe(unsigned int p_NumGrp)
{
  ChoixTableau->ViderElementsChoix();
  set<string> liste=TableauBase::GetListeTableau(TableauBase::GetRepGroupeFromId(p_NumGrp));
  for(set<string>::iterator it=liste.begin(); it!=liste.end(); it++)
  {
    ChoixTableau->AjouterElementChoix(*it);
  }
  ChoixTableau->SetIndiceCourant(0);
}

void MenuPrincipal::SetActivation(bool p_Activation)
{
  ChoixGroupe->ViderElementsChoix();
  list<string> liste=TableauBase::GetListeGroupeTableau();
  for(list<string>::iterator it=liste.begin(); it!=liste.end(); it++)
  {
    ChoixGroupe->AjouterElementChoix(*it);
  }

  ChoixTableau->ViderElementsChoix();

  set<string> ensemble=TableauBase::GetListeTableau(TableauBase::GetRepGroupeFromId(ChoixGroupe->GetIndiceCourant()));
  for(set<string>::iterator it=ensemble.begin(); it!=ensemble.end(); it++)
  {
    ChoixTableau->AjouterElementChoix(*it);
  }

  MOGL_Panneau::SetActivation(p_Activation);
}
