/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _MENUBASE
#define _MENUBASE

#include <MOGL_Panneau.h>
#include <MOGL_Choix.h>
#include <MOGL_Bouton.h>
#include <MOGL_Separateur.h>
#include "MenuOption.h"

class MenuBase: public MOGL_Panneau
{
  protected:
    bool _DemandeQuitter;

  protected: // ACTIONS
    void _OnReprendre();
    void _OnQuitter();

  public: // SIGNAUX
    MOGL_Signal0 FinMenuBase;

  public:
    MenuBase(MOGL_Fenetre * p_Fenetre);
    bool GetQuitter() const;
    void SetActivation(bool p_Activation);
};

#endif
