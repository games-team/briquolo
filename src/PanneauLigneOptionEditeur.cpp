/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include "PanneauLigneOptionEditeur.h"
#include "Gestionnaire.h"
#include "Constante.h"

PanneauLigneOptionEditeur::PanneauLigneOptionEditeur(MOGL_Fenetre * p_Fenetre): MOGL_Panneau(p_Fenetre, _("Option line"), 360, 60),
                                                                                _Police(10), _Pause(false)
{
  _AlignementHorizontal=CENTRE;
  _AlignementVertical=MAX;

  _Police.Charger((Constante::GetGlobalDir()+Constante::GetDataDir()+_("/DejaVuSans.ttf")).c_str());

  SetPolice(&_Police);

  _LabelLigne = new MOGL_Label(this, _LigneOption);
  _LabelLigne->SetPolice(&_Police);

  _SaisieLigne = new MOGL_SaisieChaine(this, _LigneOption);
  _SaisieLigne->SetPolice(&_Police);
  _SaisieLigne->SetCacher(true);
  _SaisieLigne->SetSaisieFichier(false);

  SetUtiliserEscape(false);

  p_Fenetre->ChangementMode.Connecter(this, &PanneauLigneOptionEditeur::_OnChangementResolution);
}

void PanneauLigneOptionEditeur::SetPause(bool p_Pause)
{
  _Pause=p_Pause;
}

bool PanneauLigneOptionEditeur::GetPause() const
{
  return _Pause;
}

void PanneauLigneOptionEditeur::ClavierUp(SDL_keysym * key)
{
  if (!_Pause)
  {
    MOGL_Panneau::ClavierUp(key);
    if (key->sym == SDLK_F12)
    {
      _LabelLigne->SetCacher(true);
      _SaisieLigne->SetCacher(false);
      SetFocus(_SaisieLigne);
    }
  }
}

void PanneauLigneOptionEditeur::_OnChangementResolution(int p_ResolutionX, int p_ResolutionY, int p_BitParPixel)
{
  _Police.Recharger();
}

void PanneauLigneOptionEditeur::SetLigneOption(const string & p_LigneOption)
{
  _LabelLigne->SetLabel(p_LigneOption);
}

void PanneauLigneOptionEditeur::SetEditMode(bool p_EditMode)
{
  if (p_EditMode)
  {
    _SaisieLigne->SetChaine(_LabelLigne->GetLabel());
    _LabelLigne->SetCacher(true);
    _SaisieLigne->SetCacher(false);
    SetFocus(_SaisieLigne);
  }
  else
  {
    _LabelLigne->SetCacher(false);
    _SaisieLigne->SetCacher(true);
  }
}

void PanneauLigneOptionEditeur::ValiderLigne()
{
  _LabelLigne->SetLabel(_SaisieLigne->GetChaine());
}
