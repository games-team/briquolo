/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Constante.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#else
#define DATADIR_BRIQUOLO "./"
#define PACKAGE "briquolo"
#endif

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif

#ifdef WIN32
  string Constante::_Local("");
  string Constante::_Global("");
  string Constante::_Data("");
  string Constante::_Tableau("");
  string Constante::_Config("");
  string Constante::_NomPackage("");
  string Constante::_DirLocale("");
  string Constante::_NumVersion("");
  bool Constante::_Init(false);
#else
  string Constante::_Local("");
  string Constante::_Global("");
  string Constante::_Data("");
  string Constante::_Tableau("");
  string Constante::_Config("");
  string Constante::_NomPackage("");
  string Constante::_DirLocale("");
  string Constante::_NumVersion("");
  bool Constante::_Init(false);
#endif

void Constante::_InitialiserVariables()
{

#ifdef WIN32

  string dir1(".");
  // We'll read the user profile location directly
  // from registry, since Win9x doesn't have the
  // USERPROFILE environment variable.
  //  -- Laurentiu Pancescu <lpancescu@fastmail.fm>
  LONG returnStatus;
  DWORD dwType = REG_SZ, dwSize = 255;
  char lszValue[255];
  HKEY hKey;
  returnStatus = RegOpenKeyEx(HKEY_CURRENT_USER,
                              "Software\\Microsoft\\Windows"
                              "\\CurrentVersion\\Explorer\\Shell Folders", 0L,
                              KEY_READ, &hKey);
  if (returnStatus == ERROR_SUCCESS)
  {
     returnStatus = RegQueryValueEx(hKey, "AppData", NULL, &dwType,
                                    (LPBYTE)&lszValue, &dwSize);
     if (returnStatus == ERROR_SUCCESS)
     {
       dir1 = lszValue;
     }
     RegCloseKey(hKey);
  }

  string dir2("/Briquolo");

  _Local=dir1+dir2;
  _Global=".";
  _Data="/data";
  _Tableau="/tableau";
  _Config="Config";
  _NomPackage="briquolo";
  _DirLocale="./Locale";
  _NumVersion=VERSION;

  _Init=true;

#else

  string dir1(getenv("HOME"));
  string dir2("/."PACKAGE);
  _Local=dir1+dir2;

  _NumVersion=VERSION;
  _Config="config";
  _NomPackage=PACKAGE;


  string fichierTemoin(DATADIR_BRIQUOLO"/");
  fichierTemoin+=_NumVersion;
  if (access(fichierTemoin.c_str(), F_OK) == 00)
  {
    // Le fichier t�moin existe, cela signifie que l'on a fait le make install
    // On peut prendre les fichier de donn�es dans le rep d'install
    _Global=DATADIR_BRIQUOLO;
    _DirLocale=LOCALEDIR;
  }
  else
  {
    // Le fichier t�moin n'existe pas, cela signifie que l'on n'a pas fait le make install
    // On doit prendre les fichier de donn�es dans le rep des sources
    _Global=SRCTOPDIR"/data";
    _DirLocale=SRCTOPDIR"/po";
  }
  _Data="/data";
  _Tableau="/tableau";

  _Init=true;

#endif
}

string Constante:: GetLocalDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _Local;
}

string Constante:: GetGlobalDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _Global;
}

string Constante:: GetDataDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _Data;
}

string Constante:: GetTableauDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _Tableau;
}

string Constante:: GetNomConfig()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _Config;
}

string Constante:: GetNomPackage()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _NomPackage;
}

string Constante:: GetLocaleDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _DirLocale;
}

string Constante:: GetVersion()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _NumVersion;
}

