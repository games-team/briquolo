/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ElementOptionInt.h"
#include <iostream>

ElementOptionInt::ElementOptionInt(const string & p_Nom, int p_ParametreDefaut)
  : ElementOption(p_Nom), _Parametre(p_ParametreDefaut)
{
}


bool ElementOptionInt::TraiterLigneFichier(const string & pLigne)
{
  stringstream st(GetDroiteLigne(pLigne));
  if (st.str() != "")
  {
    st>>_Parametre;
    return true;
  }
  else
  {
    return false;
  }
}

string ElementOptionInt::GenererLigneFichier()
{
  stringstream res;
  res<<_Nom<<' '<<_Parametre;
  return res.str();
}

int ElementOptionInt::GetValeur() const
{
  return _Parametre;
}

void ElementOptionInt::SetValeur(int p_Valeur)
{
  _Parametre=p_Valeur;
}
