/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "I18n.h"
#include <iostream>
#include "Constante.h"

#ifdef WIN32
  #include <windows.h>
  #include <winnt.h>
  #include <winnls.h>
#endif


static string stIdCountry;

void DefinirLanguage()
{
#ifdef WIN32
    /* set LANG by hand if it is not set */
    if (!getenv("LANG"))
    {
        char *langname = NULL;

        switch (PRIMARYLANGID(LANGIDFROMLCID(GetUserDefaultLCID())))
        {
            case LANG_BELARUSIAN:
                langname = "be";
                break;
            case LANG_GERMAN:
                langname = "de";
                break;
            case LANG_ENGLISH:
                langname = "en";
                break;
            case LANG_FRENCH:
                langname = "fr";
                break;
            case LANG_DUTCH:
                langname = "nl";
                break;
            case LANG_POLISH:
                langname = "pl";
                break;
            case LANG_PORTUGUESE:
                langname = "pt";
                break;
            case LANG_RUSSIAN:
                langname = "ru";
                break;
            case LANG_SLOVAK:
                langname = "sk";
                break;
            default:
                cout<<"Unknown locale"<<endl;

        }
        if (langname)
        {
            static char envstr[40];

            snprintf(envstr, sizeof(envstr), "LANG=%s", langname);
            putenv(envstr);
        }
    }

    //setlocale(LC_ALL, "");

    bindtextdomain(Constante::GetNomPackage().c_str(), Constante::GetLocaleDir().c_str());
    textdomain (Constante::GetNomPackage().c_str());

    bind_textdomain_codeset(Constante::GetNomPackage().c_str(), "UTF-8");

#else
    char * ch = setlocale (LC_MESSAGES, "");
    if (ch==NULL || setlocale (LC_CTYPE, "")==NULL)
    {
        cout<<"Unknown locale"<<endl;
    }

    if (ch != NULL)
    {
        stIdCountry = ch;
    }
    bindtextdomain(Constante::GetNomPackage().c_str(), Constante::GetLocaleDir().c_str());
    textdomain (Constante::GetNomPackage().c_str());
    bind_textdomain_codeset(Constante::GetNomPackage().c_str(), "UTF-8");
#endif
}

string GetIdCountry()
{
  return stIdCountry.substr(0,2);
}

