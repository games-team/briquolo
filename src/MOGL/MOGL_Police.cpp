/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "../I18n.h"
#include "MOGL_Police.h"
#include "config.h"

MOGL_Police::MOGL_Police(): _LargeurMax(0), _HauteurMax(0)
{
  // Since the conversion result is read as an Uint16 array, we must be
  // careful about endian dependency. Systems using GNU glibc seem to do
  // the right thing when using just "UCS-2", but systems using libiconv,
  // such as MinGW, will misbehave.
  //   -- Laurentiu Pancescu <lpancescu@fastmail.fm>
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
  _IconvDesc = iconv_open("UCS-2LE", "UTF-8");
#else
  _IconvDesc = iconv_open("UCS-2BE", "UTF-8");
#endif

  if (_IconvDesc == (iconv_t)(-1))
  {
    cerr<<"Unicode descriptor conversion error"<<endl;
  }
}

MOGL_Police::~MOGL_Police()
{
  iconv_close(_IconvDesc);
}

void MOGL_Police::Afficher(const string & p_Chaine, int p_X, int p_Y) const
{
  MOGL_Struct_Couleur couleur;
  couleur.r=1;
  couleur.g=1;
  couleur.b=1;
  couleur.a=1;

  Afficher(p_Chaine, p_X, p_Y, couleur);
}

unsigned int MOGL_Police::GetTailleXMax() const
{
  return _LargeurMax;
}

unsigned int MOGL_Police::GetTailleYMax() const
{
  return _HauteurMax;
}

unsigned int MOGL_Police::GetTailleXChaine(const string & p_Chaine) const
{
  unsigned int size;
  Uint16 * ucsSt = _AllocAndConvertToUCS2(p_Chaine, size);
  unsigned int taille=0;
  for(unsigned int i=0; i<size; i++)
  {
    taille+=GetTailleXCaractere(ucsSt[i]);
  }
  return taille;
}


Uint16 * MOGL_Police::_AllocAndConvertToUCS2(string p_Chaine, unsigned int & size) const
{
  // Transformation utf-8 > ucs-2
  // Alloc
  char * cStr = new char[p_Chaine.size()+1];
  char * unicStr = new char[p_Chaine.size()*2*2];
  Uint16 * stToRet = (Uint16 *)unicStr;
  // prep
  size_t sizeSrc = p_Chaine.size();
  strncpy(cStr, p_Chaine.c_str(), sizeSrc);
  
  size_t sizeDest = p_Chaine.size() * 2;
  size = sizeDest;
  // Conv
  iconv(_IconvDesc, (ICONV_CONST char **)&cStr, &sizeSrc, &unicStr, &sizeDest);
  size = (size - sizeDest) / 2;

  return stToRet;
}

unsigned int MOGL_Police::_ReadUnsignedInt(ifstream & file)
{
    unsigned int val;
    file.read((char *) &val, sizeof (val));
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
    return val;
#else
    SDL_Swap32(val);
    return val;
#endif
}
