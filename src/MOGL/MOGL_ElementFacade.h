/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ELEMENTFACADE
#define MOGL_ELEMENTFACADE

class MOGL_ElementFacade
{
  protected:
    bool _Cacher;

  public:
    /**
       Constructeur
     */
    MOGL_ElementFacade();

    /**
       Destructeur.
    */
    virtual ~MOGL_ElementFacade();

    /**
       M�thode appell�e pour afficher l'�l�ment de la facade.
       Cette m�thode est appel�e par MOGL_Facade.
     */
    virtual void Afficher()=0;

    /**
       Indique si l'�l�ment est cach�e.
       @return <i>true</i> si l'�l�ment est cach�e, <i>false</i> sinon.
     */
    virtual bool GetCacher() const;

    /**
       Permet de d�finir si l'�l�ment doit �tre cach�e ou non.
       @param p_Cacher : <i>true</i> pour cacher l'�l�ment, <i>false</i> sinon.
     */
    virtual void SetCacher(bool p_Cacher);

};

#endif
