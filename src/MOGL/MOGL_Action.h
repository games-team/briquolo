/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ACTION
#define MOGL_ACTION

#include <list>

using namespace std;

class MOGL_SignalBase;

/**
   Repr�sente une action qui peut �tre appel�e lorsqu'un MOGL_Signal est �mis.
   Chaque classe devant �tre connect�e � un MOGL_Signal doit donc �tre une
   sp�cialisation de cette classe.
*/
class MOGL_Action
{
  protected:
        // **** Types locaux � la classe ****
        /**
           Liste de pointeurs vers MOGL_SignalBase.
         */
        typedef list<MOGL_SignalBase *> MOGL_List_SignalBase;
        
        // **********************************
  protected:
        /**
           Permet de stocker la liste des signaux attach�s � cette action.
        */
        MOGL_List_SignalBase _ListSignalBase;

  public:
        /**
           Constructeur.
        */
        MOGL_Action();
        
        /**
                Constructeur par copie.
        */
        MOGL_Action(const MOGL_Action & p_Action);

        /**
                Destructeur.
        */
        virtual ~MOGL_Action();

        /**
                Red�finition de l'op�rateur =
        */
        MOGL_Action & operator = (const MOGL_Action & p_Action);

        /**
                Permet d'ajouter un signal � la liste des signaux attach�s
                � l'action.
        */
        void AjouterSignal(MOGL_SignalBase * p_SignalBase);

        /**
                Permet de retirer un signal � la liste des signaux attach�s
                � l'action.
        */
        void RetirerSignal(MOGL_SignalBase * p_SignalBase);
        
        /**
                Permet de demander � tous les signaux attach�s � l'action de se d�connecter.
                Les signaux sont alors retir� de la liste.
        */
        void DeconnecterSignaux();
        
        
};

#endif
