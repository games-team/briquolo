/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_ImageFacade.h"
#include <stdlib.h>
#include <cmath>
#include <iostream>

MOGL_ImageFacade::MOGL_ImageFacade(bool p_AutoriserTrou)
  : MOGL_ElementFacade(), MOGL_Image(p_AutoriserTrou), _Initialise(false), _Texture(true), _Nom(0)
{
}

bool MOGL_ImageFacade::_Puissance2(unsigned int p_Nombre, unsigned int & p_NombrePuissance)
{
  for(int i=0;; i++)
  {
    p_NombrePuissance=static_cast<unsigned int>(pow(static_cast<float>(2), i));
    if (p_NombrePuissance==p_Nombre)
    {
      return true;
    }
    else if (p_NombrePuissance>p_Nombre)
    {
      return false;
    }
  }
}

void MOGL_ImageFacade::Afficher()
{
  if (!_Initialise)
  {
    _Initialise=true;
    GLint tmax;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &tmax);
    if (_TailleX<=static_cast<unsigned int>(tmax) && _TailleY<=static_cast<unsigned int>(tmax))
    {
      _Texture=true;
      if (_Nom==0)
      {
        glGenTextures(1,&_Nom);
      }

      glBindTexture(GL_TEXTURE_2D,_Nom);
      unsigned int nouveauTailleX, nouveauTailleY;
      bool b1=_Puissance2(_TailleX, nouveauTailleX);
      bool b2=_Puissance2(_TailleY, nouveauTailleY);
      if (!b1 || !b2)
      {
        MOGL_Struct_Couleur * imageNouveau=new MOGL_Struct_Couleur[nouveauTailleX*nouveauTailleY];
        for(unsigned int y=0; y<nouveauTailleY; y++)
        {
          for(unsigned int x=0; x<nouveauTailleX; x++)
          {
            if (x<_TailleX && y<_TailleY)
            {
              imageNouveau[y*nouveauTailleX+x]=_Image[y*_TailleX+x];
            }
            else
            {
              MOGL_Struct_Couleur & coul=imageNouveau[y*nouveauTailleX+x];
              coul.r=0;
              coul.g=0;
              coul.b=0;
              coul.a=1;
            }
          }
        }
        _RecX=static_cast<float>(_TailleX)/nouveauTailleX;
        _RecY=static_cast<float>(_TailleY)/nouveauTailleY;
        glTexImage2D(GL_TEXTURE_2D,0,4,nouveauTailleX,nouveauTailleY,0,GL_RGBA,GL_FLOAT,imageNouveau);
      }
      else
      {
        _RecX=_RecY=1;
        glTexImage2D(GL_TEXTURE_2D,0,4,_TailleX,_TailleY,0,GL_RGBA,GL_FLOAT,_Image);
      }

      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
    }
    else
    {
      _Texture=false;
    }
  }

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_DEPTH_TEST);

  if (_Texture)
  {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _Nom);
    glColor4f(1,1,1,1);
    glBegin(GL_QUADS);
    glTexCoord2f(0,_RecY);
    glVertex2f(_PositionX, _PositionY);
    glTexCoord2f(0,0);
    glVertex2f(_PositionX, _PositionY+_TailleY);
    glTexCoord2f(_RecX,0);
    glVertex2f(_PositionX+_TailleX, _PositionY+_TailleY);
    glTexCoord2f(_RecX,_RecY);
    glVertex2f(_PositionX+_TailleX, _PositionY);
    glEnd();
  }
  else
  {
    glDisable(GL_TEXTURE_2D);
    glRasterPos2i(_PositionX, _PositionY+_TailleY);
    glDrawPixels(_TailleX, _TailleY, GL_RGBA, GL_FLOAT, _Image);
  }
}

void MOGL_ImageFacade::SetPosition(unsigned int p_PositionX, unsigned int p_PositionY)
{
  _PositionX=p_PositionX;
  _PositionY=p_PositionY;
}

unsigned int MOGL_ImageFacade::GetPositionX() const
{
  return _PositionX;
}

unsigned int MOGL_ImageFacade::GetPositionY() const
{
  return _PositionY;
}

bool MOGL_ImageFacade::Recharger()
{
  _Initialise=false;
  return MOGL_Image::Recharger();
}
