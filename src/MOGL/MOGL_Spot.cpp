/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Spot.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

void MOGL_Spot::SetDirection(const MOGL_Struct_Vecteur & p_Direction)
{
  float norm=p_Direction.x*p_Direction.x+p_Direction.y*p_Direction.y+p_Direction.z*p_Direction.z;
  _Direction.x=p_Direction.x/norm;
  _Direction.y=p_Direction.y/norm;
  _Direction.z=p_Direction.z/norm;
}

void MOGL_Spot::SetCutOff(GLfloat p_CutOff)
{
  if (p_CutOff<0)
    p_CutOff=0;
  else if (p_CutOff>90)
    p_CutOff=90;
  _CutOff=p_CutOff;
}

void MOGL_Spot::SetExposant(GLfloat p_Exposant)
{
  if (p_Exposant<0)
    p_Exposant=0;
  else if (p_Exposant>128)
    p_Exposant=128;
  _Exposant=p_Exposant;
}

void MOGL_Spot::TnL(MOGL_Afficheur * p_Afficheur)
{
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  _MatriceFinale.PrendreDeOpenGL();

  glLightfv(_NumLight,GL_DIFFUSE,(float*)&_Diffuse);
  glLightfv(_NumLight,GL_SPECULAR,(float*)&_Specular);
  glLightfv(_NumLight,GL_AMBIENT,(float*)&_Ambient);
  glEnable(_NumLight);

  int Pos[4]={0,0,0,1};
  glLightiv(_NumLight,GL_POSITION,Pos);
  glLightfv(_NumLight,GL_SPOT_DIRECTION,(GLfloat*)&_Direction);
  glLightf(_NumLight,GL_SPOT_CUTOFF,_CutOff);
  glLightf(_NumLight,GL_SPOT_EXPONENT,_Exposant);

  MOGL_ItSet_ElementArbre it;
  for(it=_SetElement.begin();it!=_SetElement.end();it++)
  {
    (*it)->TnL(p_Afficheur);
  }
  glPopMatrix();  
}

MOGL_Spot::~MOGL_Spot()
{
}
