/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Constante.h"

#ifdef HAVE_CONFIG_H
  #include "config.h"
#else
  #define DATADIR_BRIQUOLO "."
#endif

#ifdef WIN32
#else
#include <unistd.h>
#endif


const int MOGL_Constante::NB_TRIANGLE_TABLEAU_MAX=100;
string MOGL_Constante::_DataDir("");
bool MOGL_Constante::_Init(false);

void MOGL_Constante::_InitialiserVariables()
{
#ifdef WIN32
  _DataDir="./data";
#else
  string fichierTemoin(DATADIR_BRIQUOLO"/");
  fichierTemoin+=VERSION;
  if (access(fichierTemoin.c_str(), F_OK) == 00)
  {
    _DataDir=DATADIR_BRIQUOLO"/data";
  }
  else
  {
    _DataDir=SRCTOPDIR"/data/data";
  }
#endif
}

string MOGL_Constante::GetDataDir()
{
  if (!_Init)
  {
    _InitialiserVariables();
  }
  return _DataDir;
}
