/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_LENSFLAREFOND
#define MOGL_LENSFLAREFOND

#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "MOGL_LensFlare.h"

/**
   Classe de base de gestion d'un lens flare de fond. Un lens flare de fond a la particularit� de ne pas chager de 
   taille selon la distance entre la source du lens flare et la cam�ra. Pour cela la position du lensflare ne sert � rien,
   seul la direction de la source du lens flare a de l'importance. Cette direction est indiqu� � la construction de l'objet :
   MOGL_Lensflarefond().
 */
class MOGL_LensFlareFond: public MOGL_LensFlare
{
  protected:
        const MOGL_Struct_VecteurHomogene & _Direction;
        
  public:
        /**
           Constructeur. Permet de d�finir la direction du lens flare. La position du lens flare
           #_xPos, #_yPos, #_zPos est alors d�termin�e automatiquement d'apr�s la position de la cam�ra, de mani�re
           � ce que la distance soit constante.
           @param p_Direction : direction de la source du lens flare.
         */
        MOGL_LensFlareFond(const MOGL_Struct_VecteurHomogene & p_Direction);

        void TnL(MOGL_Afficheur * p_Afficheur);
};

#endif
