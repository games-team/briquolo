/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_GENERATEURPARTICULE
#define MOGL_GENERATEURPARTICULE

#include <list>
#include <sys/timeb.h>



#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include "MOGL_Particule.h"
#include "MOGL_Structure.h"
#include "MOGL_Noeud.h"
#include "MOGL_Texture.h"
#include "MOGL_Signal.h"


using namespace std;

class MOGL_GenerateurParticule: public MOGL_Noeud
{
  protected:
    // Pour l'affichage et les textures
    int _IdentifiantAbonnement;
    GLfloat * _TabPoint;
    GLfloat * _TabCouleur;
    GLfloat * _TabTexture;
    unsigned int _NbParticuleMax;
    unsigned int _NbParticule;
    unsigned int _ParticuleACreer;
    MOGL_Particule (*(* _TabParticule));
    bool _Recreer;
    MOGL_Texture * _Texture;
    MOGL_MatriceTransformation _MatriceFinale;
    MOGL_MatriceTransformation _MatriceSansCamera;

    MOGL_Camera * _Camera;
    bool _EstInactif;

  protected:
    float Random();             // r�el entre 0 et 1
    float RandomNeg();  // r�el entre -1 et 1

    virtual void CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                    MOGL_Struct_Vecteur & Velocite,
                                    MOGL_Struct_Couleur & Couleur,
                                    GLfloat & DureeVie,
                                    GLfloat & Taille)=0;

    virtual void MajParticule(MOGL_Struct_Vecteur & p_Position,
                              MOGL_Struct_Vecteur & p_Velocite,
                              MOGL_Struct_Couleur & p_Couleur)=0;

  public:  // SIGNAUX
    /**
     * Signal �mit lorsque plus aucune particule n'est active et lorsqu'il n'y en a pas en attente d'emission.
     */
    MOGL_Signal0 GenerateurInactif;


  public:
    MOGL_GenerateurParticule(unsigned int p_NbParticulesMax,
                             bool p_Recreer,
                             MOGL_Texture * p_Texture);

    ~MOGL_GenerateurParticule();

    void CreerParticules(unsigned int p_NbParticule);
    void TnL(MOGL_Afficheur * p_Afficheur);
                

};

#endif
