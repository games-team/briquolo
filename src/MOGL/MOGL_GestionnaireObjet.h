/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_GESTIONNAIREOBJET
#define MOGL_GESTIONNAIREOBJET

#include <vector>
#include <iostream>
#include <stdio.h>

#include "MOGL_Triangle.h"
#include "MOGL_Objet.h"
#include "MOGL_GestionnaireTexture.h"

using namespace std;

class MOGL_GestionnaireObjet
{
    // **** Types locaux � la classe ****
    typedef vector <MOGL_Struct_Point> MOGL_Liste_Point;
    typedef MOGL_Liste_Point::iterator MOGL_ItListe_Point;

  public :
    static bool ChargerObjetASCTriangle(const char * p_NomFichier, const MOGL_GestionnaireTexture & p_GM, MOGL_Objet & p_Objet);
    static bool ChargerObjetASCStrip(const char * p_NomFichier, const MOGL_GestionnaireTexture & p_GM, MOGL_Objet & p_Objet);
};

#endif
