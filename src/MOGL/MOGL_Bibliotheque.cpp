/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Bibliotheque.h"
#include "MOGL_Constante.h"
#include <libintl.h>

#define _(st) gettext (st)

MOGL_Bibliotheque * MOGL_Bibliotheque::_Singleton=NULL;

MOGL_Bibliotheque::MOGL_Bibliotheque(): MOGL_Action(), _Police(16, MOGL_TTF_STYLE_BOLD)
{
  MOGL_Texture * tex;
  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  /* tex->SetGestionModeAlpha(MOGL_Texture::ALPHA_FICHIER);
         tex->SetGestionModeRGB(MOGL_Texture::RGB_FICHIER);*/
  
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Flare1.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("flare1", tex);
  
  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Flare2.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("flare2", tex);
  
  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Flare3.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("flare3", tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Flare4.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("flare4", tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  tex->SetAnimation(true);
  tex->SetVitesseRotation(10);
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Shine1.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("shine1", tex);

  tex=new MOGL_Texture(MOGL_Texture::TEXTURE_TRANSPARENTE);
  _ListeTexture.push_back(tex);
  tex->SetGestionModeAlpha(MOGL_Image::ALPHA_MOYENNE_RGB);
  tex->SetGestionModeRGB(MOGL_Image::RGB_UN);
  tex->ChargerBMP((MOGL_Constante::GetDataDir()+"/Shine2.bmp").c_str());
  _GestionnaireTexture.AjouterTextureObjet("shine2", tex);
  
  
  _Police.Charger((MOGL_Constante::GetDataDir()+_("/DejaVuSans.ttf")).c_str());
}

MOGL_Bibliotheque::~MOGL_Bibliotheque()
{
  for(list<MOGL_Texture *>::iterator it=_ListeTexture.begin(); it!=_ListeTexture.end(); it++)
  {
        delete *it;
  }
}

MOGL_Bibliotheque * MOGL_Bibliotheque::GetSingleton()
{
  if (_Singleton==NULL)
  {
        _Singleton=new MOGL_Bibliotheque;
  }
  return _Singleton;
}

void MOGL_Bibliotheque::Init()
{
  if (_Singleton==NULL)
  {
        _Singleton=new MOGL_Bibliotheque;
  }
}



const MOGL_GestionnaireTexture & MOGL_Bibliotheque::GetGestionnaireTexture() const
{
  return _GestionnaireTexture;
}

const MOGL_PoliceTTF & MOGL_Bibliotheque::GetPolice() const
{
  return _Police;
}

void MOGL_Bibliotheque::ReinitialiserSurChangementModeFenetre(MOGL_Fenetre * p_Fenetre)
{
  p_Fenetre->ChangementMode.Connecter(this, &MOGL_Bibliotheque::_RechargerTextures);
}

void MOGL_Bibliotheque::_RechargerTextures(int, int, int)
{
  _GestionnaireTexture.RechargerTextures();
  _Police.Recharger();
}
