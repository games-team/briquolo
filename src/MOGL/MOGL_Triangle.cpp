/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Triangle.h"

MOGL_Triangle::MOGL_Triangle(const MOGL_Struct_Point & p_Point1,
                                                         const MOGL_Struct_Point & p_Point2,
                                                         const MOGL_Struct_Point & p_Point3,
                                                         MOGL_Texture * p_Texture)
  : _Point1(p_Point1), _Point2(p_Point2), _Point3(p_Point3),
        _Texture(p_Texture)
{
}

MOGL_Texture * MOGL_Triangle::GetTexture() const
{
  return _Texture;
}

MOGL_Struct_Point MOGL_Triangle::GetPoint(int NumPoint) const
{
  switch (NumPoint)
  {
        case 0 :
        {
          return _Point1;
        }
        break;
        case 1 :
        {
          return _Point2;
        }
        break;
        default :
        {
          return _Point3;
        }
        break;
  }
}
