/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_BOUTON
#define MOGL_BOUTON

#include "MOGL_ElementPanneau.h"
#include "MOGL_Signal.h"
#include <string>

using namespace std;

class MOGL_Bouton: public MOGL_ElementPanneau
{
  protected:
    string _Label;
    MOGL_Struct_Couleur _CouleurTexte;

  public: // SIGNAUX
    MOGL_Signal0  Activation;

  public:
    MOGL_Bouton(MOGL_Panneau * p_Panneau, const string & p_Label);
    void SetLabel(const string & p_Label);
    string GetLabel() const;

    void Afficher();
    unsigned int GetHauteurElement() const;
    unsigned int GetLargeurElement() const;
    void SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur);
    MOGL_Struct_Couleur GetCouleurLabel() const;


    void ClavierUp(SDL_keysym * key);
};

#endif
