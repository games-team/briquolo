/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TRIANGLE
#define MOGL_TRIANGLE

#include "MOGL_Structure.h"
#include "MOGL_Texture.h"

/**
   C'est un triangle classique qui utilise des points o� sont indiqu�es les coordonn�es dans l'espace,
   les coordonn�es de texture, et les coordonn�es de normale.
*/
class MOGL_Triangle
{
  protected :
    MOGL_Struct_Point _Point1, _Point2, _Point3;
    MOGL_Texture * _Texture;
  public :
    MOGL_Triangle(const MOGL_Struct_Point & p_Point1,
                  const MOGL_Struct_Point & p_Point2,
                  const MOGL_Struct_Point & p_Point3,
                  MOGL_Texture * p_Texture);

    MOGL_Texture * GetTexture() const;
    MOGL_Struct_Point GetPoint(int NumPoint) const;
};

#endif
