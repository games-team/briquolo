/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_GENPARTFEU
#define MOGL_GENPARTFEU

#include "MOGL_GenerateurParticule.h"

class MOGL_GenPartFeu: public MOGL_GenerateurParticule
{
  protected:
    void CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                            MOGL_Struct_Vecteur & Velocite,
                            MOGL_Struct_Couleur & Couleur,
                            GLfloat & DureeVie,
                            GLfloat & Taille);

    void MajParticule(  MOGL_Struct_Vecteur & p_Position,
                        MOGL_Struct_Vecteur & p_Velocite,
                        MOGL_Struct_Couleur & p_Couleur);

                

  public:
    MOGL_GenPartFeu(unsigned int p_NbParticulesMax,
                    bool p_Recreer,
                    MOGL_Texture * p_Texture);
};

#endif
