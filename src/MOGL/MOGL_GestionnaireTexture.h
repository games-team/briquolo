/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_GESTIONNAIRETEXTURE
#define MOGL_GESTIONNAIRETEXTURE

#include <map>
#include <string>

#include "MOGL_Texture.h"

using namespace std;

class MOGL_GestionnaireTexture
{
        // **** Types locaux � la classe ****
        typedef map <string, MOGL_Texture *> MOGL_Map_StringTexture;
        typedef MOGL_Map_StringTexture::iterator MOGL_ItMap_StringTexture;
        // **********************************

  protected :
        MOGL_Map_StringTexture _MapTextureObjet;
        MOGL_Map_StringTexture _MapTextureParticule;

  public :
        void AjouterTextureObjet(const string & p_NomTexture, MOGL_Texture * p_Texture);
        //void AjouterTextureParticule(const string & p_NomTexture, MOGL_Texture * p_Texture);
        MOGL_Texture * GetTexture(const string & p_NomTexture) const;
  void RechargerTextures();
};

#endif
