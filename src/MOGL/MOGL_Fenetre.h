/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_FENETRE
#define MOGL_FENETRE


#ifdef _WIN32
#include <windows.h>
#include <windowsx.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <iostream>
#include <string>
#include <map>
#include <set>
#include "MOGL_Interface.h"
#include "MOGL_Signal.h"

using namespace std;

/**
   Permet de g�rer une fen�tre destin�e � recevoir l'affichage.
   La r�solution par d�faut est 640x480 avec des couleurs cod�es sur 16 bits, en mode fen�tr�.
*/
class MOGL_Fenetre
{
    // **** Types locaux � la classe ****
  public:
    struct MOGL_Resolution;
  private:
    struct CompareResolution
    {
        bool operator() (const MOGL_Resolution & x, const MOGL_Resolution & y)
        {
          return x.w < y.w;
        }
    };

    typedef set<MOGL_Interface *> MOGL_Set_Interface;
    typedef MOGL_Set_Interface::iterator MOGL_ItSet_Interface;

    typedef map<unsigned int, MOGL_Set_Interface> MOGL_Map_Interface;
    typedef MOGL_Map_Interface::iterator MOGL_ItMap_Interface;
  public:
    struct MOGL_Resolution
    {
        MOGL_Resolution(unsigned int pW, unsigned int pH): w(pW), h(pH)
        {
        }
        unsigned int w, h;
    };
    typedef set<MOGL_Resolution, CompareResolution> MOGL_Set_Resolution;
    typedef MOGL_Set_Resolution::iterator MOGL_ItSet_Resolution;


    // **********************************

  protected :
    char * _Titre;
    int _ResolutionX, _ResolutionY;
    int _MemResolutionX, _MemResolutionY;
    unsigned int _NbBitParPixel;
    bool _FinBoucle;
    SDL_Surface * _SurfaceSDL;
    bool _Fullscreen;
    bool _GrabCurseur;
    int _RepetitionDelai;
    int _RepetitionIntervalle;

    MOGL_Map_Interface _EnsembleInterface;

    unsigned int _NumeroImage;
    bool _ExisteFenetre;
    bool _CacheSouris;

    MOGL_Set_Resolution _SetResolution;

    // Donn�es pour les joysticks
    unsigned int _NombreJoysticks;
    SDL_Joystick ** _Joysticks;
    bool _bManualResize;

    // Les Signaux
  public:
    MOGL_Signal0 Idle;
    MOGL_Signal1<SDL_keysym *> KeyDown;
    MOGL_Signal1<SDL_keysym *> KeyUp;
    MOGL_Signal1<SDL_MouseMotionEvent *> MouseMove;
    MOGL_Signal1<SDL_MouseButtonEvent *> MouseButtonDown;
    MOGL_Signal1<SDL_MouseButtonEvent *> MouseButtonUp;
    MOGL_Signal3<int,int,int> ChangementMode;
    MOGL_Signal0 Destruction;
    MOGL_Signal3<unsigned int, unsigned int, float> MouvementAxeJoystick; // Joystick index, Axe index, valeur
    MOGL_Signal2<unsigned int, unsigned int> BoutonJoystickDown;          // Joystick index, Bouton index
    MOGL_Signal2<unsigned int, unsigned int> BoutonJoystickUp;            // Joystick index, Bouton index

  protected:
    void _MouvementAxeJoystick(SDL_JoyAxisEvent * p_JoyAxisEvent);
    void _BoutonJoystickDown(SDL_JoyButtonEvent * p_JoyButtonEvent);
    void _BoutonJoystickUp(SDL_JoyButtonEvent * p_JoyButtonEvent);
    bool _InitGL();
    void _GestionEvenement();
    void _Reshape (int w,int h);
    void _ManualResize(int p_ResolutionX, int p_ResolutionY);

    SDL_Surface * _CreerFenetre(unsigned int & p_Bpp, unsigned int p_Flags);

  public :
    MOGL_Fenetre(const char * p_Titre=NULL);
    ~MOGL_Fenetre();

    /**
       Permet de d�finir la r�solution de la fen�tre
       @param p_ResolutionX : r�solution en X de la fen�tre
       @param p_ResolutionY : r�solution en Y de la fen�tre
       @param p_Fullscreen : indique si la fen�tre doit �tre plein �cran ou non.
       @return <i>true</i> si il n'y a pas eu de probl�me, <i>false</i> sinon. Un probl�me peut intervenir seuleument s'il
               s'agit d'un changement de mode alors que la fen^tre existe
    */
    bool SetMode(int p_ResolutionX, int p_ResolutionY, bool p_Fullscreen);

    MOGL_Set_Resolution GetAvailableResolution() const;

    /**
       Permet de d�finir la r�solution de la fen�tre. Le nombre de bit par pixel reste inchang�.
       @param p_ResolutionX : r�solution en X de la fen�tre
       @param p_ResolutionY : r�solution en Y de la fen�tre
    */
    bool SetMode(int p_ResolutionX, int p_ResolutionY);

    /**
       Permet de r�cup�rer la r�solution X de la fen�tre.
       @return La r�solution X.
     */
    int GetResolutionX() const;

    /**
       Permet de r�cup�rer la r�solution Y de la fen�tre.
       @return La r�solution Y.
     */
    int GetResolutionY() const;

    /**
       Permet de r�cup�rer le nombre de bits par pixel de la fen�tre.
       @return Le nombre de bits par pixels.
     */
    int GetBPP() const;

    /**
       Permet de d�finir le titre de la fen�tre. Le titre n'est affich� que lorsque la fenetre n'est pas en plein �cran
       (@sa SetFullscreen()].
       @param p_Titre : titre de la fen�tre
    */
    void SetTitre(char * p_Titre);

    /**
       Permet de mettre la fen�tre en plein �cran.
       p_Fullscreen : true si plein �cran, false sinon.
    */
    void SetFullscreen(bool p_Fullscreen);

    /**
       Indique si la fen�tre est en plein �cran.
       @return <i>true</i> si a fen�tre est en plein �cran, <i>false</i> sinon.
     */
    bool GetFullscreen() const;

    /**
       Permet d'initialiser la fen�tre d'affichage ainsi que tout l'environnement MOGL.
       Cette initialisation doit �tre effectu�e avant la plupart des op�rations pouvant �tre effectu�es par MOGL
       (chargement de texture...)
       @return <i>true</i> s'il n'y a pas eu de probl�me, et <i>false</i> sinon.
    */
    bool Initialiser();

    /**
       Permet de Lancer la boucle d'�v�nement de MOGL. La m�thode ne redonne la main que lorsque la boucle d'�v�nement est termin�e,
       ce qui signifie que l'ex�cution de MOGL est achev�e.
       @return <i>true</i> s'il n'y a pas eu de probl�me, <i>false</i> sinon.
    */
    bool LancerBoucle();

    /**
       Permet d'arreter la boucle init� par LancerBoucle()
     */
    void Arreter();

    /**
       Permet d'ajouter une interface � la fen�tre. Les interface repr�sente des <i>contenus</i> li�s � la fen�tre
       comme par exemple MOGL_Univers qui repr�sente un univers 3D ou encore MOGL_Facade qui repr�sente un affichage 2D.
    */
    void AjouterInterface(MOGL_Interface * p_Interface);

    /**
       Permet de retirer une interface.
       @sa AjouterInterface().
    */
    bool RetirerInterface(MOGL_Interface * p_Interface);


    /**
       Permet d'infiquer � la fen�tre si elle peut garder le curseur.
       @param p_Val : <i>true</i> pour l'attraper, <i>false</i> sinon.
    */
    void SetGrabCurseur(bool p_Val);

    /**
       Permet de cacher le pointeur de souris. Par d�faut, le pointeur est cach�.
       param p_Val : <i>true</i> pour le cacher, <i>false</i> sinon.
     */
    void SetCacherCurseur(bool p_Val);

    /**
       Permet de d�finir la r�p�tition des touches.
       @todo � amm�liorer.
    */
    void DefinirRepetitionTouche(int p_Delai, int p_Intervalle);

    /**
     * Permet de prendre un screenshot au format BMP.
     *
     * @param p_NomFichier : nom du fichier de sauvegarde.
     *
     * @return <i>true</i> s'il n'y a pas eu d'erreur, <i>false</i> sinon.
     */
    bool ScreenshotBMP(char * p_NomFichier);

    /**
     * Permet de prendre un screenshot au format BMP.
     * Le nom du fichier est ici d�termin� de mani�re automatique, � partir du prefix fourni.
     * Le nom correspond au pr�fix indiqu� qui est suivi d'un num�ro (le premier disponible,
     * de mani�re � ce que le fichier n'existe pas sur le disque), et de l'extension <i>.bmp</i>
     *
     * @param p_Prefix : pr�fix du nom du fichier
     *
     * @return <i>true</i> s'il n'y a pas eu d'erreur, <i>false</i> sinon.
     */
    bool ScreenshotPrefixBMP(char * p_Prefix);

    unsigned int GetNombreJoystick() const;
    bool OuvrirJoystick(unsigned int p_IndiceJoystick);
    bool FermerJoystick(unsigned int p_IndiceJoystick);
};

#endif

/** @mainpage Moteur Open GL - MOGL
        @section intro Introduction
         @subsection quoi Qu'est-ce que c'est ?
         MOGL est un <i>moteur 3D temps r�el</i>.
         Cela signifie qu'il permet de g�rer l'affichage d'un univers en trois dimension de mani�re suffisamment rapide pour
         �tre interactif.
         Ainsi MOGL propose un ensemble de classes C++ disponibles sous la forme d'une biblioth�que permettant la r�alisation ais�e
         d'une application, en proposant tous les services li�es � la mise en place d'un univers 3D : Cr�ation d'une fen�tre d'affichage,
         affichage d'objets, animations, interraction...

         @subsection specif Sp�cificit�s de MOGL
         Ce genre d'outil existe sous de tr�s nombreuses formes. Par exemple <i>3D Master Suite</i> d�velopp� � l'origine par SGI est une
         bibliot�que reposant sur OpenGL, qui est utilis� dans le monde professionel qui propose la r�alisation d'un affichage 3D
         (pour la r�alisation de logiciel de CAO par exemple). De m�me, Id Software s'est d�marqu� par la cr�ation de moteurs 3D
         utilis�s dans le jeux video (<i>Quake 3</i> par exemple).
         L'initiative de base est semblable, cependant le but �tant diff�rents ces deux outils n'ont pas les m�me contraintes,
         et ne sont pas interchangables.
         @par
         De la m�me mani�re MOGL a �t� con�u dans un but pr�cis : c'est un moteur 3D de type jeux video (ainsi il n'est peut pas con�u
         pour s'int�grer dans une IHM complexe), simple d'utilisation et multiplateforme.

         @subsection fonctionnalites Fonctionnalit�s
          @li Affichage de triangles et de strips
          @li Gestion des �l�ments de l'affichage par l'interm�diaire d'un arbre, de manipulation ais�e
          @li Moteur de particules permettant la cr�ation de nouvelles particules facilement
          @li Affichage de Lens Flare
          @li Animation ais�e des objets pr�sents dans l'arbre par l'interm�diare de classes sp�cialis�s
          @li Elements 2D plac� au dessus de l'affichage 3D pour la r�alisation ais�e d'IHM.

        @section install Installation
*/
