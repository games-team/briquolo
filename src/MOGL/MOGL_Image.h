/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_IMAGE
#define MOGL_IMAGE

#include "MOGL_Structure.h"
#include <string>

using namespace std;

class MOGL_Image
{
    // **** Types locaux � la classe ****
  protected:
    enum MOGL_Enum_TypeImage
    {
      NON_DEFINI,
      BMP,
      PNG
    };

  public:
    /**
       Indique la gestion du param�tre alpha lors du chargement d'une image
    */
    enum MOGL_Enum_GestionAlpha 
    {ALPHA_UN, /**< alpha est toujours �gal � 1.*/
     ALPHA_FICHIER, /**< alpha est �gal � la valeur alpha sp�cifi�e dans le fichier (1 s'il n'y en a pas).*/
     ALPHA_MOYENNE_RGB /**< alpha est �gal � la moyenne des valeur R, V et B du fichier (r+g+b)/3.*/
    };

    /**
       Indique la gestion de la couleur lors du chargement d'une texture
    */
    enum MOGL_Enum_GestionRGB
    {RGB_UN, /**< R, V, et B sont toujours �gal � 1.*/
     RGB_FICHIER, /**< R, V, et B sont �gal � leur valeur sp�cifi�e dans le fichier.*/
     RGB_MOYENNE_RGB /**< R, V, et B sont �gal � la moyenne des valeur R, V et B du fichier (r+g+b)/3.*/
    };
    // **********************************

  protected:
    unsigned int _TailleX;
    unsigned int _TailleY;
    MOGL_Struct_Couleur * _Image;
    bool _AutoriserTrou;
    float _R,_G,_B;
    MOGL_Enum_GestionAlpha _ModeGestionAlpha;
    MOGL_Enum_GestionRGB _ModeGestionRGB;
    MOGL_Enum_TypeImage _TypeImage;
    string _NomFichier;

  protected:
    void _TraiterCouleur(MOGL_Struct_Couleur & p_Couleur);
    /**
       Permet de lire un entier depuis un fichier de mani�re ind�pendant de l'endian.
    */
    unsigned int _ReadUnsignedInt(ifstream & file);

  public:
    MOGL_Image(bool p_AutoriserTrou=false);
    virtual ~MOGL_Image();
    virtual bool ChargerBMP(const char * p_NomFichier);
    virtual bool ChargerPNG(const char * p_NomFichier);

    /**
       Permet de d�finir la couleur de transparence de l'image.
       Il s'agit d'une couleur  pour laquelle l'image 
       est transparente.
       @param p_R : valeur <i>rouge</i> de la couleur de transparence, comprise entre 0 et 1.
       @param p_G : valeur <i>vert</i> de la couleur de transparence, comprise entre 0 et 1.
       @param p_B : valeur <i>bleu</i> de la couleur de transparence, comprise entre 0 et 1.
    */
    virtual void SetCouleurTrou(float p_R, float p_G, float p_B);

    /**
       Permet de d�finir la couleur de transparence de l'image.
       Cette couleur de transparence est une couleur pour laquelle l'image
       est "trou�e".
       @param p_R : valeur <i>rouge</i> de la couleur de transparence, comprise entre 0 et 255.
       @param p_G : valeur <i>vert</i> de la couleur de transparence, comprise entre 0 et 255.
       @param p_B : valeur <i>bleu</i> de la couleur de transparence, comprise entre 0 et 255.
    */
    virtual void SetCouleurTrouOctet(unsigned char p_R, unsigned char p_G, unsigned char p_B);

    /**
       Permet de d�finir la gestion du param�tre alpha lors du chargement d'une image.
       @param p_ModeAlpha : mode de gestion du param�tre alpha.
       @sa MOGL_Enum_GestionAlpha
    */
    virtual void SetGestionModeAlpha(MOGL_Image::MOGL_Enum_GestionAlpha p_ModeAlpha);

    /**
       Permet de d�finir la gestion de la couleur lors du chargement d'une image.
       @param p_ModeRGB : mode de gestion de la couleur.
       @sa MOGL_Enum_GestionRGB
    */
    virtual void SetGestionModeRGB(MOGL_Image::MOGL_Enum_GestionRGB p_ModeRGB);

    /**
       Indique si la texture est trou�.
       @return <i>true</i> si la texture est trou�e, <i>false</i> sinon.
     */
    virtual bool GetAutoriserTrou() const;

    /**
       Permet de recharger la texture.
    */
    virtual bool Recharger();


    MOGL_Struct_Couleur & GetPixel(unsigned int p_PositionX, unsigned int p_PositionY) const;
};

#endif
