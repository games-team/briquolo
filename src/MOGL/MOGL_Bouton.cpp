/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Bouton.h"

MOGL_Bouton::MOGL_Bouton(MOGL_Panneau * p_Panneau, const string & p_Label): MOGL_ElementPanneau(p_Panneau, true), _Label(p_Label)
{
  _CouleurTexte.r=1;
  _CouleurTexte.g=1;
  _CouleurTexte.b=1;
  _CouleurTexte.a=1;
}

void MOGL_Bouton::Afficher()
{
  MOGL_ElementPanneau::Afficher();
  if (_Focus)
  {
    _Police->Afficher(_Label, 0, 0, _CouleurTexte, _CouleurFondFocus);
  }
  else
  {
    _Police->Afficher(_Label, 0, 0, _CouleurTexte);
  }
}

void MOGL_Bouton::SetLabel(const string & p_Label)
{
  _Label=p_Label;
}

string MOGL_Bouton::GetLabel() const
{
  return _Label;
}

void MOGL_Bouton::ClavierUp(SDL_keysym * key)
{
  switch (key->sym)
  {
    case SDLK_KP_ENTER :
    case SDLK_RETURN:
    {
      Activation.Emettre();
      break;
    }
    default:
    {
      break;
    }
  }  
}

unsigned int MOGL_Bouton::GetHauteurElement() const
{
  return _Police->GetTailleYMax();
}

unsigned int MOGL_Bouton::GetLargeurElement() const
{
  return _Police->GetTailleXChaine(_Label);
}

void MOGL_Bouton::SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur)
{
  _CouleurTexte=p_Couleur;
}

MOGL_Struct_Couleur MOGL_Bouton::GetCouleurLabel() const
{
  return _CouleurTexte;
}

