/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_PANNEAU
#define MOGL_PANNEAU

#include <vector>
#include <string>
#include <iostream>

#include "MOGL_Interface.h"
#include "MOGL_ElementPanneau.h"
#include "MOGL_Police.h"
#include "MOGL_Fenetre.h"

using namespace std;

class MOGL_Panneau: public MOGL_Interface
{
    friend class MOGL_ElementPanneau;
    // **** Types locaux � la classe ****
  public:
    typedef enum{MIN, CENTRE, MAX} MOGL_Enum_Alignement;
  protected:
    typedef vector <MOGL_ElementPanneau *> MOGL_Vector_ElementPanneau;
    typedef MOGL_Vector_ElementPanneau::iterator MOGL_ItVector_ElementPanneau;
    // **********************************

  protected :
    MOGL_Fenetre * _Fenetre;
    MOGL_Panneau * _Panneau;
    MOGL_Struct_Couleur _CouleurFond, _CouleurLabel;
    MOGL_Vector_ElementPanneau _VectorPanneau;
    const MOGL_Police * _Police;
    unsigned int _LargeurPanneau, _HauteurPanneau;
    unsigned int _IndiceFocus;
    bool _Activation, _ProchaineActivation;
    MOGL_Panneau *  _FilsActif;
    string _Label;
    MOGL_Enum_Alignement _AlignementHorizontal, _AlignementVertical;
    bool _UtiliserEscape;
    SDL_keysym _LastKeyDown;
    
    virtual void _AjouterElementPanneau(MOGL_ElementPanneau * p_ElementPanneau);
                                
  public :
    MOGL_Panneau(MOGL_Fenetre * p_Fenetre, const string & p_Label, unsigned int p_LargeurPanneau, unsigned int p_HauteurPanneau);
    MOGL_Panneau(MOGL_Panneau * p_Panneau, const string & p_Label, unsigned int p_LargeurPanneau, unsigned int p_HauteurPanneau);
    virtual void Afficher();
    virtual void SetPolice(MOGL_Police * p_Police);
    virtual bool SetFocus(MOGL_ElementPanneau * p_Element);
    virtual void SetLabel(const string & p_Label);
    virtual void SetActivation(bool p_Activation);
    virtual bool GetActivation() const;
    virtual void ProchainFocus();
    virtual void PrecedentFocus();
    virtual void SetCouleurFond(const MOGL_Struct_Couleur & p_Couleur);
    virtual void SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur);
    virtual MOGL_Struct_Couleur GetCouleurFond() const;
    virtual MOGL_Struct_Couleur GetCouleurLabel() const;
    virtual void SetUtiliserEscape(bool p_Utilise);
    virtual bool GetUtiliserEscape() const;

    virtual void ClavierUp(SDL_keysym * key);
    virtual void ClavierDown(SDL_keysym * key);
    virtual void DeplacementSouris(SDL_MouseMotionEvent * p_Event);
};

#endif
