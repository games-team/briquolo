/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_POLYGONE
#define MOGL_POLYGONE

#include "MOGL_Structure.h"
#include "MOGL_ElementFacade.h"
#include <vector>

using namespace std;

class MOGL_Polygone: public MOGL_ElementFacade
{
    // **** Types locaux � la classe ****
    struct MOGL_Struct_PointPolygone
    {
        unsigned int X, Y;
        MOGL_Struct_Couleur Couleur;
    };

    typedef vector < MOGL_Struct_PointPolygone > MOGL_Vector_Point;
    typedef MOGL_Vector_Point::iterator MOGL_ItVector_Point;

    // **********************************

  protected:
    MOGL_Vector_Point _VectorPoint;

  public:
    /**
       Constructeur
    */
    MOGL_Polygone();

    /**
       Permet d'ajouter un point au polygone
       @param p_X : coordonn�e X du point.
       @param p_Y : coordonn�e Y du point.
       @param p_Couleur : couleur du point.
     */
    unsigned int AjouterPoint(unsigned int p_X, unsigned int p_Y, const MOGL_Struct_Couleur & p_Couleur);

    /**
       Permet de d�finir la couleur du point.
       @param p_Indice : indice du point � modifier (si l'indice n'existe pas, rien ne se passe)
       @param p_Couleur : nouvelle couleur du point
     */
    void SetCouleur(unsigned int p_Indice, const MOGL_Struct_Couleur & p_Couleur);

    /**
       Permet de d�finir la position d'un point.
       @param p_Indice : indice du point � modifier (si l'indice n'existe pas, rien ne se passe)
       @param p_X : nouvelle position X du point
       @param p_Y : nouvelle position Y du point
     */
    void SetPosition(unsigned int p_Indice, unsigned int p_X, unsigned int p_Y);

    void Afficher();
};

#endif
