/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Omni.h"
#include "MOGL_GestionnaireTexture.h"

MOGL_Omni::MOGL_Omni(): MOGL_Lumiere()
{
        _Ambient.r=0;
        _Ambient.g=0;
        _Ambient.b=0;
        _Ambient.a=1.f;
}


void MOGL_Omni::TnL(MOGL_Afficheur * p_Afficheur)
{
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  _MatriceFinale.PrendreDeOpenGL();

  glLightfv(_NumLight,GL_DIFFUSE,(float*)&_Diffuse);
  glLightfv(_NumLight,GL_SPECULAR,(float*)&_Specular);
  glLightfv(_NumLight,GL_AMBIENT,(float*)&_Ambient);
  glEnable(_NumLight);

  // **** R�glage de la lumi�re ****
  int Pos[4]={0,0,0,1};
  glLightiv(_NumLight,GL_POSITION,Pos);
  glLightf(_NumLight,GL_SPOT_CUTOFF,180);

  MOGL_ItSet_ElementArbre it;
  for(it=_SetElement.begin();it!=_SetElement.end();it++)
  {
        (*it)->TnL(p_Afficheur);
  }
  glPopMatrix();

  if (_ProjeterOmbre)
  {
        MOGL_Struct_Vecteur vec;
        vec.x=_MatriceFinale.Lit(3,0);
        vec.y=_MatriceFinale.Lit(3,1);
        vec.z=_MatriceFinale.Lit(3,2);

        p_Afficheur->AjouterPoint(vec);

        p_Afficheur->AjouterLumiereOmbre(vec, false);
  }
}


MOGL_Omni::~MOGL_Omni()
{
}
