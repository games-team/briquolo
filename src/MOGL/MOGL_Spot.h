/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_SPOT
#define MOGL_SPOT

#include "MOGL_Lumiere.h"

class MOGL_Spot: public MOGL_Lumiere
{
        protected:
                MOGL_Struct_Vecteur _Direction;
                GLfloat _CutOff;
                GLfloat _Exposant;

        public:
                ~MOGL_Spot();

                void SetExposant (GLfloat p_Exposant);
                void SetCutOff (GLfloat p_CutOff);
                void SetDirection(const MOGL_Struct_Vecteur & p_Direction);
                void TnL(MOGL_Afficheur * p_Afficheur);

};

#endif
