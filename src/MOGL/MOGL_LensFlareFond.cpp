/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_LensFlareFond.h"

MOGL_LensFlareFond::MOGL_LensFlareFond(const MOGL_Struct_VecteurHomogene & p_Direction): MOGL_LensFlare(), _Direction(p_Direction)
{
}

void MOGL_LensFlareFond::TnL(MOGL_Afficheur * p_Afficheur)
{
  _Afficheur=p_Afficheur;
  
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  _MatriceFinale.PrendreDeOpenGL();

  MOGL_Struct_Vecteur vec;
  vec.x=p_Afficheur->GetCamera()->GetPosition().x+_Direction.x*50;
  vec.y=p_Afficheur->GetCamera()->GetPosition().y+_Direction.y*50;
  vec.z=p_Afficheur->GetCamera()->GetPosition().z+_Direction.z*50;
  vec=_MatriceFinale.MultiplierVecteur(vec);  

  _xPos=vec.x;
  _yPos=vec.y;
  _zPos=vec.z;

  _Intensite=fabs(_zPos/sqrt(_xPos*_xPos+_yPos*_yPos+_zPos*_zPos));

  _GenererLensFlare();
 
  glPopMatrix();
}

