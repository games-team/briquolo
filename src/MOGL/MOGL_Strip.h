/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_STRIP
#define MOGL_STRIP

#include <list>

#include "MOGL_Structure.h"
#include "MOGL_Texture.h"

using namespace std;

class MOGL_Strip
{
    // **** Types locaux � la classe ****
    typedef list <MOGL_Struct_Point> MOGL_Liste_Point;
    typedef MOGL_Liste_Point::const_iterator MOGL_ItListe_Point;
    // **********************************

  protected :
    MOGL_Liste_Point _ListePoint;
    MOGL_ItListe_Point _ItListePoint;
    MOGL_Texture * _Texture;
                

  public :
    void AjouterPoint(const MOGL_Struct_Point & p_Point);
    const MOGL_Struct_Point & GetFirstPoint() const;
    const MOGL_Struct_Point & GetNextPoint() const;

    int NbPoint() const;

    void SetTexture(MOGL_Texture * p_Texture);
    MOGL_Texture * GetTexture() const;
};

#endif
