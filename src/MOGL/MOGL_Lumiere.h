/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_LUMIERE
#define MOGL_LUMIERE

#include "MOGL_Noeud.h"

class MOGL_Lumiere: public MOGL_Noeud
{
  protected:
    // **** Types locaux � la classe ****
    typedef set <int> MOGL_Set_LumiereLibre;
    typedef MOGL_Set_LumiereLibre::iterator MOGL_ItSet_LumiereLibre;
    // **********************************

  protected:
    MOGL_Struct_Couleur _Ambient;
    MOGL_Struct_Couleur _Specular;
    MOGL_Struct_Couleur _Diffuse;
    unsigned int _NumLight;

    static unsigned int _NbLight;

    MOGL_MatriceTransformation _MatriceFinale;

    bool _ProjeterOmbre;

    static MOGL_Set_LumiereLibre _SetLumIdLibre;

  public :
    MOGL_Lumiere();
    virtual ~MOGL_Lumiere();

    void TnL(MOGL_Afficheur * p_Afficheur);

    void SetAmbient(const MOGL_Struct_Couleur & LaCouleur);
    void SetSpecular(const MOGL_Struct_Couleur & LaCouleur);
    void SetDiffuse(const MOGL_Struct_Couleur & LaCouleur);

    void ProjeterOmbre(bool p_Val);
};

#endif
