/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Choix.h"
#include <algorithm>

MOGL_Choix::MOGL_Choix(MOGL_Panneau * p_Panneau, const string & p_Label): MOGL_ElementPanneau(p_Panneau, true), _Label(p_Label),
                                                                          _IndiceCourant(0)
{
  _CouleurTexte.r=1;
  _CouleurTexte.g=1;
  _CouleurTexte.b=1;
  _CouleurTexte.a=1;
}

void MOGL_Choix::Afficher()
{
  MOGL_ElementPanneau::Afficher();

  if (_IndiceCourant>=_Choix.size())
  {
    _IndiceCourant=_Choix.size()-1;
  }

  string texte(_Label);
  if (_Choix.size()!=0)
  {
    texte+=" : ";
    texte+=_Choix[_IndiceCourant];
  }  

  if (_Focus)
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte, _CouleurFondFocus);
  }
  else
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte);
  }
}

void MOGL_Choix::SetLabel(const string & p_Label)
{
  _Label=p_Label;
}

string MOGL_Choix::GetLabel() const
{
  return _Label;
}

void MOGL_Choix::ClavierUp(SDL_keysym * key)
{
  switch (key->sym)
  {
    case SDLK_LEFT :
    {
      if (_IndiceCourant==0)
      {
        _IndiceCourant=_Choix.size()-1;
      }
      else
      {
        _IndiceCourant--;
      }
      ChangementElement.Emettre(_IndiceCourant);
      break;
    }
    case SDLK_RIGHT :
    {
      _IndiceCourant++;
      if (_IndiceCourant==_Choix.size())
      {
        _IndiceCourant=0;
      }
      ChangementElement.Emettre(_IndiceCourant);
      break;
    }
    case SDLK_KP_ENTER :
    case SDLK_RETURN:
    {
      _IndiceCourant++;
      if (_IndiceCourant==_Choix.size())
      {
        _IndiceCourant=0;
      }
      ChangementElement.Emettre(_IndiceCourant);
      break;
    }
    default:
    {
      break;
    }
  }  
}

void MOGL_Choix::SetIndiceCourant(unsigned int p_Indice)
{
  if (p_Indice<_Choix.size() && _IndiceCourant!=p_Indice)
  {
    _IndiceCourant=p_Indice;
    ChangementElement.Emettre(_IndiceCourant);
  }
}

void MOGL_Choix::AjouterElementChoix(const string & p_ElementChoix)
{
  _Choix.push_back(p_ElementChoix);
}

void MOGL_Choix::ViderElementsChoix()
{
  _Choix.clear();
}

string MOGL_Choix::GetElement(unsigned int p_Indice) const
{
  if (p_Indice<_Choix.size())
  {
    return _Choix[p_Indice];
  }
  else
  {
    return string();
  }
}

int MOGL_Choix::GetIndiceElement(const string & p_Element) const
{
  for(unsigned int i=0; i<_Choix.size(); i++)
  {
    if (_Choix[i]==p_Element)
    {
      return i;
    }
  }
  return -1;
}

unsigned int MOGL_Choix::GetIndiceCourant() const
{
  return _IndiceCourant;
}

unsigned int MOGL_Choix::GetHauteurElement() const
{
  return _Police->GetTailleYMax();
}

unsigned int MOGL_Choix::GetLargeurElement() const
{
  string texte(_Label);
  if (_Choix.size()!=0)
  {
    texte+=" : ";
    texte+=_Choix[_IndiceCourant];
  }  
  return _Police->GetTailleXChaine(texte);
}

void MOGL_Choix::SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur)
{
  _CouleurTexte=p_Couleur;
}

MOGL_Struct_Couleur MOGL_Choix::GetCouleurLabel() const
{
  return _CouleurTexte;
}

string MOGL_Choix::GetElementChoixCourant() const
{
  return _Choix[_IndiceCourant];
}

unsigned int MOGL_Choix::GetNbElement() const
{
  return _Choix.size();
}
