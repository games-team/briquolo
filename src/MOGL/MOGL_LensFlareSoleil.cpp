/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_LensFlareSoleil.h"
#include "MOGL_Bibliotheque.h"

MOGL_LensFlareSoleil::MOGL_LensFlareSoleil(const MOGL_Struct_VecteurHomogene & p_Direction, float p_Taille): MOGL_LensFlareFond(p_Direction), _TailleShine(p_Taille)
{
}

void MOGL_LensFlareSoleil::_GenererLensFlare()
{
  float intensite=(_Intensite-0.8)*5;

  MOGL_GestionnaireTexture gestText=MOGL_Bibliotheque::GetSingleton()->GetGestionnaireTexture();
  MOGL_Texture * texture;
  MOGL_Struct_Couleur coul;
  coul.r=1;
  coul.g=1;
  coul.b=1;
  coul.a=1;
  
  texture=gestText.GetTexture("flare4");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 1, 5, coul);

  coul.a=intensite;


  texture=gestText.GetTexture("flare1");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 1, _TailleShine, coul);

  texture=gestText.GetTexture("shine1");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 1, _TailleShine, coul);

  texture=gestText.GetTexture("shine2");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 1, _TailleShine, coul);

  texture=gestText.GetTexture("flare3");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.8f, 5, coul);

  texture=gestText.GetTexture("flare3");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.7f, 2, coul);

  texture=gestText.GetTexture("flare2");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.6f, 4, coul);

  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.4f, 3, coul);

  texture=gestText.GetTexture("flare3");
  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.25f, 2, coul);

  _DessinerCarreTailleFixe(texture, _xPos, _yPos, _zPos, 0.1f, 1, coul);
}


