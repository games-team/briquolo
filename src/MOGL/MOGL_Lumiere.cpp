/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include <iostream>
#include "MOGL_Lumiere.h"

unsigned int MOGL_Lumiere::_NbLight=0;

MOGL_Lumiere::MOGL_Set_LumiereLibre MOGL_Lumiere::_SetLumIdLibre;

MOGL_Lumiere::MOGL_Lumiere(): MOGL_Noeud(), _ProjeterOmbre(false)
{
  if (_SetLumIdLibre.size() == 0)
  {
    _NumLight=GL_LIGHT0+_NbLight;
    _NbLight++;
  }
  else
  {
    _NumLight = *_SetLumIdLibre.begin();
    _SetLumIdLibre.erase(_SetLumIdLibre.begin());
  }

  _Ambient.r=1;
  _Ambient.g=1;
  _Ambient.b=1;
  _Ambient.a=1.f;

  _Specular.r=1.f;
  _Specular.g=1.f;
  _Specular.b=1.f;
  _Specular.a=1.f;

  _Diffuse.r=.9f;
  _Diffuse.g=.9f;
  _Diffuse.b=.9f;
  _Diffuse.a=1.f;
}

MOGL_Lumiere::~MOGL_Lumiere()
{
  glDisable(_NumLight);
  _SetLumIdLibre.insert(_NumLight);
}

void MOGL_Lumiere::TnL(MOGL_Afficheur * p_Afficheur)
{
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();

  glLightfv(_NumLight,GL_DIFFUSE,(float*)&_Diffuse);
  glLightfv(_NumLight,GL_SPECULAR,(float*)&_Specular);
  glLightfv(_NumLight,GL_AMBIENT,(float*)&_Ambient);
  glEnable(_NumLight);

  MOGL_ItSet_ElementArbre it;
  for(it=_SetElement.begin();it!=_SetElement.end();it++)
  {
        (*it)->TnL(p_Afficheur);
  }
  glPopMatrix();
}

void MOGL_Lumiere::SetAmbient(const MOGL_Struct_Couleur & p_Couleur)
{
        _Ambient=p_Couleur;
}

void MOGL_Lumiere::SetSpecular(const MOGL_Struct_Couleur & p_Couleur)
{
        _Specular=p_Couleur;
}

void MOGL_Lumiere::SetDiffuse(const MOGL_Struct_Couleur & p_Couleur)
{
        _Diffuse=p_Couleur;
}

void MOGL_Lumiere::ProjeterOmbre(bool p_Val)
{
  _ProjeterOmbre=p_Val;
}
