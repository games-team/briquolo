/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_CAMERA
#define MOGL_CAMERA

#ifdef _WIN32
#include <windows.h>
#include <windowsx.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "MOGL_MatriceTransformation.h"
#include "MOGL_Structure.h"

class MOGL_Camera
{
  protected :
    MOGL_MatriceTransformation _MatriceCamera;

    MOGL_Struct_Vecteur _Position, _Haut, _DroiteInit;
    float _AngleHaut, _AngleInclinaison;
    float _ClippingMin, _ClippingMax;
    float _Rapport;
    float _Fov;

  public :
    MOGL_Camera();

    void EnvoyerAOpenGL();

    void SetPosition(GLfloat p_X,GLfloat p_Y,GLfloat p_Z);
    void SetPosition(MOGL_Struct_Vecteur p_Vecteur);
    MOGL_Struct_Vecteur GetPosition();

    void SetDirection(GLfloat p_X,GLfloat p_Y,GLfloat p_Z);
    void SetDirection(MOGL_Struct_Vecteur p_Vecteur);

    MOGL_Struct_Vecteur GetDirection();
    MOGL_Struct_Vecteur GetDroite();

    void SetHaut(GLfloat p_X,GLfloat p_Y,GLfloat p_Z);
    void SetHaut(MOGL_Struct_Vecteur p_Vecteur);
    void SetHaut3DS();
    void SetHautBlender();
    void SetHautMilkshape();
    MOGL_Struct_Vecteur GetHaut();

    void Avancer(float p_Distance);
    void Reculer(float p_Distance);
    void Droite(float p_Distance);
    void Gauche(float p_Distance);
    void Monter(float p_Angle);
    void Descendre(float p_Angle);
    void TournerHaut(float p_Angle);
    void TournerBas(float p_Angle);
    void TournerDroite(float p_Angle);
    void TournerGauche(float p_Angle);

    void AvancerVitesse(float p_Vitesse);
    void ReculerVitesse(float p_Vitesse);
    void DroiteVitesse(float p_Vitesse);
    void GaucheVitesse(float p_Vitesse);
    void TournerDroiteVitesse(float p_Vitesse);
    void TournerGaucheVitesse(float p_Vitesse);

    void SetClipping(float p_Min, float p_Max);
    float GetClippingMin();
    float GetClippingMax();

    float GetFov() const;
    void SetFov(float p_Fov);
        

    /**
       Permet de d�finir le rapport d'affichage <i>largeur/hauteur</i>.
    */
    void SetRaport(float p_Rapport);
        

};

#endif
