/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_POLICE_TTF
#define MOGL_POLICE_TTF

#include <map>
#include <vector>
#include <SDL_ttf.h>
#include "MOGL_Police.h"
#include "MOGL_Structure.h"

#define MOGL_TTF_STYLE_NORMAL TTF_STYLE_NORMAL
#define MOGL_TTF_STYLE_BOLD TTF_STYLE_BOLD
#define MOGL_TTF_STYLE_ITALIC TTF_STYLE_ITALIC
#define MOGL_TTF_STYLE_UNDERLINE TTF_STYLE_UNDERLINE

using namespace std;

class MOGL_PoliceTTF: public MOGL_Police
{
    // **** Types locaux � la classe ****
    struct MOGL_Struct_Carac
    {
        GLuint TextureName;
        Uint16 carac;
        unsigned int x1;
        unsigned int y1;
        unsigned int x2;
        unsigned int y2;
    };

    struct MOGL_Struct_TextureCarac
    {
        GLuint TextureName;
        SDL_Surface * SurfaceSDL;
    };

    typedef map<Uint16, MOGL_Struct_Carac> MOGL_Map_Carac;
    typedef vector<MOGL_Struct_TextureCarac> MOGL_Vector_TextureName;
    // **********************************

  public:
    /**
       Constructeur
    */
    MOGL_PoliceTTF(unsigned int p_Size, int p_Style = MOGL_TTF_STYLE_NORMAL);

    /**
       Destructeur
    */
    ~MOGL_PoliceTTF();

    /**
       Permet de Charger  une police de caract�res.
    */
    bool Charger(const char * p_NomFichier);

    /**
       Permet d'afficher une chaine de caract�res.
       @param p_Chaine : chaine de caract�res � afficher.
       @param p_X : coordonn�e X de la chaine de caract�res.
       @param p_Y : coordonn�e Y de la chaine de caract�res.       
       @param p_CouleurTexte : couleur de la chaine de caract�res.
    */
    void Afficher(const string & p_Chaine, int p_X, int p_Y, const MOGL_Struct_Couleur & p_CouleurTexte) const;

    /**
       Permet d'afficher une chaine de caract�res.
       @param p_Chaine : chaine de caract�res � afficher.
       @param p_X : coordonn�e X de la chaine de caract�res.
       @param p_Y : coordonn�e Y de la chaine de caract�res.       
       @param p_CouleurTexte : couleur de la chaine de caract�res.
       @param p_CouleurFond : couleur du fond.
    */
    void Afficher(const string & p_Chaine, int p_X, int p_Y,
                  const MOGL_Struct_Couleur & p_CouleurTexte, const MOGL_Struct_Couleur & p_CouleurFond) const;

    /**
       Permet de r�cup�rer la largeur d'un caract�re de la police.
       @param p_Caractere : caract�re dont on veut la largeur.
       @return la largeur du caract�re en pixels.
    */
    unsigned int GetTailleXCaractere(Uint16 p_Caractere) const;

    /**
       Permet de r�cup�rer la hauteur d'un caract�re de la police.
       @param p_Caractere : caract�re dont on veut la hauteur.
       @return la hauteur du caract�re en pixels.
    */
    unsigned int GetTailleYCaractere(Uint16 p_Caractere) const;
        
    /**
       Permet de recharger la police.
    */
    bool Recharger();

  protected:
    /**
       Permet d'afficher un caract�re
       @param p_X : position X du caract�re
       @param p_Y : position Y du caract�re
       @param p_Caractere : caract�re � afficher.
       @return Position x du prochain caract�re
    */
    float _AfficherCaractere(float p_X, float p_Y, MOGL_Struct_Carac * p_Caractere);

    /**
       Permet d'afficher le fond d'un caract�re.
       Il s'agit en fait d'afficher un rectangle de la taille du caract�re, et de couleur unie.
       @param p_X : position X du caract�re
       @param p_Y : position Y du caract�re
       @param p_Caractere : caract�re � afficher.
       @return Position x du prochain caract�re
    */
    float _AfficherFondCaractere(float p_X, float p_Y, MOGL_Struct_Carac * p_Caractere);

    MOGL_Struct_Carac * _GetCarac(Uint16 p_Carac);
    bool _CreateCarac(Uint16 p_Carac);
    MOGL_Struct_TextureCarac _CreateSurface();
        
    unsigned int _NbCaractere;
    MOGL_Struct_Carac * _Caracteres;
    MOGL_Struct_Carac * (_Correspondance[256-32]);

    unsigned int _LastTextureX, _LastTextureY;
    unsigned int maxHeightCarac;
    MOGL_Map_Carac _MapCarac;
    MOGL_Vector_TextureName _VectorTextureName;
    TTF_Font * _Font;
    unsigned int _Size;
    int _Style;
    bool _bOldOpenGl;
};

#endif
