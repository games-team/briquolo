/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ARMATURE
#define MOGL_ARMATURE

#include <vector>
#include <map>
/*#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>*/

#include "MOGL_Noeud.h"
#include "MOGL_Structure.h"
#include "MOGL_Constante.h"

using namespace std;

/**
   Repr�sente un �l�ment d'armature. Cet �l�ment peut �tre assimil� � un os dans un squelette. Un squelette
   est form� d'un arbre de MOGL_Armature. Cette classe est con�ue pour fonctionner avec MOGL_Peau : la peau 
   recouvre le squelette. Ainsi cette classe se caract�rise par une liste de point qui est transform� selon 
   la matrice de transformation.
   @sa MOGL_Peau
 */
class MOGL_Armature: public MOGL_Noeud
{
    // **** Types locaux � la classe ****
    typedef vector <MOGL_Struct_PointSimple> MOGL_Vecteur_PointSimple;
    typedef MOGL_Vecteur_PointSimple::iterator MOGL_ItVecteur_PointSimple;
    // **********************************

  protected :
    MOGL_Vecteur_PointSimple _VecteurPoint;
    MOGL_Vecteur_PointSimple _VecteurPointTransforme;

    MOGL_MatriceTransformation _MatriceInitiale, _MatriceFinale;

    bool _ModifiePoint;
    const MOGL_MatriceTransformation * _InvMatriceBase;
    MOGL_MatriceTransformation _MatriceTransformationPoint;
        
    /**
       Calcule les points.
     */
    void CalculerPoints();

  public :
    /**
       Constructeur.
    */
    MOGL_Armature();

    virtual void TnL(MOGL_Afficheur * p_Afficheur);

    /**
       Permet d'ajouter un point.
       @param p_NumPoint : indice du point.
       @param p_Point : point � ajouter.
     */
    void AjouterPoint(unsigned int p_NumPoint, const MOGL_Struct_PointSimple & p_Point);

    /**
       Permet d'ajouter un point.
       @param p_NumPoint : indice du point.
       @param xp  : coordonn� x du point � ajouter.
       @param yp  : coordonn� y du point � ajouter.
       @param zp  : coordonn� z du point � ajouter.
       @param xn  : coordonn� x de la normale du point � ajouter.
       @param yn  : coordonn� y de la normale du du point � ajouter.
       @param zn  : coordonn� z de la normale du du point � ajouter.
     */
    void AjouterPoint(unsigned int p_NumPoint, GLfloat xp, GLfloat yp, GLfloat zp,
                      GLfloat xn, GLfloat yn, GLfloat zn);

    /**
       Permet de r�cup�rer le point initial non transform�.
       @return Le point non transform�.
     */
    MOGL_Struct_PointSimple GetPointInitial(unsigned int p_Indice);

    /**
       Permet de r�cup�rer le point initial transform�.
       @return Le point transform�.
    */
    MOGL_Struct_PointSimple GetPointTransforme(unsigned int p_Indice);

    /**
       Red�finition de MOGL_ElementArbre::ReinitialiserPosition();
    */
    void ReinitialiserPosition();

    /**
       Permet de d�finir la matrice d'initialisation.
       @param p_Matrice : matrice de transformation d'initialisation.
     */
    void SetMatriceInitialisation(const MOGL_MatriceTransformation & p_Matrice);

    /**
       Permet de d�finir la matrice inverse de base.
       Lors de l'appel � TnL(), tous les points associ�s � l'objet sont transform�s en appliquant
       la transformation absolue associ�e (c'est � dire l'ensemble des transformations des noeuds
       parents, et la transformation relative associ�e � l'objet MOGL_Armature).
       En d�finissant une matrice inverse de base, on indique une matrice repr�sentant la transformation
       inverse � appliquer au point une fois transform�. Cela permet de limiter la transformation des points
       du MOGL_Armature � un sous espace. Ainsi MOGL_Peau utilise cette m�thode pour ne transformer les points
       de ses MOGL_Armature que par rapport � lui m�me, et ainsi de ne pas les rendre d�pendant des noeuds
       parents ou de la transformation de MOGL_Camera.
       @param p_InvMatriceBase : pointeur vers la MOGL_MatriceTransformation inverse de base.
    */
    void SetInvMatriceBase(const MOGL_MatriceTransformation * p_InvMatriceBase);
};

#endif

