/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ENSEMBLEOBJET
#define MOGL_ENSEMBLEOBJET

#include <map>
#include <set>
#include <string>
#include <iostream>


#include "MOGL_Objet.h"
#include "MOGL_Noeud.h"
#include "MOGL_Univers.h"
#include "MOGL_Animation.h"
#include "MOGL_GroupeAnimation.h"
#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include "MOGL_GestionnaireTexture.h"

using namespace std;

class MOGL_EnsembleObjet
{
        // **** Types locaux � la classe ****
        typedef map<string, MOGL_Noeud*> MOGL_Map_Noeud;
        typedef MOGL_Map_Noeud::iterator MOGL_ItMap_Noeud;

        typedef map <string, MOGL_GroupeAnimation* > MOGL_Map_GroupeAnimation;
        typedef MOGL_Map_GroupeAnimation::iterator MOGL_ItMap_GroupeAnimation;

        typedef set<string> MOGL_Set_String;
        typedef MOGL_Set_String::iterator MOGL_ItSet_String;
        // **********************************

  protected :
                
        /**
           Permet de stocker les noeuds en fonction de leur nom.
         */
        MOGL_Map_Noeud _MapObjet;

        /**
           Permet de stocker les animation en fonction de leur nom.
         */
        MOGL_Map_GroupeAnimation _MapGroupeAnimation;

        /**
           Noeud de base du MOGL_EnsembleObjet destin� � �tre ajout� � l'arbre de repr�sentation.
         */
        MOGL_Noeud * _Noeud;

        /**
           Liste des noms des animations en cours.
         */
        MOGL_Set_String _ListeAnimCourante;

  public :
        MOGL_EnsembleObjet();
        bool Charger(char * p_NomFichier, const MOGL_GestionnaireTexture p_GM);
        bool ChargerArmaturePeau(char * p_NomFichier, const MOGL_GestionnaireTexture p_GM);
        bool ChargerAnimation(char * p_NomFichier, char * p_NomAnimation);

        void DebuterAnimation(char * p_NomAnim);
        void Animer();
        MOGL_Noeud * GetNoeudPrincipal();
        MOGL_Noeud * GetNoeud(char * p_Nom);
                
                
};

#endif
