/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_BIBLIOTHEQUE
#define MOGL_BIBLIOTHEQUE

#include "MOGL_GestionnaireTexture.h"
#include "MOGL_PoliceTTF.h"
#include "MOGL_Fenetre.h"
#include "MOGL_Action.h"
#include <list>

using namespace std;

class MOGL_Bibliotheque: public MOGL_Action
{
  protected:
    static MOGL_Bibliotheque * _Singleton;
    list<MOGL_Texture *> _ListeTexture;
        
        
    MOGL_GestionnaireTexture _GestionnaireTexture;
    MOGL_PoliceTTF _Police;
        
    MOGL_Bibliotheque();
    ~MOGL_Bibliotheque();
    void _RechargerTextures(int, int, int);

  public:
    /**
       Renvoie la biblioth�que. Il ne peut y avoir qu'une seule biblioth�que (un singleton), et elle ne peut donc
       pas �tre construite avec un constructeur classique. On utilise � la place cette m�thode static renvoyant 
       l'unique instance de la biblioth�que, en la contruisant si elle ne l'est pas encore.
    */
    static MOGL_Bibliotheque * GetSingleton();

    /**
       Constuit la biblioth�que. Cette fonction construit l'instance de la biblioth�que qui est renvoy�e par GetSingleton().
       L'usage de cette m�thode n'est pas obligatoire, mais elle permet de ne pas perdre de temps � contruire l'instance
       lors de l'appel de GetSingleton(). En effet l'instanciation passe par le chargement de diff�rents fichier, ce qui
       peut �tre long et non adapt� � un affichage 3D temps-r�el.
    */
    static void Init();
        
    /**
       Renvoie le gestionnaire de texture de la biblioth�que.
    */
    const MOGL_GestionnaireTexture & GetGestionnaireTexture() const;

    /**
       Renvoie la police standard de la biblioth�que
    */
    const MOGL_PoliceTTF & GetPolice() const;

    /**
       Permet de recharger les textures et polices de la biblioth�que lors d'un
       changement de r�solution.
       @param p_Fenetre : pointeur vers le MOGL_Fen�tre susceptible de changer de r�solution.
    */
    void ReinitialiserSurChangementModeFenetre(MOGL_Fenetre * p_Fenetre);
}
  ;

#endif
