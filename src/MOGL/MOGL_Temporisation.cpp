/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Temporisation.h"
#include "MOGL_Temps.h"

MOGL_Temporisation::MOGL_Temporisation(float p_DureeTemporisation, bool p_Rearmer,  bool p_Demarrer)
  : MOGL_ElementArbre(), _DureeTempo(p_DureeTemporisation), _TempsEcoule(0), _Rearmer(p_Rearmer), _EnCours(p_Demarrer)
{
}

float MOGL_Temporisation::GetDureeTemporisation() const
{
  return _DureeTempo;
}

void MOGL_Temporisation::SetDureeTemporisation(float p_DureeTemporisation)
{
  _DureeTempo=p_DureeTemporisation;
}

void MOGL_Temporisation::TnL(MOGL_Afficheur * p_Afficheur)
{
  if (_EnCours)
  {
    _TempsEcoule+=MOGL_Temps::GetVariationTemps();
    if (_TempsEcoule>=_DureeTempo)
    {
      FinTemporisation.Emettre();
      _TempsEcoule=0;
      if (!_Rearmer)
      {
        _EnCours=false;
      }
    }
  }
}

bool MOGL_Temporisation::GetRearmage() const
{
  return _Rearmer;
}

void MOGL_Temporisation::SetRearmage(bool p_Rearmer)
{
  _Rearmer=p_Rearmer;
}

void MOGL_Temporisation::Demarrer()
{
  _EnCours=true;
}

void MOGL_Temporisation::Arreter()
{
  _EnCours=false;
  _TempsEcoule=0;
}

