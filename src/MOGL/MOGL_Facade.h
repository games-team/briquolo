/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_FACADE
#define MOGL_FACADE

#include <list>

#include "MOGL_ElementFacade.h"
#include "MOGL_Interface.h"

using namespace std;

class MOGL_Facade: public MOGL_Interface
{
    // **** Types locaux � la classe ****
    typedef list <MOGL_ElementFacade *> MOGL_Liste_ElementFacade;
    typedef MOGL_Liste_ElementFacade::iterator MOGL_ItListe_ElementFacade;
    // **********************************

  protected :
    MOGL_Liste_ElementFacade _ListeElementFacade;

  public :
    MOGL_Facade();
    void Afficher();
    void AjouterElement(MOGL_ElementFacade * p_Element);
};

#endif
