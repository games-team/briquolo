/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_LENSFLARENORMAL
#define MOGL_LENSFLARENORMAL

#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "MOGL_LensFlare.h"

class MOGL_LensFlareNormal: public MOGL_LensFlare
{
  public:
        /**
           Constructeur par d�faut
         */
        MOGL_LensFlareNormal();

        void TnL(MOGL_Afficheur * p_Afficheur);
};

#endif
