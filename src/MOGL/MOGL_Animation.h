/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ANIMATION
#define MOGL_ANIMATION

#include <map>

#include "MOGL_Noeud.h"
#include "MOGL_Structure.h"
#include "MOGL_MatriceTransformation.h"

using namespace std;

// Penser � utiliser upper_bound et lower_bound

/**
* Animation d'un MOGL_Noeud.
* Permet d'animer un MOGL_Noeud � partir de clefs indiquants des translations et des rotations
* associ�e � un temps
*
* @see MOGL_Noeud
*/

class MOGL_Animation
{
        // **** Types locaux � la classe ****
        typedef map <float, MOGL_Struct_Vecteur> MOGL_Map_Vecteur;
        typedef MOGL_Map_Vecteur::iterator MOGL_ItMap_Vecteur;
        // **********************************

  protected:
        bool SetMatrice(float _Temps);
        float InterpolerAngle(float p_AngleDeb,float p_AngleFin, float p_Avancement);
        int Signe(float p_Nombre);

        MOGL_Noeud * _Noeud;
        MOGL_Map_Vecteur _MapTranslation;
        MOGL_Map_Vecteur _MapRotation;
        float _UniteTemps;              // Dur�e de l'unit� du temps sp�cifi�e pour les keyframes

        float _TempsInitial;
        float _TempsMin, _TempsMax;
                

  public:
        /**
         * Constructeur par d�faut
         * Il est necessaire de pr�ciser le @ref MOGL_Noeud sur lequel
         * l'animation s'applique avant de l'utiliser
         *
         */
        MOGL_Animation();
        MOGL_Animation(MOGL_Noeud * p_Noeud);
        void SetNoeud(MOGL_Noeud * p_Noeud);
        void AjouterClefRotation(float p_Temps, const MOGL_Struct_Vecteur & p_Rotation);
        void AjouterClefTranslation(float p_Temps, const MOGL_Struct_Vecteur & p_Translation);


        void Initialiser();

        bool SetTemps();        //renvoie false si c'est la fin de l'anim
        void SetUniteTemps(float p_Temps);
};

#endif
