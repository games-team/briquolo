/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TEXTURE
#define MOGL_TEXTURE

#include <map>

#include "MOGL_MatriceTransformation.h"
#ifdef _WIN32
  #include <windows.h>
  #include <windowsx.h>
#endif
#include <GL/gl.h>
#include "MOGL_Structure.h"
#include "MOGL_Image.h"

#include <fstream>

class MOGL_Texture: public MOGL_Image
{
    // **** Types locaux � la classe ****
  public:
    /**
       Indique le type de texture.
    */
    enum MOGL_Enum_TypeTexture
    {
      TEXTURE_OPAQUE, /**< La texture est totalement opaque, quelle que soit la valeur alpha.
                         Elle peut cependant �tre trou�e. @sa SetCouleurTrou() SetCouleurTrouOctet()*/
      TEXTURE_TRANSPARENTE, /**< La texture est transparente, avec un mode de tranparence <i>normal</i> d�fini � partir
                               de la valeur alpha. Elle peut aussi �tre trou�e. @sa SetCouleurTrou() SetCouleurTrouOctet()*/
      TEXTURE_SATURE, /**< La texture est transparente, avec un mode de tranparence <i>saturant</i> d�fini � partir
                         de la valeur alpha. Elle peut aussi �tre trou�e. @sa SetCouleurTrou() SetCouleurTrouOctet()*/
    };
                
    // **********************************
  protected :
    void _Init();
    void _AnimerTexture();
                
    GLuint _Nom;
    bool _RepeterTexture;
    MOGL_Enum_TypeTexture _TypeTexture;
    bool _Animer;
    float _VitesseX, _VitesseY, _VitesseRotation;
    float _TranslationX, _TranslationY, _Rotation;
    float _Temps;
    bool _MipMap;

  public :
    /**
       Constructeur.
       @param p_TypeTexture : indique le type de texture (par d�faut la texture est opaque).
       @sa MOGL_Type_Texture.
    */
    MOGL_Texture(MOGL_Enum_TypeTexture p_TypeTexture=TEXTURE_OPAQUE, bool p_AutoriserTrou=false, bool p_MipMap=true);

    /**
       Destructeur.
    */
    ~MOGL_Texture();

    /**
       Permet de charger la texture � partir d'une image BMP.
       @remark Une image BMP ne poss�de jamais de param�tre alpha, donc
       dans le cas de l'utilisation du mode ALPHA_FICHIER, la valeur alpha vaut toujours 1.
       @param p_NomFichier : nom du fichier � charger.
       @return <i>true</i> si il n'y a pas eu de probl�me, <i>false</i> sinon.
       @sa SetGestionModeAlpha() SetGestionModeRGB()
    */
    bool ChargerBMP(const char * p_NomFichier);

    /**
       Permet de charger la texture � partir d'une image PNG.
       @remark Une image PNG peut ne pas poss�der de param�tre alpha donc
       dans ce cas, l'utilisation du mode ALPHA_FICHIER donne une valeur alpha de 1.
       @param p_NomFichier : nom du fichier � charger.
       @return <i>true</i> si il n'y a pas eu de probl�me, <i>false</i> sinon.
       @sa SetGestionModeAlpha() SetGestionModeRGB()
    */
    bool ChargerPNG(const char * p_NomFichier);

    /**
       Permet de r�cup�rer le type de texture. Le type de la texture est d�fini � la construction de celle-ci.
       @return Le type de texture.
       @sa MOGL_Type_Texture MOGL_Texture.
    */
    MOGL_Enum_TypeTexture GetTypeTexture() const;

    /**
       Permet de d�finir s'il faut ou no animer la texture
       Par d�faut elle ne l'est pas
       @param p_Animer : <i>true</i> pour animer, <i>false</i> sinon.
       @sa GetAnimation() SetVitesseTranslation() SetVitesseRotation()
    */
    void SetAnimation(bool p_Animer);

    /**
       Indique si la texture est anim�e.
       @return <i>true</i> si la texture est anim�e, <i>false</i> sinon.
       @sa SetAnimation() SetVitesseTranslation() SetVitesseRotation()
    */
    bool GetAnimation() const;
        
    /**
       Permet de sp�cifier la vitesse de translation de la texture.
       Cette vitesse est utilis�e si la texture est anim�e (cf SetAnimation()).
       @param p_VitesseX : vitesse de translation en largeur/seconde.
       La largeur de la texture est �gale � 1 ainsi une vitesse de 5, indique que
       la texture est d�plac�e de 5 fois sa largeur en une seconde.
       @param p_VitesseY : vitesse de translation en hauteur/seconde.
       La hauteur de la texture est �gale � 1 ainsi une vitesse de 5, indique que
       la texture est d�plac�e de 5 fois sa hauteur en une seconde.
       @sa SetAnimation() GetAnimation(), SetVitesseRotation()
    */
    void SetVitesseTranslation(float p_VitesseX, float p_VitesseY);

    /**
       Permet de sp�cifier la vitesse de rotation de la texture.
       Cette vitesse est utilis�e si la texture est anim�e (cf SetAnimation()).
       @param p_VitesseRotation : vitesse de rotation en degr� par seconde.
       @sa SetAnimation() GetAnimation(), SetVitesseTranslation()
    */
    void SetVitesseRotation(float p_VitesseRotation);
        

    /**
       Permet de s�lectionner la texture au niveau d'OpenGL.
    */
    void Selectionner();

    /**
       Permet de d�finir si la texture doit �tre r�p�t�e ou non.
    */
    void RepeterTexture(bool p_Val);
};

#endif
