/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_ElementPanneau.h"
#include "MOGL_Bibliotheque.h"

#include "MOGL_Panneau.h"

MOGL_ElementPanneau::MOGL_ElementPanneau(MOGL_Panneau * p_Panneau, bool p_GestionFocus)
  : _Police(NULL), _Focus(false), _GestionFocus(p_GestionFocus), _Cacher(false), _Panneau(p_Panneau)
{
  _CouleurFondFocus.r=.5;
  _CouleurFondFocus.g=.5;
  _CouleurFondFocus.b=.5;
  _CouleurFondFocus.a=0.5;
  p_Panneau->_AjouterElementPanneau(this);
}

MOGL_ElementPanneau::~MOGL_ElementPanneau()
{
}

void MOGL_ElementPanneau::SetPolice(MOGL_Police * p_Police)
{
  _Police=p_Police;
}

void MOGL_ElementPanneau::Afficher()
{
  if (!_Police)
  {
    _Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
  }
}

void MOGL_ElementPanneau::ClavierUp(SDL_keysym * key)
{
}

void MOGL_ElementPanneau::ClavierDown(SDL_keysym * key)
{
}

void MOGL_ElementPanneau::DeplacementSouris(SDL_MouseMotionEvent * p_Event)
{
}

const MOGL_Police * MOGL_ElementPanneau::GetPolice() const
{
  return _Police;
}

void MOGL_ElementPanneau::SetCouleurFondFocus(const MOGL_Struct_Couleur & p_Couleur)
{
  _CouleurFondFocus=p_Couleur;
}

MOGL_Struct_Couleur MOGL_ElementPanneau::GetCouleurFondFocus() const
{
  return _CouleurFondFocus;
}

bool MOGL_ElementPanneau::GetCacher() const
{
  return _Cacher;
}

void MOGL_ElementPanneau::SetCacher(bool p_Cacher)
{
  _Cacher=p_Cacher;
}
