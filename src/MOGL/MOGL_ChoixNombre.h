/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_CHOIXNOMBRE
#define MOGL_CHOIXNOMBRE

#include "MOGL_ElementPanneau.h"
#include "MOGL_Signal.h"
#include <string>

using namespace std;

class MOGL_ChoixNombre: public MOGL_ElementPanneau
{
  protected:
    string _Label;
    MOGL_Struct_Couleur _CouleurTexte;
    float _Nombre;
    float _Variation;
    float _Min, _Max;

  public: // SIGNAUX
    MOGL_Signal1<float> ChangementNombre;

  public:
    MOGL_ChoixNombre(MOGL_Panneau * p_Panneau, const string & p_Label, float p_NombreInit=0, float p_Variation=0.1);
    void SetLabel(const string & p_Label);
    string GetLabel() const;

    void Afficher();
    unsigned int GetHauteurElement() const;
    unsigned int GetLargeurElement() const;
    void SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur);
    MOGL_Struct_Couleur GetCouleurLabel() const;
    float GetNombre() const;
    void SetNombre(float p_Nombre);
    float GetVariation() const;
    void SetVariation(float p_Variation);

    void ClavierUp(SDL_keysym * key);

    void SetMin(float p_Min);
    void SetMax(float p_Max);
    float GetMin() const;
    float GetMax() const;
};

#endif
