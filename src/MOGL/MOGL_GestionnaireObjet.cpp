/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "../I18n.h"
#include "MOGL_GestionnaireObjet.h"

bool MOGL_GestionnaireObjet::ChargerObjetASCTriangle(const char * p_NomFichier, const MOGL_GestionnaireTexture & p_GM,
                                                     MOGL_Objet & p_Objet)
{
  char  chaine[200];
  int   i,j;
  char  temp[50];
  float x,y,z,u,v,xn,yn,zn;
  int index_point=0;
  MOGL_Struct_Point Pt[3];
  MOGL_Texture * Texture=NULL;

  ifstream Fichier(p_NomFichier);


  if (Fichier.fail())
  {     return false;
  }


  do
  {     Fichier.getline(chaine,200);
  } while (strncmp(chaine,"FACE",4));

  do
  {
    Fichier.getline(chaine,200);

    if (!strncmp(chaine,"FACE",4))
    {
      p_Objet.AjouterTriangle(Pt[0],Pt[1],Pt[2],Texture);
      index_point=0;
    }
    if (!strncmp(chaine,"Material",8))
    {
      // Quelle est la texture ?
      i=0;
      while(chaine[i]!='"') i++;
      i++;
      j=i;
      while(chaine[j]!='"') j++;
      strncpy(temp,chaine+i,j-i);
      temp[j-i]=0;
      Texture=p_GM.GetTexture(temp);
      if (Texture==NULL)
      {
        cerr<< "Texture" << " \""<<temp<<"\" "<< "not found"<<endl;
      }

    }

    if (!strncmp(chaine,"Vertex",6))
    {   // Lecture des coordonn�es d'un point


      i=0;

      while(chaine[i]!='X') i++;
      i+=2;
      sscanf(chaine+i,"%f",&x);

      while(chaine[i]!='Y') i++;
      i+=2;
      sscanf(chaine+i,"%f",&y);

      while(chaine[i]!='Z') i++;
      i+=2;
      sscanf(chaine+i,"%f",&z);

      while(chaine[i]!='U') i++;
      i+=2;
      sscanf(chaine+i,"%f",&u);

      while(chaine[i]!='V') i++;
      i+=2;
      sscanf(chaine+i,"%f",&v);

      while(chaine[i]!='x') i++;
      i+=2;
      sscanf(chaine+i,"%f",&xn);

      while(chaine[i]!='y') i++;
      i+=2;
      sscanf(chaine+i,"%f",&yn);

      while(chaine[i]!='z') i++;
      i+=2;
      sscanf(chaine+i,"%f",&zn);

      Pt[index_point].xp=x;
      Pt[index_point].yp=y;
      Pt[index_point].zp=z;

      float norm=sqrt(xn*xn + yn*yn + zn*zn);
      Pt[index_point].xn=xn/norm;
      Pt[index_point].yn=yn/norm;
      Pt[index_point].zn=zn/norm;

      Pt[index_point].xt=u;
      Pt[index_point].yt=v;

      index_point++;
    }
  } while(!Fichier.eof());
  p_Objet.AjouterTriangle(Pt[0],Pt[1],Pt[2],Texture);

  return true;
}


bool MOGL_GestionnaireObjet::ChargerObjetASCStrip(const char * p_NomFichier, const MOGL_GestionnaireTexture & p_GM, MOGL_Objet & p_Objet)
{
        char  chaine[200];
        int   i,j;
        char  temp[50];
        float x,y,z,u,v,xn,yn,zn;
        bool strip=false;
        MOGL_Struct_Point Point;
        MOGL_Liste_Point ListePoint;
        MOGL_Texture * Texture=NULL;

        int NbTotal=0;

        ifstream Fichier(p_NomFichier);


        if (Fichier.fail())
        {       return false;
        }

        do
        {       Fichier.getline(chaine,200);
        } while (strncmp(chaine,"TRIANGLE",8) && strncmp(chaine,"STRIP",4));

        if (!strncmp(chaine,"STRIP",5))
        {
                strip=true;
        }
        else
        {
                strip=false;
        }
        do
        {
                Fichier.getline(chaine,200);

                if (!strncmp(chaine,"TRIANGLE",8))
                {
                        if (strip)
                        {
                                MOGL_Strip Strip;
                                Strip.SetTexture(Texture);
                                MOGL_ItListe_Point it;
                                for(it=ListePoint.begin();it!=ListePoint.end();it++)
                                {
                                        Strip.AjouterPoint(*it);
                                }
                                p_Objet.AjouterStrip(Strip);
                                NbTotal+=Strip.NbPoint()-2;
                        }
                        else
                        {
                                p_Objet.AjouterTriangle(ListePoint[0],ListePoint[1],ListePoint[2],Texture);
                                NbTotal++;

                        }
                        ListePoint.clear();
                        strip=false;
                }
                if (!strncmp(chaine,"STRIP",5))
                {
                        if (strip)
                        {
                                MOGL_Strip Strip;
                                Strip.SetTexture(Texture);
                                MOGL_ItListe_Point it;
                                for(it=ListePoint.begin();it!=ListePoint.end();it++)
                                {
                                        Strip.AjouterPoint(*it);
                                }
                                p_Objet.AjouterStrip(Strip);
                                NbTotal+=Strip.NbPoint()-2;
                        }
                        else
                        {
                                p_Objet.AjouterTriangle(ListePoint[0],ListePoint[1],ListePoint[2],Texture);
                                NbTotal++;

                        }
                        ListePoint.clear();
                        strip=true;
                }
                if (!strncmp(chaine,"Material",8))
                {
                        // Quelle est la texture ?
                        i=0;
                        while(chaine[i]!='"') i++;
                        i++;
                        j=i;
                        while(chaine[j]!='"') j++;
                        strncpy(temp,chaine+i,j-i);
                        temp[j-i]=0;
                        Texture=p_GM.GetTexture(temp);

                }

                if (!strncmp(chaine,"Vertex",6))
                {       // Lecture des coordonn�es d'un point
                        i=0;



                        while(chaine[i]!='X') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&x);

                        while(chaine[i]!='Y') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&y);

                        while(chaine[i]!='Z') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&z);

                        while(chaine[i]!='U') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&u);

                        while(chaine[i]!='V') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&v);

                        while(chaine[i]!='x') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&xn);

                        while(chaine[i]!='y') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&yn);

                        while(chaine[i]!='z') i++;
                        i+=2;
                        sscanf(chaine+i,"%f",&zn);

                        Point.xp=x;
                        Point.yp=y;
                        Point.zp=z;

                        Point.xn=xn;
                        Point.yn=yn;
                        Point.zn=zn;

                        Point.xt=u;
                        Point.yt=v;

                        ListePoint.push_back(Point);
                }
        } while(!Fichier.eof());

        if (strip)
        {
                MOGL_Strip Strip;
                Strip.SetTexture(Texture);
                MOGL_ItListe_Point it;
                for(it=ListePoint.begin();it!=ListePoint.end();it++)
                {
                        Strip.AjouterPoint(*it);
                }
                p_Objet.AjouterStrip(Strip);
                NbTotal+=Strip.NbPoint()-2;

        }
        else
        {
                p_Objet.AjouterTriangle(ListePoint[0],ListePoint[1],ListePoint[2],Texture);
                NbTotal++;
        }

        char ch[10];
//      _itoa(NbTotal,ch,10);
        sprintf(ch,"%d",NbTotal);

        //MessageBox(NULL,ch,"Nb Points Charg�s",MB_OK|MB_ICONSTOP);

        return true;
}

