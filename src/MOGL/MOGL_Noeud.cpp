/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Noeud.h"

MOGL_Noeud::MOGL_Noeud() : MOGL_ElementArbre()
{
}

MOGL_Noeud::MOGL_Noeud(const MOGL_Noeud & p_Noeud): MOGL_ElementArbre(p_Noeud)
{
}

MOGL_Noeud::~MOGL_Noeud()
{
}

void MOGL_Noeud::AjouterElement(MOGL_ElementArbre * p_Element)
{
  _SetElement.insert(p_Element);
}

bool MOGL_Noeud::RetirerElement(MOGL_ElementArbre * p_Element)
{
  MOGL_ItSet_ElementArbre it=_SetElement.find(p_Element);
  if (it!=_SetElement.end())
  {
        _SetElement.erase(it);
        return true;
  }
  else
  {
        return false;
  }
}

void MOGL_Noeud::TnL(MOGL_Afficheur * p_Afficheur)
{
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  MOGL_ItSet_ElementArbre it;
  for(it=_SetElement.begin();it!=_SetElement.end();it++)
  {
        (*it)->TnL(p_Afficheur);
  }
  glPopMatrix();
}
