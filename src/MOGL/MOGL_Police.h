/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_POLICE
#define MOGL_POLICE

#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <SDL.h>
#include <iconv.h>
#include "MOGL_Structure.h"

using namespace std;

class MOGL_Police
{
  public:
    /**
       Constructeur
    */
    MOGL_Police();

    /**
       Destructeur
    */
    virtual ~MOGL_Police();

    /**
       Permet de Charger  une police de caract�res.
    */
    virtual bool Charger(const char * p_NomFichier) = 0;

    /**
       Permet d'afficher une chaine de caract�res.
       @param p_Chaine : chaine de caract�res � afficher.
       @param p_X : coordonn�e X de la chaine de caract�res.
       @param p_Y : coordonn�e Y de la chaine de caract�res.       
    */
    virtual void Afficher(const string & p_Chaine, int p_X, int p_Y) const ;

    /**
       Permet d'afficher une chaine de caract�res.
       @param p_Chaine : chaine de caract�res � afficher.
       @param p_X : coordonn�e X de la chaine de caract�res.
       @param p_Y : coordonn�e Y de la chaine de caract�res.       
       @param p_CouleurTexte : couleur de la chaine de caract�res.
    */
    virtual void Afficher(const string & p_Chaine, int p_X, int p_Y, const MOGL_Struct_Couleur & p_CouleurTexte) const = 0;

    /**
       Permet d'afficher une chaine de caract�res.
       @param p_Chaine : chaine de caract�res � afficher.
       @param p_X : coordonn�e X de la chaine de caract�res.
       @param p_Y : coordonn�e Y de la chaine de caract�res.       
       @param p_CouleurTexte : couleur de la chaine de caract�res.
       @param p_CouleurFond : couleur du fond.
    */
    virtual void Afficher(const string & p_Chaine, int p_X, int p_Y,
                          const MOGL_Struct_Couleur & p_CouleurTexte, const MOGL_Struct_Couleur & p_CouleurFond) const = 0;

    /**
       Permet de r�cup�rer la largeur d'un caract�re de la police.
       @param p_Caractere : caract�re dont on veut la largeur.
       @return la largeur du caract�re en pixels.
    */
    virtual unsigned int GetTailleXCaractere(Uint16 p_Caractere) const = 0;

    /**
       Permet de r�cup�rer la hauteur d'un caract�re de la police.
       @param p_Caractere : caract�re dont on veut la hauteur.
       @return la hauteur du caract�re en pixels.
    */
    virtual unsigned int GetTailleYCaractere(Uint16 p_Caractere) const = 0;

    /**
       Permet de r�cup�rer la largeur maximale des caract�res de la police.
       @return la largeur en pixels.
    */
    unsigned int GetTailleXMax() const;

    /**
       Permet de r�cup�rer la hauteur maximale des caract�res de la police.
       @return la hauteur en pixels.
    */
    unsigned int GetTailleYMax() const;

    /**
       Permet de r�cup�rer la largeur d'une chaine de caract�res.
       @param p_Caractere : chaine de caract�res dont on veut la largeur.
       @return la largeur de la chaine de caract�res en pixels.
    */
    unsigned int GetTailleXChaine(const string & p_Chaine) const;

        
    /**
       Permet de recharger la police.
    */
    virtual bool Recharger() = 0;

  protected:
    /**
       Permet de lire un entier depuis un fichier de mani�re ind�pendant de l'endian.
    */
    unsigned int _ReadUnsignedInt(ifstream & file);


  protected:
    unsigned int _LargeurMax;
    unsigned int _HauteurMax;
    string _NomFichier;
    iconv_t _IconvDesc;

    Uint16 * _AllocAndConvertToUCS2(string p_Chaine,unsigned int & size) const;
};

#endif
