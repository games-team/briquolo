/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_CHOIX
#define MOGL_CHOIX

#include "MOGL_ElementPanneau.h"
#include "MOGL_Signal.h"
#include <string>
#include <vector>

using namespace std;

class MOGL_Choix: public MOGL_ElementPanneau
{
    // **** Types locaux � la classe ****
    typedef vector <string> MOGL_Vector_String;
    typedef MOGL_Vector_String::iterator MOGL_ItVector_String;
    // **********************************
  protected:
    string _Label;
    MOGL_Vector_String _Choix;
    unsigned int _IndiceCourant;
    MOGL_Struct_Couleur _CouleurTexte;

  public: // SIGNAUX
    MOGL_Signal1<unsigned int> ChangementElement;

  public:
    MOGL_Choix(MOGL_Panneau * p_Panneau, const string & p_Label);
    void SetLabel(const string & p_Label);
    string GetLabel() const;
    void AjouterElementChoix(const string & p_ElementChoix);
    void ViderElementsChoix();
    string GetElement(unsigned int p_Indice) const;
    unsigned int GetNbElement() const;

    unsigned int GetIndiceCourant() const;
    void SetIndiceCourant(unsigned int p_Indice);
    
    string GetElementChoixCourant() const;
    int GetIndiceElement(const string & p_Element) const;

    unsigned int GetHauteurElement() const;
    unsigned int GetLargeurElement() const;
    void Afficher();
    void SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur);
    MOGL_Struct_Couleur GetCouleurLabel() const;


    void ClavierUp(SDL_keysym * key);
};

#endif
