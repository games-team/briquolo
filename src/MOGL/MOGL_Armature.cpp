/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Armature.h"

#include <iostream>

MOGL_Armature::MOGL_Armature(): MOGL_Noeud(), _ModifiePoint(true), _InvMatriceBase(NULL)
{
}

void MOGL_Armature::CalculerPoints()
{

        MOGL_ItVecteur_PointSimple itPoint;
        unsigned int num=0;
        for(itPoint=_VecteurPoint.begin();itPoint!=_VecteurPoint.end();itPoint++)
        {
                MOGL_Struct_Vecteur vec,pt,normal;
                vec.x=itPoint->xp;
                vec.y=itPoint->yp;
                vec.z=itPoint->zp;
                pt=_MatriceTransformationPoint.MultiplierVecteur(vec);

                vec.x=itPoint->xn;
                vec.y=itPoint->yn;
                vec.z=itPoint->zn;
                
                normal=_MatriceTransformationPoint.EffectuerRotationVecteur(vec);
                
                MOGL_Struct_PointSimple ptSimple;
                ptSimple.xp=pt.x;
                ptSimple.yp=pt.y;
                ptSimple.zp=pt.z;
                ptSimple.xn=normal.x;
                ptSimple.yn=normal.y;
                ptSimple.zn=normal.z;
                _VecteurPointTransforme[num]=ptSimple;

                num++;
        }
}


void MOGL_Armature::TnL(MOGL_Afficheur * p_Afficheur)
{

        //ReinitialiserPosition();

        glPushMatrix();
        _Matrice.MultiplierDansOpenGL();
        _MatriceFinale.PrendreDeOpenGL();

        _MatriceTransformationPoint=_MatriceFinale;
        if (_InvMatriceBase!=NULL)
        {
          _MatriceTransformationPoint.Multiplier(*_InvMatriceBase);
        }
        
        if (_ModifieMatrice || _ModifiePoint)
        {
                // La matrice a �t� transform�, alors on recalcule les points
                CalculerPoints();
                _ModifieMatrice=false;
                _ModifiePoint=false;
        }
        

        MOGL_ItSet_ElementArbre it;
        int in=0;
        for(it=_SetElement.begin();it!=_SetElement.end();it++)
        {
                in++;
                (*it)->TnL(p_Afficheur);
        }
        glPopMatrix();
}

void MOGL_Armature::AjouterPoint(unsigned int p_NumPoint, const MOGL_Struct_PointSimple & p_Point)
{
        if (_VecteurPoint.size()<=p_NumPoint)
        {
                _VecteurPoint.resize(p_NumPoint+1);
        }
        _VecteurPoint[p_NumPoint]=p_Point;
}

void MOGL_Armature::AjouterPoint(unsigned int p_NumPoint, GLfloat xp, GLfloat yp, GLfloat zp,
                                                                                             GLfloat xn, GLfloat yn, GLfloat zn)

{
        MOGL_Struct_PointSimple Pt;
        Pt.xp=xp;
        Pt.yp=yp;
        Pt.zp=zp;
        Pt.xn=xn;
        Pt.yn=yn;
        Pt.zn=zn;
        
        if (_VecteurPoint.size()<=p_NumPoint)
        {
                _VecteurPoint.resize(p_NumPoint+1);
                _VecteurPointTransforme.resize(p_NumPoint+1);
        }
        _VecteurPoint[p_NumPoint]=Pt;
}

MOGL_Struct_PointSimple MOGL_Armature::GetPointInitial(unsigned int p_Indice)
{
        return _VecteurPoint[p_Indice];
}

MOGL_Struct_PointSimple MOGL_Armature::GetPointTransforme(unsigned int p_Indice)
{
        return _VecteurPointTransforme[p_Indice];
}

void MOGL_Armature::ReinitialiserPosition()
{
        _Matrice=_MatriceInitiale;
        _ModifieMatrice=true;
}

void MOGL_Armature::SetMatriceInitialisation(const MOGL_MatriceTransformation & p_Matrice)
{
        _MatriceInitiale=p_Matrice;
}

void MOGL_Armature::SetInvMatriceBase(const MOGL_MatriceTransformation * p_InvMatriceBase)
{
  _InvMatriceBase=p_InvMatriceBase;
}
