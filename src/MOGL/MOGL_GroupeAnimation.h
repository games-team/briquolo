/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_GROUPEANIMATION
#define MOGL_GROUPEANIMATION

#include <set>

#include "MOGL_Animation.h"

using namespace std;

class MOGL_GroupeAnimation
{
        // **** Types locaux � la classe ****
        typedef list <MOGL_Animation *> MOGL_Set_Animation;
        typedef MOGL_Set_Animation::iterator MOGL_ItSet_Animation;
        // **********************************
        
  protected:
        MOGL_Set_Animation _SetAnimation;

  public:
        MOGL_GroupeAnimation();

        void AjouterAnimation(MOGL_Animation * p_Animation);

        void Initialiser();
        void SetUniteTemps(float p_Temps);

        bool SetTemps();        //renvoie false si c'est la fin de l'anim
};

#endif
