/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_FENETREKIT
#define MOGL_FENETREKIT

#include "MOGL_Action.h"
#include "MOGL_Fenetre.h"
#include "MOGL_Univers.h"
#include "MOGL_Facade.h"
#include "MOGL_Camera.h"
#include "MOGL_Texte.h"
#include "MOGL_GestionnaireTexture.h"

/**
 * Cet objet a pour but de fournir les objets de base n�cessaires � une utilisation de base de MOGL.
 * Les objets constuits sont :
 * - MOGL_Fenetre qui repr�sente la fen�tre d'affichage (640x480x16 en mode fen�tr�)
 * - MOGL_Univers qui repr�sente l'univers � repr�senter (vide)
 * - MOGL_Facade qui repr�sente une facade (sur laquelle est affich� le nombre de FPS)
 * - MOGL_GestionnaireTexture qui repr�sente un gestionnaire de texture (vide)
 * .
 * Un param�trage de base est effectu� afin que les objets soit directement utilisable sans pr�caution particuliaire.
 * En outre une connection <i>MOGL_Signal/MOGL_Action</i> est effectu�e afin de diriger la cam�ra � l'aide du clavier
 * et de la souris :
 * - La souris pour tourner la cam�ra.
 * - Les fl�ches du clavier pour d�placer la cam�ra (haut->avancer ; bas->reculer ; gauche->d�placement sur la gauche ; droite->d�placement sur la droite).
 * .
 * Les objets construits sont accessibles avec les m�thodes GetFenetre(), GetUnivers(), GetFacade(), GetCamera(), GetGestionnaireTexture().
 * La construction de l'objet MOGL_FenetreKit se fait de la mani�re habituelle. Par contre la m�thode Initialiser() doit �tre appel�e
 * avant la plupart des m�thodes de MOGL (cf MOGL_Fenetre::Initialiser()).
 */
class MOGL_FenetreKit: public MOGL_Action
{
  protected:
    MOGL_Fenetre _Fenetre;
    MOGL_Univers _Univers;
    MOGL_Facade _Facade;
    MOGL_Camera _Camera;
    MOGL_Texte _TexteFPS;
    MOGL_GestionnaireTexture _GestionnaireTexture;

    // MOGL_Action
    void Idle();
    void Clavier(SDL_keysym * key);
    void DeplacementSouris(SDL_MouseMotionEvent * p_Event);

  public:
    /** 
     * Constructeur.
     * 
     * @param p_Titre : titre de la fen�tre
     */
    
    MOGL_FenetreKit(char * p_Titre=NULL);

    /**
       Permet d'initialiser la fen�tre d'affichage ainsi que tout l'environnement MOGL.
       Cette initialisation doit �tre effectu�e avant la plupart des op�rations pouvant �tre effectu�es par MOGL
       (chargement de texture...)
       @return <i>true</i> s'il n'y a pas eu de probl�me, et <i>false</i> sinon.
    */
    bool Initialiser();

    /**
       Permet de Lancer la boucle d'�v�nement de MOGL. La m�thode ne redonne la main que lorsque la boucle d'�v�nement est termin�e,
       ce qui signifie que l'ex�cution de MOGL est achev�e.
       @return <i>true</i> s'il n'y a pas eu de probl�me, <i>false</i> sinon.
    */
    bool LancerBoucle();

    MOGL_Fenetre * GetFenetre();
    MOGL_Univers * GetUnivers();
    MOGL_Facade * GetFacade();
    MOGL_Camera * GetCamera();
    MOGL_GestionnaireTexture * GetGestionnaireTexture();
};

#endif
