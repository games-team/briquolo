/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_LensFlare.h"

MOGL_LensFlare::MOGL_LensFlare(): MOGL_ElementArbre()
{
}

void MOGL_LensFlare::TnL(MOGL_Afficheur * p_Afficheur)
{
  _Afficheur=p_Afficheur;
  
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  _MatriceFinale.PrendreDeOpenGL();

  _xPos=_MatriceFinale.Lit(3,0);
  _yPos=_MatriceFinale.Lit(3,1);
  _zPos=_MatriceFinale.Lit(3,2);

  _GenererLensFlare();

  _Intensite=fabs(_zPos/sqrt(_xPos*_xPos+_yPos*_yPos+_zPos*_zPos));
  
  glPopMatrix();
}

void MOGL_LensFlare::_DessinerCarre(MOGL_Texture * p_Texture, float p_CentreX, float p_CentreY, float p_CentreZ, 
                                                                        float p_Position, float p_Taille, const MOGL_Struct_Couleur & p_Couleur)
{
  _Afficheur->AjouterPartieLensFlare(p_Texture, p_Position, p_CentreX, p_CentreY, p_CentreZ, p_Taille, p_Couleur);
  
}

void MOGL_LensFlare::_DessinerCarreTailleFixe(MOGL_Texture * p_Texture, float p_CentreX, float p_CentreY, float p_CentreZ, 
                                                                                          float p_Position, float p_Taille, const MOGL_Struct_Couleur & p_Couleur)
{
  _Afficheur->AjouterPartieLensFlareTailleFixe(p_Texture, p_Position, p_CentreX, p_CentreY, p_CentreZ, p_Taille, p_Couleur);
}

