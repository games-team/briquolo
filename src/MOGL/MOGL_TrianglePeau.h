/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TRIANGLEPEAU
#define MOGL_TRIANGLEPEAU

#include "MOGL_Structure.h"
#include "MOGL_Armature.h"
#include "MOGL_Texture.h"

/**
Repr�sente un triangle utilis� par MOGL_Peau. Les points du triangle sont
soit des r�f�rences vers des points internes au MOGL_Peau (auquel cas on
indique juste un indice), soit des r�f�rences vers des points externes attach�s
� des MOGL_Armature (auquel cas on indique un pointeur vers le MOGL_Armature,
et un indice.
*/
class MOGL_TrianglePeau
{
  protected :
        unsigned int _IndiceReferencePoint[3];
        MOGL_Armature * _ArmatureReferencePoint[3];
        MOGL_Struct_PointTexture _PointTexture[3];
        MOGL_Texture * _Texture;
  public :
        /**
           Constructeur
        */
        MOGL_TrianglePeau();

        /**
           Permet de r�initialiser le triangle.
        */
        void Reinitialiser();
        
        /**
           Permet de r�cup�rer la texture de la face.
           @return Pointeur vers MOGL_Texture.
        */
        MOGL_Texture * GetTexture() const;

        /**
           Permet de d�finir la texture de la face.
           @param p_Texture : pointeur vers un MOGL_Texture
        */
        void SetTexture(MOGL_Texture * p_Texture);

        /**
           Permet de d�finir un point interne.
           @param p_IndicePoint : indice du point du triangle � d�finir (doit �tre compris entre 0 et 2)
           @param p_IndiceReferenceInterne : indice du point r�f�renc�.
        */
        void SetPoint(unsigned int p_IndicePoint, unsigned int p_IndiceReferenceInterne);

        /**
           Permet de d�finir un point externe.
           @param p_IndicePoint : indice du point du triangle � d�finir (doit �tre compris entre 0 et 2)
           @param p_Armature : pointeur vers le MOGL_Armature du point externe.
           @param p_IndiceReferenceExterne : indice du point r�f�renc� dans le MOGL_Armature.
        */
        void SetPoint(unsigned int p_IndicePoint, MOGL_Armature * p_Armature, unsigned int p_IndiceReferenceExterne);

        /**
           Permet de r�cup�rer la r�f�rence d'un point du triangle.
           La r�f�rence peut �tre interne ou externe, il s'agit juste de l'indice.
           @param p_IndicePoint : indice du point du triangle (doit �tre compris entre 0 et 2)
           @return Indice du point.
        */
        unsigned int GetReferencePoint(unsigned int p_IndicePoint);

        /**
           Permet de r�cup�rer le MOGL_Armature d'un point du triangle.
           Cette m�thode n'a de sens que pour un point correspondant � une r�f�rence externe.
           @param p_IndicePoint : indice du point du triangle (doit �tre compris entre 0 et 2)
           @return Le Pointeur vers le MOGL_Armature de la r�f�rence externe (ou NULL s'il s'agit d'une r�f�rence interne).
        */
        MOGL_Armature * GetArmaturePoint(unsigned int p_IndicePoint);

        /**
           Permet de savoir si le point est une r�f�rence interne.
           @param p_IndicePoint : indice du point du triangle (doit �tre compris entre 0 et 2)
           @return <i>true</i> si le point est une r�f�rence interne, <i>false</i> sinon.
        */
        bool EstPointInterne(unsigned int p_IndicePoint);

        /**
           Permet de d�finir les coordonn�es de texture d'un point du triangle.
           @param p_Indice : indice du point du triangle (doit �tre compris entre 0 et 2)
           @param p_PointTexture : MOGL_Struct_PointTexture contenant les coordonn�es de texture.
        */
        void SetPointTexture(unsigned int p_Indice, MOGL_Struct_PointTexture p_PointTexture);

        /**
           Permet de r�cup�rer les coordonn�es de texture d'un point du triangle.
           @param p_Indice : indice du point du triangle (doit �tre compris entre 0 et 2)
           @return Le MOGL_Struct_PointTexture contenant les coordonn�es de texture.
        */
        MOGL_Struct_PointTexture GetPointTexture(unsigned int p_Indice);
};

#endif
