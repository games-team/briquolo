/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Texte.h"
#include "MOGL_Bibliotheque.h"
#include <fstream>

MOGL_Texte::MOGL_Texte(): MOGL_ElementFacade(), _X(0), _Y(0), _Police(NULL)
{
  _CouleurTexte.r=1;
  _CouleurTexte.g=1;
  _CouleurTexte.b=1;
  _CouleurTexte.a=1;

  _CouleurFond.r=0;
  _CouleurFond.g=0;
  _CouleurFond.b=0;
  _CouleurFond.a=0;
}

MOGL_Texte::MOGL_Texte(char * p_Chaine, int p_X, int p_Y, const MOGL_Police * p_Police)
  : _Chaine(p_Chaine), _X(p_X), _Y(p_Y), _Police(p_Police)
{
}

MOGL_Texte::MOGL_Texte(const string & p_Chaine,  int p_X, int p_Y, const MOGL_Police * p_Police)
  : _Chaine(p_Chaine), _X(p_X), _Y(p_Y), _Police(p_Police)
{
}

void MOGL_Texte::SetTexte(char * p_Chaine)
{
  _Chaine=string(p_Chaine);
}

void MOGL_Texte::SetTexte(const string & p_Chaine)
{
  _Chaine=p_Chaine;
}

void MOGL_Texte::SetPosition(int p_X, int p_Y)
{
  _X=p_X;
  _Y=p_Y;
}
        
const string & MOGL_Texte::GetString()
{
  return _Chaine;
}

void MOGL_Texte::SetPolice(MOGL_Police * p_Police)
{
  _Police=p_Police;
}

        
void MOGL_Texte::Afficher()
{
  if (!_Police)
  {
    _Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
  }
  
  if (_CouleurFond.a==0)
  {
    _Police->Afficher(_Chaine, _X, _Y, _CouleurTexte);
  }
  else
  {
    _Police->Afficher(_Chaine, _X, _Y, _CouleurTexte, _CouleurFond);
  }
}

void MOGL_Texte::SetCouleurTexte(const MOGL_Struct_Couleur & p_CouleurTexte)
{
  _CouleurTexte=p_CouleurTexte;
}

const MOGL_Struct_Couleur & MOGL_Texte::GetCouleurTexte()
{
  return _CouleurTexte;
}

void MOGL_Texte::SetCouleurFond(const MOGL_Struct_Couleur & p_CouleurFond)
{
  _CouleurFond=p_CouleurFond;
}

const MOGL_Struct_Couleur & MOGL_Texte::GetCouleurFond()
{
  return _CouleurFond;
}

unsigned int MOGL_Texte::GetTailleX() const
{
  if (!_Police)
  {
    const_cast<MOGL_Texte *>(this)->_Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
  }
  return _Police->GetTailleXChaine(_Chaine);
}

unsigned int MOGL_Texte::GetTailleY() const
{
  if (!_Police)
  {
    const_cast<MOGL_Texte *>(this)->_Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
  }
  return _Police->GetTailleYMax();
}
