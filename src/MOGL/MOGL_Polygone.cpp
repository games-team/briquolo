/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Polygone.h"

MOGL_Polygone::MOGL_Polygone(): MOGL_ElementFacade()
{
}

unsigned int MOGL_Polygone::AjouterPoint(unsigned int p_X, unsigned int p_Y, const MOGL_Struct_Couleur & p_Couleur)
{
  MOGL_Struct_PointPolygone pt;
  pt.X=p_X;
  pt.Y=p_Y;
  pt.Couleur=p_Couleur;
    
  _VectorPoint.push_back(pt);
  return _VectorPoint.size()-1;
}

void MOGL_Polygone::SetCouleur(unsigned int p_Indice, const MOGL_Struct_Couleur & p_Couleur)
{
  if (p_Indice< _VectorPoint.size())
  {
    _VectorPoint[p_Indice].Couleur=p_Couleur;
  }
}

void MOGL_Polygone::Afficher()
{
  glDisable(GL_TEXTURE_2D);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_DEPTH_TEST);

  glBegin(GL_POLYGON);
  for(MOGL_ItVector_Point it=_VectorPoint.begin(); it!=_VectorPoint.end(); it++)
  {
    glColor4f(it->Couleur.r, it->Couleur.g, it->Couleur.b, it->Couleur.a);
    glVertex2f(it->X, it->Y);
  }
  glEnd();
}

void MOGL_Polygone::SetPosition(unsigned int p_Indice, unsigned int p_X, unsigned int p_Y)
{
  if (p_Indice< _VectorPoint.size())
  {
    _VectorPoint[p_Indice].X=p_X;
    _VectorPoint[p_Indice].Y=p_Y;
  }
}
