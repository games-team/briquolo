/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_PARTICULE
#define MOGL_PARTICULE

#include "MOGL_Structure.h"
#include "MOGL_Texture.h"

class MOGL_Particule
{
    friend class MOGL_GenerateurParticule;

  protected:
    MOGL_Struct_Vecteur _Position;
    MOGL_Struct_Vecteur _Velocite;
    MOGL_Struct_Couleur _Couleur;
    GLfloat _Age, _DureeVie;
    GLfloat _Taille;
    bool _Vivant;

    MOGL_Texture * _Texture;
                
  public:
    MOGL_Particule();

    void Afficher();
};

#endif
