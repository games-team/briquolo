/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Texture.h"
#include "MOGL_Temps.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include <iostream>

MOGL_Texture::MOGL_Texture(MOGL_Enum_TypeTexture p_TypeTexture, bool p_AutoriserTrou, bool p_MipMap):
  MOGL_Image(p_AutoriserTrou), _Nom(0), _RepeterTexture(true), _TypeTexture(p_TypeTexture),
  _Animer(false), _VitesseX(0), _VitesseY(0), _VitesseRotation(0), _TranslationX(0), _TranslationY(0), _Rotation(0),
  _Temps(0), _MipMap(p_MipMap)

{
}

MOGL_Texture::~MOGL_Texture()
{
  if (_Nom!=0)
  {
    glDeleteTextures(1,&_Nom);
  }
}

void MOGL_Texture::Selectionner()
{
  if (_Animer)
  {
    _AnimerTexture();
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glTranslatef(_TranslationX, _TranslationY, 0);
    glTranslatef(0.5, 0.5, 0);
    glRotatef(_Rotation,0,0,1);
    glTranslatef(-0.5, -0.5, 0);
    glMatrixMode(GL_MODELVIEW); 
  }
  else
  {
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glTranslatef(_VitesseX, _VitesseY, 0);
    glRotatef(_VitesseRotation,0,0,1);
    glMatrixMode(GL_MODELVIEW); 
  }

  glBindTexture(GL_TEXTURE_2D,_Nom);
}

void MOGL_Texture::RepeterTexture(bool p_Val)
{
  _RepeterTexture=p_Val;
}

bool MOGL_Texture::ChargerBMP(const char * p_NomFichier)
{
  MOGL_Image::ChargerBMP(p_NomFichier);
  _Init();
  delete [] _Image;
  _Image=NULL;

  return true;
}


bool MOGL_Texture::ChargerPNG(const char * p_NomFichier)
{
  MOGL_Image::ChargerPNG(p_NomFichier);

  _Init();
  delete [] _Image;
  _Image=NULL;

  return true;
}

void MOGL_Texture::_Init()
{
  if (_Nom==0)
  {
    glGenTextures(1,&_Nom);
  }

  glBindTexture(GL_TEXTURE_2D,_Nom);
  if (_MipMap)
  {
    gluBuild2DMipmaps(GL_TEXTURE_2D, 4, _TailleX, _TailleY, GL_RGBA, GL_FLOAT, _Image);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
  }
  else
  {
    glTexImage2D(GL_TEXTURE_2D,0,4,_TailleX,_TailleY,0,GL_RGBA,GL_FLOAT,_Image);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  }

  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

  if (_RepeterTexture)
  {
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
  }
  else
  {
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
  }
}

MOGL_Texture::MOGL_Enum_TypeTexture MOGL_Texture::GetTypeTexture() const
{
  return _TypeTexture;
}

void MOGL_Texture::SetAnimation(bool p_Animer)
{
  _Animer=p_Animer;
}

bool MOGL_Texture::GetAnimation() const
{
  return _Animer;
}
        
void MOGL_Texture::SetVitesseTranslation(float p_VitesseX, float p_VitesseY)
{
  _VitesseX=p_VitesseX;
  _VitesseY=p_VitesseY;
}

void MOGL_Texture::SetVitesseRotation(float p_VitesseRotation)
{
  //_VitesseRotation=p_VitesseRotation*M_PI/180.0;
  _VitesseRotation=p_VitesseRotation;
}

void MOGL_Texture::_AnimerTexture()
{
  if (_Temps!=MOGL_Temps::GetTemps())
  {
    _Temps=MOGL_Temps::GetTemps();
    _TranslationX+=_VitesseX*MOGL_Temps::GetVariationTemps();
    _TranslationY+=_VitesseY*MOGL_Temps::GetVariationTemps();
    _Rotation+=_VitesseRotation*MOGL_Temps::GetVariationTemps();
  }
}

