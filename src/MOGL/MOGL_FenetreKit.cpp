/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_FenetreKit.h"
#include "MOGL_Temps.h"

MOGL_FenetreKit::MOGL_FenetreKit(char * p_Titre): MOGL_Action(), _Fenetre(p_Titre)
{
}

bool MOGL_FenetreKit::Initialiser()
{
  _Camera.SetClipping(1,1000);
  _Fenetre.SetMode(800,600,16);
  _Fenetre.SetFullscreen(false);
  if (!_Fenetre.Initialiser())
  {
    return false;
  }

  _Fenetre.DefinirRepetitionTouche(1,1);
  _Fenetre.SetGrabCurseur(true);

  _Fenetre.AjouterInterface(&_Facade);
  _Fenetre.AjouterInterface(&_Univers);

  _Camera.SetHaut(0,0,1);


  MOGL_Struct_Couleur coulTexte;
  coulTexte.r=0;
  coulTexte.g=0;
  coulTexte.b=1;
  coulTexte.a=0.5;

  MOGL_Struct_Couleur coulFond;
  coulFond.r=1;
  coulFond.g=1;
  coulFond.b=1;
  coulFond.a=0.5;

  _TexteFPS.SetCouleurTexte(coulTexte);
  _TexteFPS.SetCouleurFond(coulFond);
  _Univers.SetCamera(&_Camera);
  _Facade.AjouterElement(&_TexteFPS);


  _Fenetre.Idle.Connecter(this,&MOGL_FenetreKit::Idle);
  _Fenetre.KeyDown.Connecter(this,&MOGL_FenetreKit::Clavier);
  _Fenetre.MouseMove.Connecter(this,&MOGL_FenetreKit::DeplacementSouris);

  return true;
}

void MOGL_FenetreKit::Idle()
{
  char ch[20];
  sprintf(ch,"FPS : %.2f",MOGL_Temps::GetFPS());
  _TexteFPS.SetTexte(ch);
}

void MOGL_FenetreKit::Clavier(SDL_keysym * key)
{
  switch (key->sym)
  {
    case SDLK_F12 :
    {
      _Fenetre.ScreenshotPrefixBMP("mogl");
      break;
    }
    case SDLK_RETURN:
    {
      if ((key->mod&KMOD_LALT)!=0)
      {
        _Fenetre.SetFullscreen(!_Fenetre.GetFullscreen());
      }
      break;
    }
    case SDLK_UP :
    {
      _Camera.AvancerVitesse(20);
      break;
    }
    case SDLK_DOWN :
    {
      _Camera.ReculerVitesse(20);
      break;
    }
    case SDLK_LEFT :
    {           
      _Camera.GaucheVitesse(20);
      break;
    }
    case SDLK_RIGHT :
    {
      _Camera.DroiteVitesse(20);
      break;
    }
    case SDLK_ESCAPE:
    {
      _Fenetre.Arreter();
      break;
    }
    default:
      break;
  }
}

void MOGL_FenetreKit::DeplacementSouris(SDL_MouseMotionEvent * p_Event)
{
  int val=p_Event->xrel;
  
  if (val<0)
  {
        _Camera.TournerDroite(fabs(fmod(static_cast<float>(val),360))/7);
  }
  else
  {
        _Camera.TournerGauche(fmod(static_cast<float>(val),360)/7);
  }

  val=p_Event->yrel;
  
  if (val<0)
  {
        _Camera.TournerBas(fabs(fmod(static_cast<float>(val),360))/4);
  }
  else
  {
        _Camera.TournerHaut(fmod(static_cast<float>(val),360)/4);
  }
}

bool MOGL_FenetreKit::LancerBoucle()
{
  return _Fenetre.LancerBoucle();
}

MOGL_Fenetre * MOGL_FenetreKit::GetFenetre()
{
  return &_Fenetre;
}

MOGL_Univers * MOGL_FenetreKit::GetUnivers()
{
  return &_Univers;
}

MOGL_Facade * MOGL_FenetreKit::GetFacade()
{
  return &_Facade;
}

MOGL_Camera * MOGL_FenetreKit::GetCamera()
{
  return &_Camera;
}

MOGL_GestionnaireTexture * MOGL_FenetreKit::GetGestionnaireTexture()
{
  return &_GestionnaireTexture;
}
