/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_TrianglePeau.h"

MOGL_TrianglePeau::MOGL_TrianglePeau()
{
        for(int i=0; i<3; i++)
        {
                _ArmatureReferencePoint[i]=NULL;
                _IndiceReferencePoint[i]=0;
        }
}

void MOGL_TrianglePeau::Reinitialiser()
{
  for(int i=0; i<3; i++)
  {
        _ArmatureReferencePoint[i]=NULL;
        _IndiceReferencePoint[i]=0;
  }
}


MOGL_Texture * MOGL_TrianglePeau::GetTexture() const
{
        return _Texture;
}


void MOGL_TrianglePeau::SetTexture(MOGL_Texture * p_Texture)
{
        _Texture=p_Texture;
}

void MOGL_TrianglePeau::SetPoint(unsigned int p_IndicePoint, unsigned int p_IndiceReferenceInterne)
{
        _IndiceReferencePoint[p_IndicePoint]=p_IndiceReferenceInterne;
}

void MOGL_TrianglePeau::SetPoint(unsigned int p_IndicePoint, MOGL_Armature * p_Armature, unsigned int p_IndiceReferenceExterne)
{
        _IndiceReferencePoint[p_IndicePoint]=p_IndiceReferenceExterne;
        _ArmatureReferencePoint[p_IndicePoint]=p_Armature;
}

unsigned int MOGL_TrianglePeau::GetReferencePoint(unsigned int p_IndicePoint)
{
        return _IndiceReferencePoint[p_IndicePoint];
}

MOGL_Armature * MOGL_TrianglePeau::GetArmaturePoint(unsigned int p_IndicePoint)
{
        return _ArmatureReferencePoint[p_IndicePoint];
}

bool MOGL_TrianglePeau::EstPointInterne(unsigned int p_IndicePoint)
{
        return _ArmatureReferencePoint[p_IndicePoint]==NULL;
}

void MOGL_TrianglePeau::SetPointTexture(unsigned int p_Indice, MOGL_Struct_PointTexture p_PointTexture)
{
        _PointTexture[p_Indice]=p_PointTexture;
}

MOGL_Struct_PointTexture MOGL_TrianglePeau::GetPointTexture(unsigned int p_Indice)
{
        return _PointTexture[p_Indice];
}

