/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_IMAGEFACADE
#define MOGL_IMAGEFACADE

#include "MOGL_ElementFacade.h"
#include "MOGL_Image.h"

/**
   Repr�sente une image destin�e � �tre plac� dans un MOGL_Facade.
 */
class MOGL_ImageFacade: public MOGL_ElementFacade, public MOGL_Image
{
  protected:
    unsigned int _PositionX, _PositionY;
    bool _Initialise;
    bool _Texture;
    GLuint _Nom;
    float _RecX, _RecY;

  protected:
    bool _Puissance2(unsigned int p_Nombre, unsigned int & p_NombrePuissance);

  public:
    /**
       Constructeur
     */
    MOGL_ImageFacade(bool p_AutoriserTrou=false);

    /**
       Permet de d�finir la position de l'image
       @param p_PositionX : position X de l'image.
       @param p_PositionY : position Y de l'image.
     */
    void SetPosition(unsigned int p_PositionX, unsigned int p_PositionY);

    /**
       Permet de r�cup�rer la position X de l'image.
       @return Position X de l'image.
     */
    unsigned int GetPositionX() const;

    /**
       Permet de r�cup�rer la position Y de l'image.
       @return Position Y de l'image.
     */
    unsigned int GetPositionY() const;

    void Afficher();
    bool Recharger();
};

#endif
