/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Facade.h"
#ifdef _WIN32
  #include <windows.h>
  #include <windowsx.h>
#endif

#include <GL/glu.h>

MOGL_Facade::MOGL_Facade(): MOGL_Interface()
{
  _Niveau=50;
}

void MOGL_Facade::Afficher()
{
  //Initialize OpenGL
  glColor4f(1.0,1.0,1.0,1.0);
  glEnable(GL_BLEND);
  glDisable(GL_LIGHTING);
  glDisable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0,_Largeur,_Hauteur,0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  MOGL_ItListe_ElementFacade itElement;
  for(itElement=_ListeElementFacade.begin();itElement!=_ListeElementFacade.end();itElement++)
  {
    if (!(*itElement)->GetCacher())
    {
      (*itElement)->Afficher();
    }
  }

  glEnable(GL_LIGHTING);
}

void MOGL_Facade::AjouterElement(MOGL_ElementFacade * p_Element)
{
  _ListeElementFacade.push_back(p_Element);
}
