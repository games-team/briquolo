/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ELEMENTARBRE
#define MOGL_ELEMENTARBRE

#include "MOGL_MatriceTransformation.h"
#include "MOGL_Afficheur.h"

class MOGL_ElementArbre
{
  protected :
    /**
       Matrice de transformation locale � l'�l�ment de l'arbre.
     */
    MOGL_MatriceTransformation _Matrice;

    /**
       Indique que la matrice a �t� modifi�e. L'objet enfant qui utilise
       cet attribut doit le remettre � faux de sa propre initiative
    */
    bool _ModifieMatrice;

  public :
    /**
       Constructeur par d�faut.
    */
    MOGL_ElementArbre();

    /**
       Constructeur par copie.
    */
    MOGL_ElementArbre(const MOGL_ElementArbre & p_ElementArbre);

    /**
       Destructeur.
     */
    virtual ~MOGL_ElementArbre();

    /**
       M�thode de pr�paration de l'affichage. TnL() (<i>Transform and Lighting</i>) est appel�e,
       de fa�on � transf�rer les donn�es repr�sent�e par l'�l�ment de l'arbre � un MOGL_Afficheur.
       @param p_Afficheur : pointeur vers le MOGL_Afficheur destin�e � repr�senter les informations stock�es par l'�l�ment de l'arbre.
     */
    virtual void TnL(MOGL_Afficheur * p_Afficheur)=0;

    /**
       Permet de r�initialiser la matrice de transformation locale.
     */
    virtual void ReinitialiserPosition();

    /**
       Permet d'ajouter une transformation � l'�l�ment de l'arbre.
       @param p_Matrice : matrice de transformation � ajouter � la matrice de transformation locale
                          (cette matrice va �tre multipli�e � gauche de #_Matrice)
     */
    void AjouteTransformation(const MOGL_MatriceTransformation & p_Matrice);

    /**
       Permet d'ajouter une translation � l'�l�ment de l'arbre.
       @param p_X : valeur en X de la translation.
       @param p_Y : valeur en Y de la translation.
       @param p_Z : valeur en Z de la translation.
     */
    void AjouterTranslation(GLfloat p_X, GLfloat p_Y, GLfloat p_Z);
                
};

#endif
