/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_STRUCTURE
#define MOGL_STRUCTURE

#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
//#include "MOGL_ObjetAbstrait2.h"
class MOGL_ObjetAbstrait2;

struct MOGL_Struct_Point
{
    GLfloat xn,yn,zn;
    GLfloat xt,yt;
    GLfloat xp,yp,zp;
};

struct MOGL_Struct_Vecteur
{
    GLfloat x,y,z;
    MOGL_Struct_Vecteur ProduitVectoriel(const MOGL_Struct_Vecteur & p_Vecteur)
    {
      MOGL_Struct_Vecteur vec;
      vec.x=y*p_Vecteur.z - z*p_Vecteur.y;
      vec.y=z*p_Vecteur.x - x*p_Vecteur.z;
      vec.z=x*p_Vecteur.y - y*p_Vecteur.x;
      return vec;
    }
};

struct MOGL_Struct_VecteurHomogene
{
    GLfloat x,y,z,h;
};

/*struct MOGL_Struct_Couleur
{
        GLfloat r,g,b,a;
};*/

struct MOGL_Struct_Couleur
{
    GLfloat r,g,b,a;
};

/*struct MOGL_Struct_Couleur2
{
        GLfloat r,g,b;
};*/

struct MOGL_Struct_PointSimple
{
    GLfloat xp,yp,zp;
    GLfloat xn,yn,zn;
};

struct MOGL_Struct_PointTexture
{
    GLfloat xt,yt;
};

struct MOGL_Struct_ReferencePoint
{
    MOGL_ObjetAbstrait2 * objet;
    unsigned int indicePoint;
};





#endif
