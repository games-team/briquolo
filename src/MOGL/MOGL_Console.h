/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_CONSOLE
#define MOGL_CONSOLE

#include <list>
#include <string>
#include <iostream>

#include "MOGL_Police.h"
#include "MOGL_Interface.h"

using namespace std;

class MOGL_Console: public MOGL_Interface
{
    // **** Types locaux � la classe ****
    struct MOGL_Struct_Texte
    {
        string texte;
        unsigned int x;
        unsigned int y;
    };
        
    typedef list <MOGL_Struct_Texte> MOGL_List_Texte;
    typedef MOGL_List_Texte::iterator MOGL_ItList_Texte;
    // **********************************

  protected :
    void _AjouterChaine(char * p_Buffer, unsigned int p_Taille);

    MOGL_List_Texte _Chaines;
    const MOGL_Police * _Police;
    unsigned int _LargeurCarac, _HauteurCarac, _LargeurConsole, _HauteurConsole;
    unsigned int _PositionX, _PositionY;
                                
  public :
    MOGL_Console();
        
    void Afficher();
    void Ecrire(char * p_Buffer, unsigned int p_Taille);
        
};

#endif
