/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Univers.h"
#include <SDL/SDL.h>
#include <iostream>

MOGL_Univers::MOGL_Univers(): MOGL_Interface()
{
  _Niveau=20;
  _AfficheurFond.SetCamera(&_CameraFond);
  _AfficheurFond.ActiverLumiere(false);
}


void MOGL_Univers::Afficher()
{
  _NoeudFond.ReinitialiserPosition();
  _NoeudFond.AjouteTransformation(MOGL_MatriceTransformation::FabriqueEchelle(2,2,2));

  glColorMask(true, true, true, false);
  // Quelques pr�cisions pour la cam�ra
  _Camera->SetRaport(_Rapport);
  _CameraFond.SetRaport(_Rapport);
  _CameraFond=*_Camera;
  _CameraFond.SetPosition(0,0,0);

  _Afficheur.DebutTnL();
  _Noeud.TnL(&_Afficheur);      
  glLoadIdentity();
  glColor4f(1,1,1,1);
  glDepthRange(0,1);
  //glClear(GL_DEPTH_BUFFER_BIT);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glDepthRange(0,0.99);
  
  _Afficheur.AfficherOpaque();

  _AfficheurFond.DebutTnL();
  _NoeudFond.TnL(&_AfficheurFond);
  glLoadIdentity();
  glColor4f(1,1,1,1);
//  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glDepthRange(0.99,1);
  
  _AfficheurFond.AfficherOpaque();

  glDepthRange(0,0.99);
  _Afficheur.AfficherNonOpaque();
}

void MOGL_Univers::AjouterElement(MOGL_ElementArbre * p_Element)
{
  _Noeud.AjouterElement(p_Element);
}

bool MOGL_Univers::RetirerElement(MOGL_ElementArbre * p_Element)
{
  return _Noeud.RetirerElement(p_Element);
}

void MOGL_Univers::AjouterElementFond(MOGL_ElementArbre * p_Element)
{
  //_ListeObjetFond.push_back(p_ObjetFond);
  _NoeudFond.AjouterElement(p_Element);
}

void MOGL_Univers::SetCamera(MOGL_Camera * p_Camera)
{
  _Camera=p_Camera;
  
  _Afficheur.SetCamera(_Camera);
}

void MOGL_Univers::DessinerBord(bool p_Val)
{
  _Afficheur.DessinerBord(p_Val);
}

void MOGL_Univers::SetTailleBord(unsigned int p_Taille)
{
  _Afficheur.SetTailleBord(p_Taille);
}

void MOGL_Univers::SetDimension(unsigned int p_Largeur, unsigned int p_Hauteur)
{
  MOGL_Interface::SetDimension(p_Largeur, p_Hauteur);
  
  _Afficheur.SetDimension(p_Largeur, p_Hauteur);
  
}
