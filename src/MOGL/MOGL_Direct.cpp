/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Direct.h"
#include "MOGL_LensFlareSoleil.h"

MOGL_Direct::MOGL_Direct(): MOGL_Lumiere()
{
  _Direction.x=1;
  _Direction.y=1;
  _Direction.z=1;
  _Direction.h=0;  
}


void MOGL_Direct::TnL(MOGL_Afficheur * p_Afficheur)
{
  // **** R�glage de la lumi�re ****
  glPushMatrix();
  _Matrice.MultiplierDansOpenGL();
  _MatriceFinale.PrendreDeOpenGL();

  glLightfv(_NumLight,GL_DIFFUSE,(float*)&_Diffuse);
  glLightfv(_NumLight,GL_SPECULAR,(float*)&_Specular);
  glLightfv(_NumLight,GL_AMBIENT,(float*)&_Ambient);
  glLightfv(_NumLight,GL_POSITION,(GLfloat *)&_Direction);
  glEnable(_NumLight);

  MOGL_ItSet_ElementArbre it;
  for(it=_SetElement.begin();it!=_SetElement.end();it++)
  {
    (*it)->TnL(p_Afficheur);
  }
  glPopMatrix();

  if (_ProjeterOmbre)
  {
    MOGL_Struct_Vecteur vec;
    vec.x=-_Direction.x;
    vec.y=-_Direction.y;
    vec.z=-_Direction.z;
    vec=_MatriceFinale.EffectuerRotationVecteur(vec);

    p_Afficheur->AjouterLumiereOmbre(vec, true);
  }
}

void MOGL_Direct::SetDirection(const MOGL_Struct_Vecteur & p_Direction)
{
  float norm=sqrt(p_Direction.x*p_Direction.x+p_Direction.y*p_Direction.y+p_Direction.z*p_Direction.z);
  _Direction.x=-p_Direction.x/norm;
  _Direction.y=-p_Direction.y/norm;
  _Direction.z=-p_Direction.z/norm;
  _Direction.h=0;
}

const MOGL_Struct_VecteurHomogene & MOGL_Direct::GetDirection() const
{
  return _Direction;
}
