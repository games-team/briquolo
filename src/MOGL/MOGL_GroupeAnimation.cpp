/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_GroupeAnimation.h"

MOGL_GroupeAnimation::MOGL_GroupeAnimation()
{
}

void MOGL_GroupeAnimation::AjouterAnimation(MOGL_Animation * p_Animation)
{
  _SetAnimation.push_back(p_Animation);
}

/*void MOGL_GroupeAnimation::Initialise()
{
        MOGL_ItSet_Animation It;
        for(It=_SetAnimation.begin();It!=_SetAnimation.end();It++)
        {
                (*It)->Initialise();
        }
}*/

//void MOGL_GroupeAnimation::Initialise(unsigned int p_TempsInitial)
void MOGL_GroupeAnimation::Initialiser()
{
  MOGL_ItSet_Animation It;
  for(It=_SetAnimation.begin();It!=_SetAnimation.end();It++)
  {
//              (*It)->Initialise(p_TempsInitial);
        (*It)->Initialiser();
  }
}

void MOGL_GroupeAnimation::SetUniteTemps(float p_Temps)
{
  MOGL_ItSet_Animation It;
  for(It=_SetAnimation.begin();It!=_SetAnimation.end();It++)
  {
        (*It)->SetUniteTemps(p_Temps);
  }

}

//bool MOGL_GroupeAnimation::SetTemps(unsigned int p_Temps)
bool MOGL_GroupeAnimation::SetTemps()
{
  MOGL_ItSet_Animation It;
  bool PasFin=false;
  for(It=_SetAnimation.begin();It!=_SetAnimation.end();It++)
  {
        //PasFin=(*It)->SetTemps(p_Temps) || PasFin;
        PasFin=(*It)->SetTemps() || PasFin;
  }

  return PasFin;
}
