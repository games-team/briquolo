/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TEMPS
#define MOGL_TEMPS

#include <SDL/SDL.h>
#include "MOGL_Structure.h"

/**
   Classe g�rant le <i>temps</i>.
   Toutes les m�thodes d'acc�s sont static, donc aucune construction d'objet n'est necessaire.
*/
class MOGL_Temps
{
    friend class MOGL_Fenetre;

  private:
    MOGL_Temps();
        
  protected :
    static const unsigned int _TempsMajFPS;
    static void _MajTemps();
        
    static unsigned int _Temps, _TempsPause;
    static unsigned int _DernierTemps;
    static unsigned int _VariationTemps;
    static float _VariationTempsFloat;

    static unsigned int _TempsFPS;
    static float _NbImages;
    static float _FPS, _MoyenneFPS;
    static unsigned int _PoidsMoyenne;
    static bool _Pause;
    static float _MaxFPS;
        
  public:
    static float GetTemps();
    static float GetVariationTemps();
    static float GetVariationDistance(float p_Vitesse);
    static MOGL_Struct_Vecteur GetVariationVecteurDistance(const MOGL_Struct_Vecteur & p_VecteurVitesse);
    static float GetVariationAngle(float p_Vitesse);
    static MOGL_Struct_Vecteur GetVariationVecteurAngle(const MOGL_Struct_Vecteur & p_VecteurVitesse);
        
    static float GetFPS();
    static float GetMoyenneFPS();

    static bool GetPause();
    static void SetPause(bool p_Pause);

    static void SetMaxFPS(float p_NombreMax);
    static float GetMaxFPS();
};

#endif
