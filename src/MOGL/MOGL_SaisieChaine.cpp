/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "../I18n.h"
#include <iconv.h>
#include "MOGL_SaisieChaine.h"
#include <errno.h>
#include "config.h"

MOGL_SaisieChaine::MOGL_SaisieChaine(MOGL_Panneau * p_Panneau, const string & p_Label, const string & p_Chaine)
  : MOGL_ElementPanneau(p_Panneau, true), _Label(p_Label), _Chaine(p_Chaine), _SaisieFichier(true)
{
  _CouleurTexte.r=1;
  _CouleurTexte.g=1;
  _CouleurTexte.b=1;
  _CouleurTexte.a=1;

  // Since the conversion result is read as an Uint16 array, we must be
  // careful about endian dependency. Systems using GNU glibc seem to do
  // the right thing when using just "UCS-2", but systems using libiconv,
  // such as MinGW, will misbehave.
  //   -- Laurentiu Pancescu <lpancescu@fastmail.fm>
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
  _IconvDesc = iconv_open("UTF-8", "UCS-2LE");
#else
  _IconvDesc = iconv_open("UTF-8", "UCS-2BE");
#endif

  //_IconvDesc = iconv_open("UTF-8", "ISO_8859-1");
  if (_IconvDesc == (iconv_t)(-1))
  {
    cerr<<"Unicode descriptor conversion error"<<endl;
  }
}

MOGL_SaisieChaine::~MOGL_SaisieChaine()
{
  iconv_close(_IconvDesc);
}


void MOGL_SaisieChaine::Afficher()
{
  MOGL_ElementPanneau::Afficher();

  string texte(_Label);
  texte+=" : ";
  texte+=_Chaine;

  unsigned int taille=_Police->GetTailleXChaine(texte);

  if (_Focus)
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte, _CouleurFondFocus);
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glVertex2f(taille,0);
    glVertex2f(taille, _Police->GetTailleYCaractere('A'));
    glVertex2f(taille + _Police->GetTailleXMax(), _Police->GetTailleYMax());
    glVertex2f(taille + _Police->GetTailleXMax(), 0);
    glEnd();
  }
  else
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte);
  }
}

void MOGL_SaisieChaine::SetLabel(const string & p_Label)
{
  _Label=p_Label;
}

string MOGL_SaisieChaine::GetLabel() const
{
  return _Label;
}

void MOGL_SaisieChaine::ClavierDown(SDL_keysym * key)
{

    switch (key->sym)
    {
        case SDLK_LEFT :
        case SDLK_BACKSPACE :
        {
            if (_Chaine.size()>0)
            {
                _Chaine.resize(_Chaine.size()-1);
                ChangementChaine.Emettre(_Chaine);
            }
            break;
        }
        default:
        {
            break;
        }
    }  

    if (!_SaisieFichier)
    {
        _Chaine += ConvertToUTF8(key->unicode);
        ChangementChaine.Emettre(_Chaine);
    }
    else
    {
        string st = ConvertToUTF8(key->unicode);
        if (st != "/")
        {
            _Chaine += st;
            ChangementChaine.Emettre(_Chaine);
        }
    }
}

string MOGL_SaisieChaine::ConvertToUTF8(Uint16 charUCS2) const
{
  // Transformation ucs-2 -> utf-8
  if (iswprint(charUCS2))
  {

    // Alloc
    char * pUcs2Char = (char *)&charUCS2;
    char * pUtf8Char = new char[8];
    char * pUtf8CharInit = pUtf8Char;
    // prep
    size_t sizeSrc = 2;
    size_t sizeDest = 80;
    size_t size = sizeDest;
    // Conv
    iconv(_IconvDesc, (ICONV_CONST char **)&pUcs2Char, &sizeSrc, &pUtf8Char, &sizeDest);
    size = size - sizeDest;
    return string((char *) pUtf8CharInit, size);
  }
  else
  {
    return string();
  }
}


unsigned int MOGL_SaisieChaine::GetHauteurElement() const
{
  return _Police->GetTailleYMax();
}

unsigned int MOGL_SaisieChaine::GetLargeurElement() const
{
  string texte(_Label);
  if (_Label != "")
  {
    texte+=" : ";
  }
  texte+=_Chaine;

  return _Police->GetTailleXChaine(texte);
}

void MOGL_SaisieChaine::SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur)
{
  _CouleurTexte=p_Couleur;
}

MOGL_Struct_Couleur MOGL_SaisieChaine::GetCouleurLabel() const
{
  return _CouleurTexte;
}

string MOGL_SaisieChaine::GetChaine() const
{
  return _Chaine;
}

void MOGL_SaisieChaine::SetChaine(const string & p_Chaine)
{
  _Chaine=p_Chaine;
}

void MOGL_SaisieChaine::SetSaisieFichier(bool p_SaisieFichier)
{
  _SaisieFichier = p_SaisieFichier;
}
