/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_GestionnaireTexture.h"

void MOGL_GestionnaireTexture::AjouterTextureObjet(const string & p_NomTexture, MOGL_Texture * p_Texture)
{
        _MapTextureObjet[p_NomTexture]=p_Texture;
}

/*
void MOGL_GestionnaireTexture::AjouterTextureParticule(const string & p_NomTexture, MOGL_Texture * p_Texture)
{
        _MapTextureParticule[p_NomTexture]=p_Texture;
}
*/

MOGL_Texture * MOGL_GestionnaireTexture::GetTexture(const string & p_NomTexture) const
{
        return _MapTextureObjet.find(p_NomTexture)->second;

}

void MOGL_GestionnaireTexture::RechargerTextures()
{
  for(MOGL_ItMap_StringTexture it=_MapTextureObjet.begin(); it!= _MapTextureObjet.end(); it++)
  {
    it->second->Recharger();
  }
}
