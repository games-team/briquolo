/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_NOEUD
#define MOGL_NOEUD

#include <set>

#include "MOGL_ElementArbre.h"


using namespace std;

/**
   Permet de repr�senter un arbre de MOGL_ElementArbre.
   Cette classe est un MOGL_ElementArbre, et permet de stocker une liste de MOGL_ElementArbre.
   Elle permet ainsi de cr�er un arbre de MOGL_ElementArbre ayant chacun une matrice de transformation locale
   qui est ajout�e � la transformation d�finie par le MOGL_ElementArbre parent de l'arbre, de mani�re � cr�er
   une transformation absolue.
 */
class MOGL_Noeud : public MOGL_ElementArbre
{
  protected:
    // **** Types locaux � la classe ****
    typedef set <MOGL_ElementArbre *> MOGL_Set_ElementArbre;
    typedef MOGL_Set_ElementArbre::iterator MOGL_ItSet_ElementArbre;
    // **********************************

  public :
    /**
       Constructeur par d�faut.
     */
    MOGL_Noeud();
    
    /**
       Constructeur par copie. Le sous arbre de l'objet original n'est pas copi�.
     */
    MOGL_Noeud(const MOGL_Noeud & p_Noeud);

    /**
       Destructeur.
    */
    virtual ~MOGL_Noeud();

    /**
       Permet d'ajouter un MOGL_ElementArbre au sous arbre.
     */
    void AjouterElement(MOGL_ElementArbre * p_Element);

    /**
       Permet de retirer un MOGL_ElementArbre au sous arbre.
       @return <i>true</i> si le MOGL_ElementArbre a bien �t� trouv� et retir�, <i>false</i> sinon.
     */
    bool RetirerElement(MOGL_ElementArbre * p_Element);

    /**
       M�thode de pr�paration de l'affichage. TnL() (<i>Transform and Lighting</i>) est appel�e,
       de fa�on � transf�rer les donn�es repr�sent�e par le noeud de l'arbre � un MOGL_Afficheur, ainsi qu'� tous les enfant de ce noeud.
       Ainsi la m�thode MOGL_ElementArbre::TnL() des MOGL_ElementArbre de la liste est appel�e en ayant pr�alablement ajout�
       la transformation courante � OpenGL (MOGL_MatriceTransformation::MultiplierDansOpenGL()
       @param p_Afficheur : pointeur vers le MOGL_Afficheur destin�e � repr�senter les informations stock�es par le noeud de l'arbre.
     */
    virtual void TnL(MOGL_Afficheur * p_Afficheur);

  protected :
    /**
       Liste des MOGL_ElementArbre du sous arbre.
     */
    MOGL_Set_ElementArbre _SetElement;

};

#endif
