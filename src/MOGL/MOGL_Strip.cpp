/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Strip.h"

void MOGL_Strip::AjouterPoint(const MOGL_Struct_Point & p_Point)
{
  _ListePoint.push_back(p_Point);
}

MOGL_Texture * MOGL_Strip::GetTexture() const
{
  return _Texture;
}

void MOGL_Strip::SetTexture(MOGL_Texture * p_Texture)
{
  _Texture=p_Texture;
}

const MOGL_Struct_Point & MOGL_Strip::GetFirstPoint() const
{
  MOGL_ItListe_Point * it =const_cast<MOGL_ItListe_Point*>(&_ItListePoint);
  
  *it=_ListePoint.begin();
  return *_ItListePoint;
}

const MOGL_Struct_Point & MOGL_Strip::GetNextPoint() const
{
  MOGL_ItListe_Point * it =const_cast<MOGL_ItListe_Point*>(&_ItListePoint);
  (it)++;
  return *_ItListePoint;        
}

int MOGL_Strip::NbPoint() const
{
  return _ListePoint.size();
}

