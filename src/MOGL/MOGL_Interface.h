/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_INTERFACE
#define MOGL_INTERFACE

#include "MOGL_Action.h"

class MOGL_Interface: public MOGL_Action
{
  protected:
    /**
       Correspond au rapport <i>largeur/hauteur</i> de l'affichage.
    */
    float _Rapport;

    /**
       Correspond � la largeur et la hauteur de la fen�tre
    */
    unsigned int _Largeur, _Hauteur;

    /**
       Correspond au niveau de l'interface.
       Les interfaces sont affich�e dans l'ordre de leur niveau.
     */
    unsigned int _Niveau;

    /**
       Indique si l'interface est cach�e ou non.
     */
    bool _Cacher;
        
  public:
    /**
       Constructeur
     */
    MOGL_Interface();

    /**
       Permet de d�finir les dimension de la fen�tre.
    */
    virtual void SetDimension(unsigned int p_Largeur, unsigned int p_Hauteur);

    /**
       M�thode d'affichage. Cette m�thode est abstraite.
    */
    virtual void Afficher()=0;

    /**
       Permet de r�cup�rer le niveau d'affichage de l'interface.
       Ce niveau est indique l'ordre d'affichage des diff�rents type d'interface.
       Plus le niveau est bas, plus l'interface est affich�e en premier.
       Les classes sp�cialisant MOGL_Interface doivent dont sp�cifier MOGL_Interface::_Niveau
     */
    virtual unsigned int GetNiveau() const;

    /**
       Indique si l'interface est cach�e.
       @return <i>true</i> si l'interface est cach�e, <i>false</i> sinon.
     */
    virtual bool GetCacher() const;

    /**
       Permet de d�finir si l'interface doit �tre cach�e ou non.
       @param p_Cacher : <i>true</i> pour cacher l'interface, <i>false</i> sinon.
     */
    virtual void SetCacher(bool p_Cacher);

    /**
       Destructeur
    */
    virtual ~MOGL_Interface();
        
        
};

#endif
