/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Temps.h"
#include <cmath>
#include <iostream>

const unsigned int MOGL_Temps::_TempsMajFPS=250;

unsigned int MOGL_Temps::_Temps=0;
unsigned int MOGL_Temps::_TempsPause=0;
unsigned int MOGL_Temps::_DernierTemps=MOGL_Temps::_Temps;
unsigned int MOGL_Temps::_VariationTemps=0;
float MOGL_Temps::_VariationTempsFloat=0;
float MOGL_Temps::_FPS=0;
float MOGL_Temps::_MoyenneFPS=0;
unsigned int MOGL_Temps::_PoidsMoyenne=0;
unsigned int MOGL_Temps::_TempsFPS=0;
float MOGL_Temps::_NbImages=0;
bool MOGL_Temps::_Pause=false;
float MOGL_Temps::_MaxFPS=0;


void MOGL_Temps::_MajTemps()
{
  if (_Pause)
  {
    _DernierTemps=_TempsPause;
    _TempsPause=SDL_GetTicks();
    unsigned int variation=_TempsPause-_DernierTemps;

    if (_MaxFPS>0 && variation<(1000/_MaxFPS))
    {
      SDL_Delay(static_cast<unsigned int>(1000/_MaxFPS - variation));
      _TempsPause=SDL_GetTicks();
    }

    _VariationTemps=0;
    _VariationTempsFloat=0;
    //Maj FPS
    _TempsFPS+=_TempsPause-_DernierTemps;

  }
  else
  {
    _DernierTemps=_Temps;
    _Temps=SDL_GetTicks();
    _VariationTemps=_Temps-_DernierTemps;

    if (_MaxFPS>0 && _VariationTemps<(1000/_MaxFPS))
    {
      SDL_Delay(static_cast<unsigned int>(1000/_MaxFPS) - _VariationTemps);
      _Temps=SDL_GetTicks();
      _VariationTemps=_Temps-_DernierTemps;
    }

    _VariationTempsFloat=_VariationTemps/1000.0f;
    //Maj FPS
    _TempsFPS+=_VariationTemps;

  }

  _NbImages++;
  
  if (_TempsFPS>_TempsMajFPS)
  {
    _FPS=(_NbImages*1000)/_TempsFPS;
    _MoyenneFPS=(_MoyenneFPS*_PoidsMoyenne+_FPS) / (_PoidsMoyenne+1);
    _PoidsMoyenne++;

    _TempsFPS=0;
    _NbImages=0;
  }
}


float MOGL_Temps::GetTemps()
{
  return MOGL_Temps::_Temps/1000.0f;
}

float MOGL_Temps::GetVariationTemps()
{
  return MOGL_Temps::_VariationTempsFloat;
  
}

float MOGL_Temps::GetVariationDistance(float p_Vitesse)
{
  return p_Vitesse*MOGL_Temps::_VariationTempsFloat;
}

MOGL_Struct_Vecteur MOGL_Temps::GetVariationVecteurDistance(const MOGL_Struct_Vecteur & p_VecteurVitesse)
{
  MOGL_Struct_Vecteur vec;
  vec.x=p_VecteurVitesse.x*MOGL_Temps::_VariationTempsFloat;
  vec.y=p_VecteurVitesse.y*MOGL_Temps::_VariationTempsFloat;
  vec.z=p_VecteurVitesse.z*MOGL_Temps::_VariationTempsFloat;
  
  return vec;
}

float MOGL_Temps::GetVariationAngle(float p_Vitesse)
{
  return static_cast<float>(fmod(p_Vitesse*MOGL_Temps::_VariationTempsFloat,360));
}

MOGL_Struct_Vecteur MOGL_Temps::GetVariationVecteurAngle(const MOGL_Struct_Vecteur & p_VecteurVitesse)
{
  
  MOGL_Struct_Vecteur vec;
  vec.x=static_cast<float>(fmod(p_VecteurVitesse.x*MOGL_Temps::_VariationTempsFloat,360));
  vec.y=static_cast<float>(fmod(p_VecteurVitesse.y*MOGL_Temps::_VariationTempsFloat,360));
  vec.z=static_cast<float>(fmod(p_VecteurVitesse.z*MOGL_Temps::_VariationTempsFloat,360));
  
  return vec;
}

float MOGL_Temps::GetFPS()
{
  return MOGL_Temps::_FPS;
}

float MOGL_Temps::GetMoyenneFPS()
{
  return MOGL_Temps::_MoyenneFPS;
}

bool MOGL_Temps::GetPause()
{
  return _Pause;
}

void MOGL_Temps::SetPause(bool p_Pause)
{
  if (_Pause!=p_Pause)
  {
    _Pause=p_Pause;
    if (p_Pause)
    {
      //_TempsPause=_Temps;
      _TempsPause=SDL_GetTicks();
    }
    else
    {
      //_Temps=_TempsPause;
      _Temps=SDL_GetTicks();
    }
  }
}


void MOGL_Temps::SetMaxFPS(float p_NombreMax)
{
  _MaxFPS=p_NombreMax;
}

float MOGL_Temps::GetMaxFPS()
{
  return _MaxFPS;
}
