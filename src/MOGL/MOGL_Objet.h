/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_OBJET
#define MOGL_OBJET

#include <list>
#include <map>

#include "MOGL_Noeud.h"
#include "MOGL_Triangle.h"
#include "MOGL_Strip.h"
#include "MOGL_Structure.h"
#include "MOGL_Constante.h"

using namespace std;

class MOGL_Objet: public MOGL_Noeud
{
    // **** Types locaux � la classe ****
  protected:
    typedef list <MOGL_Triangle> MOGL_Liste_Triangle;
    typedef MOGL_Liste_Triangle::iterator MOGL_ItListe_Triangle;

    typedef list <MOGL_Strip> MOGL_Liste_Strip;
    typedef MOGL_Liste_Strip::iterator MOGL_ItListe_Strip;

    struct MOGL_Struct_TableauTexture
    {
        int IdentifiantAbonnement;
        GLfloat * TabPoint;
        GLfloat * TabNormale;
        GLfloat * TabTexture;
        MOGL_Texture * Texture;
    };

    typedef list <MOGL_Struct_TableauTexture> MOGL_Liste_TableauxTriangle;
    typedef MOGL_Liste_TableauxTriangle::iterator MOGL_ItListe_TableauxTriangle;

    typedef list <MOGL_Struct_TableauTexture> MOGL_Liste_TableauxStrip;
    typedef MOGL_Liste_TableauxStrip::iterator MOGL_ItListe_TableauxStrip;

    // **********************************

  protected :

    void _TnLAbonnement(MOGL_Afficheur * p_Afficheur);

    void _TnLOmbre(MOGL_Afficheur * p_Afficheur);

    MOGL_Liste_Triangle _ListeTriangle;
    MOGL_Liste_Strip _ListeStrip;

    bool _ModifieTriangle, _ModifieStrip;

    MOGL_Liste_TableauxTriangle _TabTriangle;
    MOGL_Liste_TableauxStrip _TabStrip;
                
    MOGL_MatriceTransformation _MatriceFinale;

//    unsigned int _AbonnementTrianglesUnique, _AbonnementStripsUnique;   

    bool _ProjeterOmbre;
    MOGL_Afficheur * _Afficheur;
    bool _DessinerBord;
    MOGL_Struct_Couleur _CouleurBord;
    unsigned int _TailleBord;

  public :
    MOGL_Objet();
    MOGL_Objet(const MOGL_Objet & p_Objet);
    ~MOGL_Objet();

    void AjouterTriangle(const MOGL_Struct_Point & p_Point1, 
                         const MOGL_Struct_Point & p_Point2,
                         const MOGL_Struct_Point & p_Point,
                         MOGL_Texture * p_Texture3);
    void AjouterTriangle(const MOGL_Triangle & p_Triangle);
    void AjouterStrip(const MOGL_Strip & p_Strip);

    void TnL(MOGL_Afficheur * p_Afficheur);

    void ProjeterOmbre(bool p_Val);

    void SetDessinerBord(bool p_Dessiner);
    bool GetDessinerBord() const;

    void SetCouleurBord(MOGL_Struct_Couleur p_Couleur);
    MOGL_Struct_Couleur GetCouleurBord() const;

    void SetTailleBord(unsigned int p_TailleBord);
    unsigned int GetTailleBord() const;
};

#endif
