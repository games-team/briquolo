/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TEMPORISATION
#define MOGL_TEMPORISATION

#include "MOGL_ElementArbre.h"
#include "MOGL_Signal.h"

/**
   Permet de d�finir une temporisation.
   La temporisation est destin�e � �tre ins�rer dans l'arbre de repr�entation de MOGL, c'est pourquoi
   MOGL_Temporisation sp�cialise MOGL_ElementArbre.
   Une fois la temporisation �coul�e, le signal FinTemporisation est emis.
   La temporisation est mise en pause lorsque MOGL_Temps est mis en pause (cf MOGL_Temps::SetPause()).
   La temporisation de s'�coule que lorsque l'arbre de repr�sentation parcourt l'objet (c'est � dire lorsque TnL() est appel�.)
 */
class MOGL_Temporisation: public MOGL_ElementArbre
{
  protected:
    /**
       Dur�e de la temporisation (en secondes).
     */
    float _DureeTempo;

    /**
       Dur�e �coul�e.
     */
    float _TempsEcoule;

    /**
       Indique si la temporisation doit �tre r�arm�e apr�s �coulement.
     */
    bool _Rearmer;

    /**
       Indique si la temporisation est en cours d'�coulement.
     */
    bool _EnCours;

  public: // SIGNAUX
    MOGL_Signal0 FinTemporisation;

  public:
    /**
       Permet de construire une temporisation.
       @param p_DureeTemporisation : dur�e de la temporisation, en seconde.
       @param p_Rearmer : indique si la temporisation doit �tre r�arm�e apr�s �coulement.
       @param p_Demarrer : indique si la temporisation doit �tre d�marr�e automatiquement d�s la cr�ation.
     */
    MOGL_Temporisation(float p_DureeTemporisation=1, bool p_Rearmer=false, bool p_Demarrer=true);

    /**
       Permet de r�cup�rer la dur�e de la temporisation.
       @return Dur�e de la temporisation, en secondes.
     */
    float GetDureeTemporisation() const;

    /**
       Permet de d�finir la dur�e de la temporisation.
       @param p_DureeTemporisation : dur�e de la temporisation, en secondes.
     */
    void SetDureeTemporisation(float p_DureeTemporisation);

    /**
       Permet de savoir si la temporisation est r�arm�e apr�s �coulement.
       @return <i>true</i> si la temporisation est r�arm�e, <i>false</i> sinon.
     */
    bool GetRearmage() const;

    /**
       Permet de d�finir si la temporisation doit �tre r�arm�e apr�s �coulement.
       @param p_Rearmer : <i>true</i> si la temporisation doit �tre r�arm�e, <i>false</i> sinon.
     */
    void SetRearmage(bool p_Rearmer);

    /**
       Permet de d�marrer la temporisation. Si la temporisation est d�j� d�marr�e, la m�thode n'a aucun effet.
     */
    void Demarrer();

    /**
       Permet d'arr�ter la temporisation. Si la temporisation est d�j� arr�t�, la m�thode n'a aucun effet.
     */
    void Arreter();

    void TnL(MOGL_Afficheur * p_Afficheur);
};

#endif
