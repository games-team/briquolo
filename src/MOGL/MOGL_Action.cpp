/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Action.h"
#include "MOGL_Signal.h"

MOGL_Action::MOGL_Action()
{
}

MOGL_Action::MOGL_Action(const MOGL_Action & p_Action) 
{
  for(MOGL_List_SignalBase::const_iterator it=p_Action._ListSignalBase.begin(); it!=p_Action._ListSignalBase.end(); it++)
  {
        (*it)->DupliquerConnexion(&p_Action, this);
        _ListSignalBase.push_back(*it);
  }
}

MOGL_Action & MOGL_Action::operator = (const MOGL_Action & p_Action)
{
  for(MOGL_List_SignalBase::const_iterator it=p_Action._ListSignalBase.begin(); it!=p_Action._ListSignalBase.end(); it++)
  {
        (*it)->DupliquerConnexion(&p_Action, this);
        _ListSignalBase.push_back(*it);
  }

  return *this;
}

MOGL_Action::~MOGL_Action()
{
  DeconnecterSignaux();
}


void MOGL_Action::AjouterSignal(MOGL_SignalBase * p_SignalBase)
{
  _ListSignalBase.push_back(p_SignalBase);
}

void MOGL_Action::RetirerSignal(MOGL_SignalBase * p_SignalBase)
{
  MOGL_List_SignalBase::iterator it=_ListSignalBase.begin();
  while(it!=_ListSignalBase.end())
  {
        if ((*it)==p_SignalBase)
        {
          MOGL_List_SignalBase::iterator it2=it;
          it++;
          _ListSignalBase.erase(it2);
        }
        else
        {
          it++;
        }
  }
}


void MOGL_Action::DeconnecterSignaux()
{
  for(MOGL_List_SignalBase::iterator it=_ListSignalBase.begin(); it!=_ListSignalBase.end(); it++)
  {
        (*it)->RetirerAction(this);
  }
}

