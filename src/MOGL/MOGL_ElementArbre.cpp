/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_ElementArbre.h"

MOGL_ElementArbre::MOGL_ElementArbre() : _ModifieMatrice(true)
{
}

MOGL_ElementArbre::MOGL_ElementArbre(const MOGL_ElementArbre & p_ElementArbre) :  _Matrice(p_ElementArbre._Matrice), _ModifieMatrice(true)
{
}

MOGL_ElementArbre::~MOGL_ElementArbre()
{
}

void MOGL_ElementArbre::ReinitialiserPosition()
{
        _Matrice.Reinitialiser();
        _ModifieMatrice=true;
}

void MOGL_ElementArbre::AjouterTranslation(GLfloat p_X, GLfloat p_Y, GLfloat p_Z)
{
        _Matrice.AjouterTranslation(p_X,p_Y,p_Z);
        _ModifieMatrice=true;
}

void MOGL_ElementArbre::AjouteTransformation(const MOGL_MatriceTransformation & p_Matrice)
{
        _Matrice.Multiplier(p_Matrice);
        _ModifieMatrice=true;
}
