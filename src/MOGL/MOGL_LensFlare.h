/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_LENSFLARE
#define MOGL_LENSFLARE

#ifdef _WIN32
        #include <windows.h>
        #include <windowsx.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "MOGL_ElementArbre.h"

/**
   Classe de base de gestion d'un lens flare.
   Elle propose un ensemble de m�thodes permettant de repr�senter les diff�rentes parties d'un lens flare.
   Un lens flare doit donc sp�cialiser cette classe en impl�mentant la m�thode _GenererLensFlare();
   Les attributs #_xPos, #_yPos, et #_zPos repr�sente la source du lens flare, et #_Intensite repr�sente
   son intensit�. Ces valeurs doivent �tre utilis�s pour g�n�rer le lens flare.
 */
class MOGL_LensFlare: public MOGL_ElementArbre
{
  protected:
        /**
           M�thode appel�e pour g�n�rer le lens flare. Il s'agit de la m�thode principale d'impl�mentation d'un lens flare.
           Elle utilise les m�thodes _DessinnerRectangle() et _DessinnerRectangleTailleFixe().
         */
        virtual void _GenererLensFlare()=0;

        /**
           Permet de dessiner un carr� d'une texture d�termin�e. Un lens flare est form� d'une s�rie de carr�.
           @param p_Texture : texture � plaquer sur le carr�. Toute la texture est utilis�e.
           @param p_CentreX : coordonn�e x du centre du carr�.
           @param p_CentreY : coordonn�e y du centre du carre.
           @param p_CentreZ : coordonn�e z du centre du carre.
           @param p_Position : position de la partie du lens flare. Doit �tre compris entre 0 et 1. 1 correspond � l'origine du lens flare
                           (donc la source de lumi�re) et 0 correspond au centre de l'�cran. Les autres valeurs sont ditribu�es
                                                   lin�airement entres ces deux positions.
           @param p_Taille : taille du carre.
           @param p_Couleur : couleur.
         */
        void _DessinerCarre(MOGL_Texture * p_Texture, float p_CentreX, float p_CentreY, float p_CentreZ, 
                                                float p_Position, float p_Taille, const MOGL_Struct_Couleur & p_Couleur);

        /**
           Permet de dessiner un carr� d'une texture d�termin�e et de taille fixe. Un lens flare est form� d'une s�rie de carr�.
           Ici, la taille du carr� ne d�pend pas de ses coordonn�es.
           @param p_Texture : texture � plaquer sur le carr�. Toute la texture est utilis�e.
           @param p_CentreX : coordonn�e x du centre du carr�.
           @param p_CentreY : coordonn�e y du centre du carre.
           @param p_CentreZ : coordonn�e z du centre du carre.
           @param p_Position : position de la partie du lens flare. Doit �tre compris entre 0 et 1. 1 correspond � l'origine du lens flare
                           (donc la source de lumi�re) et 0 correspond au centre de l'�cran. Les autres valeurs sont ditribu�es
                                                   lin�airement entres ces deux positions.
           @param p_Taille : taille du carre en pourcentage de la largeur de la fen�tre (par exemple une valeur de 25 donne un carr�
                         dont la taille est le quart de la largeur de l'�cran).
           @param p_Couleur : couleur.
         */
        void _DessinerCarreTailleFixe(MOGL_Texture * p_Texture, float p_CentreX, float p_CentreY, float p_CentreZ, 
                                                                  float p_Position, float p_Taille, const MOGL_Struct_Couleur & p_Couleur);

        MOGL_MatriceTransformation _MatriceFinale;
        /**
           Position de la source de lumi�re � l'origine du lens flare.
         */
        float _xPos, _yPos, _zPos;

        /**
           Intensit� du lens flare. Il s'agit en fait du cosinus de l'angle form� par la direction
           de la cam�ra et la direction de la source de lumi�re par rapport � la camera.
         */
        float _Intensite;

        /**
           Afficheur utilis� pour afficher le lens flare.
           Normalement, il ne devrait pas �tre utilis� dans une utilisation normale.
         */
        MOGL_Afficheur * _Afficheur;
        
        
  public:
        /**
           Constructeur par d�faut
         */
        MOGL_LensFlare();

        void TnL(MOGL_Afficheur * p_Afficheur);
};

#endif
