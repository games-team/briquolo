/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_ChoixNombre.h"

MOGL_ChoixNombre::MOGL_ChoixNombre(MOGL_Panneau * p_Panneau, const string & p_Label, float p_NombreInit, float p_Variation)
  : MOGL_ElementPanneau(p_Panneau, true), _Label(p_Label), _Nombre(p_NombreInit), _Variation(p_Variation), _Min(0), _Max(100)
{
  _CouleurTexte.r=1;
  _CouleurTexte.g=1;
  _CouleurTexte.b=1;
  _CouleurTexte.a=1;
}

void MOGL_ChoixNombre::Afficher()
{
  MOGL_ElementPanneau::Afficher();

  string texte(_Label);
  texte+=" : ";
  char ch[40];
  sprintf(ch,"%.2f",_Nombre);
  texte+=ch;

  if (_Focus)
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte, _CouleurFondFocus);
  }
  else
  {
    _Police->Afficher(texte, 0, 0, _CouleurTexte);
  }
}

void MOGL_ChoixNombre::SetLabel(const string & p_Label)
{
  _Label=p_Label;
}

string MOGL_ChoixNombre::GetLabel() const
{
  return _Label;
}

void MOGL_ChoixNombre::ClavierUp(SDL_keysym * key)
{
  switch (key->sym)
  {
    case SDLK_LEFT :
    {
      _Nombre-=_Variation;
      if (_Nombre < _Min)
      {
        _Nombre = _Min;
      }
      ChangementNombre.Emettre(_Nombre);

      break;
    }
    case SDLK_RIGHT :
    {
      _Nombre+=_Variation;
      if (_Nombre > _Max)
      {
        _Nombre = _Max;
      }
      ChangementNombre.Emettre(_Nombre);
      break;
    }
    default:
    {
      break;
    }
  }  
}

unsigned int MOGL_ChoixNombre::GetHauteurElement() const
{
  return _Police->GetTailleYMax();
}

unsigned int MOGL_ChoixNombre::GetLargeurElement() const
{

  string texte(_Label);
  texte+=" : ";
  char ch[40];
  sprintf(ch,"%.2f",_Nombre);
  texte+=ch;
  return _Police->GetTailleXChaine(texte);
}

void MOGL_ChoixNombre::SetCouleurLabel(const MOGL_Struct_Couleur & p_Couleur)
{
  _CouleurTexte=p_Couleur;
}

MOGL_Struct_Couleur MOGL_ChoixNombre::GetCouleurLabel() const
{
  return _CouleurTexte;
}

float MOGL_ChoixNombre::GetNombre() const
{
  return _Nombre;
}

void MOGL_ChoixNombre::SetNombre(float p_Nombre)
{
  _Nombre=p_Nombre;
}


float MOGL_ChoixNombre::GetVariation() const
{
  return _Variation;
}

void MOGL_ChoixNombre::SetVariation(float p_Variation)
{
  _Variation=p_Variation;
}

void MOGL_ChoixNombre::SetMin(float p_Min)
{
  _Min = p_Min;
}

void MOGL_ChoixNombre::SetMax(float p_Max)
{
  _Max = p_Max;
}

float MOGL_ChoixNombre::GetMin() const
{
  return _Min;
}

float MOGL_ChoixNombre::GetMax() const
{
  return _Max;
}
