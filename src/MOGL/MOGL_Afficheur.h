/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_AFFICHEUR
#define MOGL_AFFICHEUR

#include <map>
#include <list>
#include <set>
#include "MOGL_MatriceTransformation.h"
#include "MOGL_Camera.h"
#include "MOGL_Texture.h"
#include "MOGL_Triangle.h"
#include "MOGL_TriangleOmbre.h"
#include "MOGL_Strip.h"

using namespace std;

struct MOGL_Struct_Triangle2D
{
        float x[2],y[2];
        float r,g,b,a;
        MOGL_Texture * texture;
};

class MOGL_NoeudOpenGL;

/**
   Re�oit les polygones ayant subit la phase de <i>TnL</i> et destin� � les aficher pendant la phase effective d'<i>affichage</i>.
   Cette classe g�re aussi des abonnements de certaines listes de polygones qui sont li�s � un pointeur vers une matrice de transformation
   et qui sont destin�s � �tre afficher chaque fois (d'o� l'abonnement...).
*/
class MOGL_Afficheur
{
    // **** Types locaux � la classe ****
        
    struct MOGL_Struct_TableauTextureObjet
    {
        int NbPoint;
        bool * DessinerBord;
        MOGL_Struct_Couleur * CouleurBord;
        unsigned int * TailleBord;
        MOGL_MatriceTransformation * Matrice;
        GLfloat * TabPoint;
        GLfloat * TabNormale;
        GLfloat * TabTexture;
    };

    struct MOGL_Struct_LumiereOmbre
    {
        bool VecteurEstDirection;
        MOGL_Struct_Vecteur Vecteur;
    };
        
    typedef map <int, MOGL_Struct_TableauTextureObjet> MOGL_Map_TableauxTriangle;
    typedef MOGL_Map_TableauxTriangle::iterator MOGL_ItMap_TableauxTriangle;

    typedef map <MOGL_Texture *, MOGL_Map_TableauxTriangle> MOGL_Map_TextureTriangles;
    typedef MOGL_Map_TextureTriangles::iterator MOGL_ItMap_TextureTriangles;

    typedef map <int, MOGL_Struct_TableauTextureObjet> MOGL_Map_TableauxStrip;
    typedef MOGL_Map_TableauxStrip::iterator MOGL_ItMap_TableauxStrip;

    typedef map <MOGL_Texture *, MOGL_Map_TableauxStrip> MOGL_Map_TextureStrips;
    typedef MOGL_Map_TextureStrips::iterator MOGL_ItMap_TextureStrips;

    typedef list<MOGL_Struct_Vecteur> MOGL_List_Vecteur;
    typedef MOGL_List_Vecteur::iterator MOGL_ItList_Vecteur;

    typedef list <MOGL_Triangle> MOGL_Liste_Triangle;
    typedef MOGL_Liste_Triangle::const_iterator MOGL_ItListe_Triangle;

    typedef list<MOGL_TriangleOmbre> MOGL_List_TriangleOmbre;
    typedef MOGL_List_TriangleOmbre::iterator MOGL_ItList_TriangleOmbre;

    typedef list <MOGL_Strip> MOGL_Liste_Strip;
    typedef MOGL_Liste_Strip::const_iterator MOGL_ItListe_Strip;

    typedef list <MOGL_Struct_LumiereOmbre> MOGL_Liste_LumiereOmbre;
    typedef MOGL_Liste_LumiereOmbre::iterator MOGL_ItListe_LumiereOmbre;

    typedef map<unsigned int, MOGL_MatriceTransformation *> MOGL_Map_EntierMatrice;
    typedef MOGL_Map_EntierMatrice::iterator MOGL_ItMap_EntierMatrice;

    struct MOGL_Struct_Image
    {
        MOGL_Struct_Image(MOGL_Texture * p_Texture, float p_X, float p_Y, float p_Z,
                          float p_TailleX, float p_TailleY, const MOGL_Struct_Couleur & p_Couleur):
          texture(p_Texture), x(p_X), y(p_Y), z(p_Z), tailleX(p_TailleX), tailleY(p_TailleY), couleur(p_Couleur)
        {
        }

        MOGL_Texture * texture;
        float x,y,z;
        float tailleX, tailleY;
        MOGL_Struct_Couleur couleur;
    };

    struct MOGL_Struct_Flare
    {
        MOGL_Struct_Flare(MOGL_Texture * p_Texture, float p_Position, float p_X, float p_Y, float p_Z,
                          float p_Taille, const MOGL_Struct_Couleur & p_Couleur):
          texture(p_Texture), position(p_Position), x(p_X), y(p_Y), z(p_Z), taille(p_Taille), couleur(p_Couleur)
        {
        }

        MOGL_Texture * texture;
        float position;
        float x,y,z;
        float taille;
        MOGL_Struct_Couleur couleur;
    };

    typedef multimap <float, MOGL_Struct_Image> MOGL_Multimap_ProfondeurImage;
    typedef MOGL_Multimap_ProfondeurImage::iterator MOGL_ItMultimap_ProfondeurImage;

    typedef list<MOGL_Struct_Image> MOGL_Liste_Image;
    typedef MOGL_Liste_Image::iterator MOGL_ItListe_Image;
        
    typedef list<MOGL_Struct_Flare> MOGL_Liste_Flare;
    typedef MOGL_Liste_Flare::iterator MOGL_ItListe_Flare;

    typedef set<MOGL_NoeudOpenGL *> MOGL_Set_NoeudOpenGL;
    typedef MOGL_Set_NoeudOpenGL::iterator MOGL_ItSet_NoeudOpenGL;
    // **********************************

  protected:
    void _AfficherAbonnementsOpaque(bool p_AvecNormalTexture);
    void _AfficherBordAbonnementsOpaque();
    void _AfficherAbonnementsNonOpaque(bool p_AvecNormalTexture);
    //void _AfficherParticules();
    void _AfficherOmbre();
    void _AfficherImageOpaque();
    void _AfficherImageNonOpaque();
    void _AfficherLensFlare();

    void _GetProjectionPoint(const MOGL_Struct_Vecteur & p_Point, const MOGL_Struct_Vecteur & p_Direction, float * p_PointResultat);
    void _FabriqueQuadrilatere(float * p_TabQuadri, const MOGL_Struct_Vecteur & p_Vecteur, bool p_VecteurEstDirection,
    const MOGL_Struct_Vecteur & p_Point1, const MOGL_Struct_Vecteur & p_Point2);
        
    int _DernierIdentifiantObjet;
    MOGL_Map_TextureTriangles _MapTabTriangle;
    MOGL_Map_TextureStrips _MapTabStrip;
        
    MOGL_List_Vecteur _ListPoint;
        
    MOGL_Camera * _Camera;
    MOGL_MatriceTransformation _MatriceCamera, _MatriceInvCamera;

    MOGL_List_TriangleOmbre _ListeTriangleOmbre;
    MOGL_Liste_LumiereOmbre _ListeLumiereOmbre;

    MOGL_Multimap_ProfondeurImage _MultimapImage;
    MOGL_Multimap_ProfondeurImage _MultimapImageTailleFixe;
    MOGL_Liste_Image _ListeImage;
    MOGL_Liste_Image _ListeImageTailleFixe;

    MOGL_Liste_Flare _ListeFlare;
    MOGL_Liste_Flare _ListeFlareTailleFixe;

    MOGL_Set_NoeudOpenGL _SetNoeudOpenGL;

    bool _DessinerBord;
    unsigned int _TailleBord;
        
    unsigned int _Largeur, _Hauteur;
        
    float _ptX, _ptY, _ptZ;
    bool _ActiverLumiere;

    float _zMin, _zMax;
        
  public:
    /**
       Constructeur.
    */
    MOGL_Afficheur();

    /**
       Destrcueteur.
    */
    ~MOGL_Afficheur();
        
    /**
       Provoque la remise � z�ro des structures temproraires d'affichage. Doit �tre appel� avant chaque <i>TnL</i>
    */
    void DebutTnL();

    /**
       Permet d'abonner une liste de triangles associ�s � une m�me texture.
       @param p_Texture : pointeur vers la texture.
       @param p_Matrice : pointeur vers la matrice de transformation li� � cet ensemble de triangle.
       Si <i>null</i>, alors les points fournis sont d�j� transform�s
       @param p_NbPoint : nombre de points (qui correspondent � <i>p_NbPoint</i>/3).
       @param p_TabPoints : tableau des points des triangles (de taille 3*p_NbPoint)
       @param p_TabNormale : tableau des normales aux points des triangles (de taille 3*p_NbPoint)
       @param p_TabTexture : tableau des coordonn�es de texture des points (de taille 2*p_NbPoint)
    */
    int AbonnerTriangle(MOGL_Texture * p_Texture, MOGL_MatriceTransformation * p_Matrice,int p_NbPoint, bool * p_DessinerBord,
                        MOGL_Struct_Couleur * p_CouleurBord, unsigned int * p_TailleBord,
                        GLfloat * p_TabPoints, GLfloat * p_TabNormale, GLfloat * p_TabTexture);

    /**
       Permet d'abonner un liste de triangles associ�s � une m�me texture qui est sous la forme d'un <i>strip</i>.
       @param p_Texture : pointeur vers la texture.
       @param p_Matrice : pointeur vers la matrice de transformation li� � cet ensemble de triangle.
       Si <i>null</i>, alors les points fournis sont d�j� transform�s
       @param p_NbPoint : nombre de points (qui correspondent � <i>p_NbPoint</i>-2).
       @param p_TabPoints : tableau des points des triangles (de taille 3*p_NbPoint)
       @param p_TabNormale : tableau des normales aux points des triangles (de taille 3*p_NbPoint)
       @param p_TabTexture : tableau des coordonn�es de texture des points (de taille 2*p_NbPoint)
    */
    int AbonnerStrip(MOGL_Texture * p_Texture, MOGL_MatriceTransformation * p_Matrice,int p_NbPoint, bool * p_DessinerBord,
                     MOGL_Struct_Couleur * p_CouleurBord, unsigned int * p_TailleBord,
                     GLfloat * p_TabPoints, GLfloat * p_TabNormale, GLfloat * p_TabTexture);

    /**
       D�sabonne une liste de triangle pr�alablement abonn� avec
       AbonnerTriangle(MOGL_Texture *, MOGL_MatriceTransformation *,int, GLfloat *,     GLfloat *, GLfloat *).
       @param p_Texture : texture de l'abonnement.
       @param p_Identifiant : num�ro d'identifiant de l'abonnement.
    */
    bool DesabonnerTriangle(MOGL_Texture * p_Texture, int p_Identifiant);

    /**
       D�sabonne une liste de triangle, qui est sous la forme d'un strip, pr�alablement abonn� avec
       AbonnerStrip(MOGL_Texture *, MOGL_MatriceTransformation *,int, GLfloat *, GLfloat *, GLfloat *).
       @param p_Texture : texture de l'abonnement.
       @param p_Identifiant : num�ro d'identifiant de l'abonnement.
    */
    bool DesabonnerStrip(MOGL_Texture * p_Texture, int p_Identifiant);

    /**
       Permet d'ajouter des triangles � la liste des triangles projecteur d'ombre.
    */
    void AjouterTriangleOmbre(const MOGL_List_TriangleOmbre & p_ListeTriangle);

    /**
       Permet d'ajouter des triangles � la liste des triangles projecteur d'ombre.
    */
    void AjouterTriangleOmbre(const MOGL_Liste_Triangle & p_ListeTriangle);

    /**
       Permet d'ajouter un triangle projecteur d'ombre
    */
    void AjouterTriangleOmbre(const MOGL_TriangleOmbre & p_Triangle);

    /**
       Permet d'ajouter une source de lumi�re qui projete une ombre.
       @param p_Vecteur : voir <i>p_VecteurEstDirection</i>
       @param p_VecteurEstDirection : <i>false</i> indique que le vecteur indique la position de la lumi�re
       (pour les lumi�res omnidirectionnelles), <i>true</i> indique que le vecteur est la direction de la
       lumi�re (pour les lumi�res directionnelles.
    */
    void AjouterLumiereOmbre(const MOGL_Struct_Vecteur & p_Vecteur, bool p_VecteurEstDirection);
        
    /**
       Envoie � OpenGL tous les triangles � afficher qui sont opaques. D'abord les abonnements, puis les particules.
    */
    void AfficherOpaque();

    /**
       Envoie � OpenGL tous les triangles � afficher qui ne sont pas opaques. D'abord les abonnements, puis les particules.
    */
    void AfficherNonOpaque();

    /**
       D�finit la cam�ra
       @param p_Camera : la cam�ra
    */
    void SetCamera(MOGL_Camera * p_Camera);

    /**
       Permet de r�cup�rer la cam�ra courante associ�e � l'afficheur
       @return Pointeur vers la cam�ra.
    */
    MOGL_Camera * GetCamera() const;
        
    /**
       Permet de r�cup�rer la matrice de transformation correspondant � la cam�ra
       return R�f�rence vers la matrice de transformation. C'est une constante !
    */
    const MOGL_MatriceTransformation & GetMatriceCamera() const;

    /**
       Permet de r�cup�rer l'inverse de la matrice de transformation correspondant � la cam�ra
       return R�f�rence vers la matrice de transformation. C'est une constante !
    */
    const MOGL_MatriceTransformation & GetMatriceInvCamera() const;

    /**
       Permet d'indiquer s'il faut dessiner les bords des objets. Le bord correspond au "d�tourage"
       style <i>bande d�ssin�e</i>
       @param p_Val : true -> d�ssiner les bords, false -> ne pas d�ssiner les bords
    */
    void DessinerBord(bool p_Val);

    /**
       Permet de d�finir la taille en pixels du bord.
       @param p_Taille : taille en pixels
       @sa DessinerBord()
    */
    void SetTailleBord(unsigned int p_Taille);

    /**
       Permet de d�finir les dimension de la fen�tre.
    */
    void SetDimension(unsigned int p_Largeur, unsigned int p_Hauteur);
    /**
       Permet de r�cup�rer les dimensiosn de la fen�tre
    */
    void GetDimension(unsigned int & p_Largeur, unsigned int & p_Hauteur);

    /**
       Permet d'activer ou d�sactiver la gestion des lumi�res
    */
    void ActiverLumiere(bool p_Activation);

    /**
       Permet d'ajouter un point.
    */
    void AjouterPoint(MOGL_Struct_Vecteur p_Point);

    /**
       Permet d'ajouter une image. Aussi connu sous le nom de <i>billboard</i>. L'image va �tre
       affich�e de mani�re � �tre toujours face � la cam�ra. Sa taille va varier selon sa distance par rapport
       � la cam�ra.
       @param p_Texture : pointeur vers le MOGL_Texture correspondant � l'image.
       @param p_X : position en X du centre de l'image.
       @param p_Y : position en Y du centre de l'image.
       @param p_Z : position en Z du centre de l'image.
       @param p_TailleX : largeur de l'image.
       @param p_TailleY : hauteur de l'image.
       @param p_Couleur : couleur.
       @sa AjouterImageTailleFixe
    */
    void AjouterImage(MOGL_Texture * p_Texture, float p_X, float p_Y, float p_Z,
                      float p_TailleX, float p_TailleY, const MOGL_Struct_Couleur & p_Couleur);

    /**
       Permet d'ajouter une image dont la taille ne varie pas selon la distance.
       Aussi connu sous le nom de <i>billboard</i>. L'image va �tre
       affich�e de mani�re � �tre toujours face � la cam�ra.
       � la cam�ra.
       @param p_Texture : pointeur vers le MOGL_Texture correspondant � l'image.
       @param p_X : position en X du centre de l'image.
       @param p_Y : position en Y du centre de l'image.
       @param p_Z : position en Z du centre de l'image.
       @param p_TailleX : largeur de l'image (pourcentage de la largeur de la fen�tre, par exemple 25 correpond au quart de la largeur).
       @param p_TailleY : hauteur de l'image (pourcentage de la hauteur de la fen�tre, par exemple 25 correpond au quart de la hauteur).
       Si la valeur est n�gative alors la hauteur de l'image sera �gale � la largeur.
       @param p_Couleur : couleur.
       @sa AjouterImage
    */
    void AjouterImageTailleFixe(MOGL_Texture * p_Texture, float p_X, float p_Y, float p_Z,
                                float p_TailleX, float p_TailleY, const MOGL_Struct_Couleur & p_Couleur);

    /**
       Permet d'ajouter une partie d'une lens flare.
       Il n'est affich� que si l'origine n'est pas masqu�.
       @param p_Texture : texture � utiliser.
       @param p_Position : position de la partie du lens flare. Doit �tre compris entre 0 et 1. 1 correspond � l'origine du lens flare
       (donc la source de lumi�re) et 0 correspond au centre de l'�cran. Les autres valeurs sont ditribu�es
       lin�airement entres ces deux positions.
       @param p_OrigineX : coordonn�e X de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_OrigineY : coordonn�e Y de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_OrigineZ : coordonn�e Z de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_Taille : taille.
       @param p_Couleur : couleur � utiliser lors du plaquage de texture.
    */
    void AjouterPartieLensFlare(MOGL_Texture * p_Texture, float p_Position,
                                float p_OrigineX, float p_OrigineY, float p_OrigineZ,
                                float p_Taille, const MOGL_Struct_Couleur & p_Couleur);
        
    /**
       Permet d'ajouter une partie d'une lens flare dont la taille ne varie pas selon la distance.
       Il n'est affich� que si l'origine n'est pas masqu�.
       @param p_Texture : texture � utiliser.
       @param p_Position : position de la partie du lens flare. Doit �tre compris entre 0 et 1. 1 correspond � l'origine du lens flare
       (donc la source de lumi�re) et 0 correspond au centre de l'�cran. Les autres valeurs sont ditribu�es
       lin�airement entres ces deux positions.
       @param p_OrigineX : coordonn�e X de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_OrigineY : coordonn�e Y de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_OrigineZ : coordonn�e Z de l'origine du lens flare (donc la source de lumi�re correspondante).
       @param p_Taille : taille (pourcentage de la largeur de la fen�tre, ainsi une valeur de 25 correspond au
       quart de la largeur de la fen�tre).
       @param p_Couleur : couleur � utiliser lors du plaquage de texture.
    */
    void AjouterPartieLensFlareTailleFixe(MOGL_Texture * p_Texture, float p_Position,
                                          float p_OrigineX, float p_OrigineY, float p_OrigineZ,
                                          float p_Taille, const MOGL_Struct_Couleur & p_Couleur);
        
    /**
       Permet d'ajouter un MOGL_NoeudOpenGL.
       @param p_NoeudOpenGL : noeud � ajouter.
     */
    void AjouterNoeudOpenGL(MOGL_NoeudOpenGL * p_NoeudOpenGL);

    /**
       Permet de retirer un MOGL_NoeudOpenGL.
       @param p_NoeudOpenGL : noeud � retirer.
       @return <i>true</i> si le MOGL_NoeudOpenGL a bien �t� trouv� et retir�, <i>false</i> sinon.
     */
    bool RetirerNoeudOpenGL(MOGL_NoeudOpenGL * p_NoeudOpenGL);

};


#endif
