/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_SIGNALBASE
#define MOGL_SIGNALBASE

#include "MOGL_Action.h"
#include <list>

class MOGL_ConnexionBaseBase
{
  protected:
    /**
       Pointeur vers MOGL_Action associ� � la connexion.
    */
    MOGL_Action * _Objet;
  public:
    /**
       Permet de r�cup�rer le MOGL_Action associ�.
       @return Le MOGL_Action associ�.
    */
    MOGL_Action * GetObjet()
    {
      return _Objet;
    }

    /**
       Permet de dupliquer une connexion et de red�finir le MOGL_Action asoci�.
       @param p_NouvelObjet : nouveau MOGL_Action associ�.
    */
    virtual MOGL_ConnexionBaseBase * Dupliquer(MOGL_Action * p_NouvelObjet)=0;
    virtual ~MOGL_ConnexionBaseBase() {}
};

class MOGL_SignalBase
{
    friend void MOGL_Action::DeconnecterSignaux();
    friend MOGL_Action::MOGL_Action(const MOGL_Action &);
    friend MOGL_Action & MOGL_Action::operator = (const MOGL_Action &);

  protected:
    // **** Types locaux � la classe ****
    /**
       Liste de pointeurs vers MOGL_ConnexionBaseBase.
    */
    typedef list <MOGL_ConnexionBaseBase *> MOGL_List_Connexion;
    // **********************************

  protected:
    /**
       Liste des connexions associ�es.
    */
    MOGL_List_Connexion _ListConnexion;

  protected:
    /**
       Permet de retirer de la liste des connexions toutes celles dont le MOGL_Action correspond au param�tre indiqu�.
       Cette m�thode est destin�e � �tre appel�e lorsque le MOGL_Action correspondant est d�truit.
       @param p_Action : pointeur vers le MOGL_Action qu'il faut retirer.
    */
    void RetirerAction(MOGL_Action * p_Action)
    {
      MOGL_List_Connexion::iterator it=_ListConnexion.begin();
      while (it!=_ListConnexion.end())
      {
        if ((*it)->GetObjet()==p_Action)
        {
          MOGL_List_Connexion::iterator it2=it;
          it++;
                  
          delete *it2;
          _ListConnexion.erase(it2);
        }
        else
        {
          it++;
        }
      }
    }

    /**
       Permet de dupliquer toutes les connexions associ�es � un MOGL_Action et de les asscocier � un autre MOGL_Action.
       Cette m�thode est destin�e � �tre appel�e par le constructeur par copie et par l'op�rateur d'affectation de MOGL_Action.
       @param p_ActionOrigine : MOGL_Action � dupliquer
       @param p_ActionNouvelle : nouveau MOGL_Action associ�.
    */
    void DupliquerConnexion(const MOGL_Action * p_ActionOrigine, MOGL_Action * p_ActionNouvelle)
    {
      for(MOGL_List_Connexion::const_iterator it=_ListConnexion.begin(); it!=_ListConnexion.end(); it++)
      {
        if ((*it)->GetObjet()==p_ActionOrigine)
        {
          _ListConnexion.push_back((*it)->Dupliquer(p_ActionNouvelle));
        }
      }     
    }

  public:
    /**
       Constructeur par d�faut.
    */
    MOGL_SignalBase()
    {
    }
        
    /**
       Constructeur par copie.
    */
    MOGL_SignalBase(const MOGL_SignalBase & p_SignalBase)
    {
      for(MOGL_List_Connexion::const_iterator it=p_SignalBase._ListConnexion.begin(); it!=p_SignalBase._ListConnexion.end(); it++)
      {
        (*it)->GetObjet()->AjouterSignal(this);
        _ListConnexion.push_back((*it)->Dupliquer((*it)->GetObjet()));
      }
    }

    /**
       Red�finition de l'op�rateur =.
    */
    MOGL_SignalBase & operator = (const MOGL_SignalBase & p_SignalBase)
    {
      for(MOGL_List_Connexion::const_iterator it=p_SignalBase._ListConnexion.begin(); it!=p_SignalBase._ListConnexion.end(); it++)
      {
        (*it)->GetObjet()->AjouterSignal(this);
        _ListConnexion.push_back((*it)->Dupliquer((*it)->GetObjet()));
      }
      return * this;
    }

    /**
       Destructeur.
    */
    virtual ~MOGL_SignalBase()
    {
      DeconnecterTout();
    }       

    /**
       Permet de d�connecter tous les MOGL_Action connect�s au signal.
    */
    void DeconnecterTout()
    {
      for(MOGL_List_Connexion::iterator it=_ListConnexion.begin(); it!=_ListConnexion.end(); it++)
      {
        (*it)->GetObjet()->RetirerSignal(this);
        delete *it;
      }
      _ListConnexion.clear();
    }
        
    /**
       Permet de d�connecter un MOGL_Action.
       @param p_Action : pointeur vers une instance d'une classe sp�cialisant un MOGL_Action qu'il faut d�connecter.
    */
    void Deconnecter(MOGL_Action * p_Action)
    {
      MOGL_List_Connexion::iterator it=_ListConnexion.begin();
      while (it!=_ListConnexion.end())
      {
        if ((*it)->GetObjet()==p_Action)
        {
          MOGL_List_Connexion::iterator it2=it;
          it++;
                  
          (*it2)->GetObjet()->RetirerSignal(this);
                  
          delete *it2;
          _ListConnexion.erase(it2);
        }
        else
        {
          it++;
        }
      }
    }
};

#endif
