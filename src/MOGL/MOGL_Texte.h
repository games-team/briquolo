/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_TEXTE
#define MOGL_TEXTE

#include <string>
#include "MOGL_Police.h"
#include "MOGL_ElementFacade.h"

using namespace std;

class MOGL_Texte: public MOGL_ElementFacade
{
  public:
    MOGL_Texte();
    MOGL_Texte(char * p_Chaine, int p_X=0, int p_Y=0, const MOGL_Police * p_Police=NULL);
    MOGL_Texte(const string & p_Chaine, int p_X=0, int p_Y=0, const MOGL_Police * p_Police=NULL);
        
    void SetTexte(char * p_Chaine);
    void SetTexte(const string & p_Chaine);

    void SetCouleurTexte(const MOGL_Struct_Couleur & p_CouleurTexte);
    const MOGL_Struct_Couleur & GetCouleurTexte();

    void SetCouleurFond(const MOGL_Struct_Couleur & p_CouleurFond);
    const MOGL_Struct_Couleur & GetCouleurFond();

    void SetPosition(int p_X, int p_Y);
    void SetPolice(MOGL_Police * p_Police);

    unsigned int GetTailleX() const;
    unsigned int GetTailleY() const;
                
    const string & GetString();
        
    void Afficher();

  protected:
    string _Chaine;
    int _X, _Y;
    const MOGL_Police * _Police;
    MOGL_Struct_Couleur _CouleurTexte;
    MOGL_Struct_Couleur _CouleurFond;
};

#endif
