/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_TriangleOmbre.h"

MOGL_TriangleOmbre::MOGL_TriangleOmbre(const MOGL_Struct_Vecteur & p_Point1,
                                                                           const MOGL_Struct_Vecteur & p_Point2,
                                                                           const MOGL_Struct_Vecteur & p_Point3)
  : _Point1(p_Point1), _Point2(p_Point2), _Point3(p_Point3)
{
  MOGL_Struct_Vecteur v1, v2;

  v1.x=_Point2.x-_Point1.x;
  v1.y=_Point2.y-_Point1.y;
  v1.z=_Point2.z-_Point1.z;
  
  v2.x=_Point3.x-_Point1.x;
  v2.y=_Point3.y-_Point1.y;
  v2.z=_Point3.z-_Point1.z;

  _Normale.x=v1.y*v2.z - v2.y*v1.z;
  _Normale.y=v1.z*v2.x - v2.z*v1.x;
  _Normale.z=v1.x*v2.y - v2.x*v1.y;
  
}

const MOGL_Struct_Vecteur & MOGL_TriangleOmbre::GetPoint(int NumPoint) const
{
  switch (NumPoint)
  {
        case 0 :
        {
          return _Point1;
        }
        break;
        case 1 :
        {
          return _Point2;
        }
        break;
        default :
        {
          return _Point3;
        }
        break;
  }
}

const MOGL_Struct_Vecteur & MOGL_TriangleOmbre::GetNormale() const
{
  return _Normale;
}
