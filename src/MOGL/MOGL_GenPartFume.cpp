/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_GenPartFume.h"
#include "MOGL_Temps.h"

MOGL_GenPartFume::MOGL_GenPartFume(unsigned int p_NbParticulesMax,
                                                                   bool p_Recreer,
                                                                   MOGL_Texture * p_Texture) :MOGL_GenerateurParticule(p_NbParticulesMax, p_Recreer, p_Texture)
{
}

void MOGL_GenPartFume::CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                                                                  MOGL_Struct_Vecteur & Velocite,
                                                                                  MOGL_Struct_Couleur & Couleur,
                                                                                  GLfloat & DureeVie,
                                                                                  GLfloat & Taille)
{
  Position.x=6*RandomNeg();
  Position.y=6*RandomNeg();
  Position.z=6*RandomNeg();

  Velocite.x=6*RandomNeg();
  Velocite.y=6*RandomNeg();
  Velocite.z=65;

  Couleur.r=0.5;
  Couleur.g=0.5;
  Couleur.b=0.5;

  DureeVie=2*Random();
  Taille=20;
}

void MOGL_GenPartFume::MajParticule(MOGL_Struct_Vecteur & p_Position,
                                                                        MOGL_Struct_Vecteur & p_Velocite,
                                                                        MOGL_Struct_Couleur & p_Couleur)
{
  MOGL_Struct_Vecteur vec=MOGL_Temps::GetVariationVecteurDistance(p_Velocite);
  
  p_Position.x+=vec.x;
  p_Position.y+=vec.y;
  p_Position.z+=vec.z;

/*  p_Position.x+=p_Velocite.x*_Temps;
        p_Position.y+=p_Velocite.y*_Temps;
        p_Position.z+=p_Velocite.z*_Temps;*/
}


