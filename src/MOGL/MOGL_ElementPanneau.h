/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_ELEMENTPANNEAU
#define MOGL_ELEMENTPANNEAU

#include "MOGL_Police.h"
#include "MOGL_Fenetre.h"

class MOGL_Panneau;

class MOGL_ElementPanneau
{
    friend class MOGL_Panneau;
  protected:
    const MOGL_Police * _Police;
    bool _Focus, _GestionFocus;
    MOGL_Struct_Couleur _CouleurFondFocus;
    bool _Cacher;
    MOGL_Panneau * _Panneau;

  public:
    MOGL_ElementPanneau(MOGL_Panneau * p_Panneau, bool p_GestionFocus);
    virtual ~MOGL_ElementPanneau();
    virtual void Afficher();
    void SetPolice(MOGL_Police * p_Police);
    const MOGL_Police * GetPolice() const;
    virtual unsigned int GetHauteurElement() const=0;
    virtual unsigned int GetLargeurElement() const=0;

    virtual void ClavierUp(SDL_keysym * key);
    virtual void ClavierDown(SDL_keysym * key); 
    virtual void DeplacementSouris(SDL_MouseMotionEvent * p_Event);
    virtual void SetCouleurFondFocus(const MOGL_Struct_Couleur & p_Couleur);
    virtual MOGL_Struct_Couleur GetCouleurFondFocus() const;

    virtual bool GetCacher() const;
    virtual void SetCacher(bool p_Cacher);

};

#endif
