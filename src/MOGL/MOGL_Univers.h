/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef MOGL_UNIVERS
#define MOGL_UNIVERS

#include <list>

#include "MOGL_Noeud.h"
#include "MOGL_ElementArbre.h"
#include "MOGL_Camera.h"
#include "MOGL_Afficheur.h"
#include "MOGL_Interface.h"

using namespace std;

class MOGL_Univers: public MOGL_Interface
{
    // **** Types locaux � la classe ****
    // **********************************

  protected :
    MOGL_Noeud _Noeud, _NoeudFond;
    MOGL_Camera * _Camera;
    MOGL_Camera _CameraFond;
    MOGL_Afficheur _Afficheur, _AfficheurFond;
  public :
    MOGL_Univers();
        
    void Afficher();
    void AjouterElement(MOGL_ElementArbre * p_Element);
    bool RetirerElement(MOGL_ElementArbre * p_Element);
    void AjouterElementFond(MOGL_ElementArbre * p_Element);
    void SetCamera(MOGL_Camera * p_Camera);

    /**
       Permet d'indiquer s'il faut dessiner les bords des objets. Le bord correspond au "d�tourage"
       style <i>bande d�ssin�e</i>
       @param p_Val : true -> d�ssiner les bords, false -> ne pas d�ssiner les bords
    */
    void DessinerBord(bool p_Val);

    /**
       Permet de d�finir la taille en pixels du bord.
       @param p_Taille : taille en pixels
       @sa DessinerBord()
    */
    void SetTailleBord(unsigned int p_Taille);

    /**
       Permet de d�finir les dimension de la fen�tre.
       C'est une red�finition de la m�thode de la classe MOGL_Interface
    */
    void SetDimension(unsigned int p_Largeur, unsigned int p_Hauteur);

};

#endif
