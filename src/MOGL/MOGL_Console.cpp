/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "MOGL_Console.h"
#include "MOGL_Bibliotheque.h"
#include <GL/glu.h>

MOGL_Console::MOGL_Console(): _Police(NULL), _LargeurCarac(0), _HauteurCarac(0), _LargeurConsole(0), _HauteurConsole(0),
                              _PositionX(0), _PositionY(0)
{
}


void MOGL_Console::Afficher()
{
  //Initialize OpenGL
  glColor4f(1.0,1.0,1.0,1.0);
  glEnable(GL_BLEND);
  glDisable(GL_LIGHTING);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  //Initialize OpenGL projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0,_Largeur,_Hauteur,0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  if (!_Police)
  {
    _Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
    _LargeurCarac=_Police->GetTailleXMax();
    _HauteurCarac=_Police->GetTailleYMax();
    _LargeurConsole=_Largeur/_LargeurCarac;
    _HauteurConsole=_Hauteur/_HauteurCarac;
  }

  for(MOGL_ItList_Texte it=_Chaines.begin(); it!=_Chaines.end(); it++)
  {
    _Police->Afficher(it->texte, it->x*_LargeurCarac, it->y*_HauteurCarac);
  }

  glEnable(GL_LIGHTING);
}


void MOGL_Console::Ecrire(char * p_Buffer, unsigned int p_Taille)
{
        unsigned int debut=0;
        
        for(unsigned int i=0; i< p_Taille; i++)
        {
                unsigned char car=p_Buffer[i];
                if (car<32)
                {
                        switch (car)
                        {
              case 13:
              {
                      _PositionX=0;
                      //_PositionY++;
                      break;
              }
              case 10:
              {
                      _PositionX=0;
                      _PositionY++;
                      break;
              }
              case 27:
              {
                      _AjouterChaine("ESC", 3);
              }
                
                        }
                
                        if (i!=debut)
                        {
        _AjouterChaine(&p_Buffer[debut], i-debut);
                        }
                        debut=i+1;
                }
        }

        if (p_Buffer[p_Taille-1]>=32)
        {
                if (p_Taille-debut>0)
                {
                        _AjouterChaine(&p_Buffer[debut], p_Taille-debut);
                }
        }
}

void MOGL_Console::_AjouterChaine(char * p_Buffer, unsigned int p_Taille)
{
  if (!_Police)
  {
    _Police=&(MOGL_Bibliotheque::GetSingleton()->GetPolice());
    _LargeurCarac=_Police->GetTailleXMax();
    _HauteurCarac=_Police->GetTailleYMax();
    _LargeurConsole=_Largeur/_LargeurCarac;
    _HauteurConsole=_Hauteur/_HauteurCarac;
  }

  bool pas_fini=true;
  while (pas_fini)
  {
    if (_PositionX+p_Taille<=_LargeurConsole)
    {
      MOGL_Struct_Texte text;
      text.texte=string(p_Buffer, p_Taille);
      text.x=_PositionX;
      text.y=_PositionY;
      _Chaines.push_back(text);

      _PositionX+=p_Taille;
      pas_fini=false;
    }
    else
    {
      if (_PositionX<_LargeurConsole)
      {
        MOGL_Struct_Texte text;
        text.texte=string(p_Buffer, _LargeurConsole-_PositionX);
        text.x=_PositionX;
        text.y=_PositionY;
        _Chaines.push_back(text);
        p_Buffer+=_LargeurConsole-_PositionX;
        p_Taille-=_LargeurConsole-_PositionX;
        if (p_Taille==0)
        {
          pas_fini=false;
        }
      }
      _PositionX=0;
      _PositionY++;
          
    }
        
  }       

}
