/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "ExplosionBreche.h"
#include "MOGL_Temps.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif


ExplosionBreche::ExplosionBreche(BriqueBase * p_Brique)
: Generateur(100, false, Texture::GetGestionnaireTexture()->GetTexture("ParticuleBreche"))
{
  Point p=p_Brique->GetVisualCenter();
  _X=p.x;
  _Y=p.y;

  CreerParticules(100);
}

void ExplosionBreche::CreerParamInitiaux(MOGL_Struct_Vecteur & Position,
                                         MOGL_Struct_Vecteur & Velocite,
                                         MOGL_Struct_Couleur & Couleur,
                                         GLfloat & DureeVie,
                                         GLfloat & Taille)
{
  Position.x=_X;
  Position.y=_Y;
  Position.z=0;

  float angle=Random()*M_PI*2;
  float angle2=RandomNeg()*M_PI;

  Velocite.x=10*cos(angle)*cos(angle2);
  Velocite.y=6*sin(angle)*cos(angle2);
  Velocite.z=15*sin(angle2);

  Couleur.r=1;
  Couleur.g=1;
  Couleur.b=1;
  Couleur.a=1;

  DureeVie=0.3*Random();
  
  Taille=1.3*Random();
}

void ExplosionBreche::MajParticule(MOGL_Struct_Vecteur & p_Position,
                                   MOGL_Struct_Vecteur & p_Velocite,
                                   MOGL_Struct_Couleur & p_Couleur)
{
  MOGL_Struct_Vecteur vec=MOGL_Temps::GetVariationVecteurDistance(p_Velocite);
  
  p_Position.x+=vec.x;
  p_Position.y+=vec.y;
  p_Position.z+=vec.z;
}


