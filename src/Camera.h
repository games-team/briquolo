/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _CAMERA
#define _CAMERA

#include <MOGL_Camera.h>
#include <MOGL_Temporisation.h>
#include "Plateau.h"

class Camera: public MOGL_Action
{
    // **** Types locaux � la classe ****
    typedef enum{HAUT, HAUT2} Enum_TypeVue;
    // **********************************
  protected:   
    const unsigned int NB_VUE;

    Enum_TypeVue _VueCourante;
    MOGL_Camera _Camera;
    Plateau * _Plateau;
    bool _VueSubjective;
    MOGL_Temporisation _TempoVueSubjective;

    bool _AllerSubjectif, _RetourSubjectif;
    float _Fact;
    float _OffsetX, _OffsetY;

    void _VueHaut();
    void _VueHaut2();
    void _VuePlateau();

    void _OnFinVueSubjective();

  public:
    Camera(Plateau * plateau);
    void ChangerVue();
    void ChangerVue(Enum_TypeVue p_TypeVue);
   
    void Maj();

    void SetVueSubjective(bool p_Actif);
    bool GetVueSubjective() const;
    void SetOffset(float pOffsetX, float pOffsetY);

    MOGL_Camera * GetMOGLCamera();
};
#endif
