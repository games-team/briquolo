/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _CASSEBRIQUE
#define _CASSEBRIQUE

#include <MOGL_Objet.h>
#include <MOGL_Fenetre.h>
#include <MOGL_Facade.h>
#include <MOGL_Univers.h>
#include <MOGL_Action.h>
#include <MOGL_Texte.h>
#include <MOGL_Polygone.h>
#include <MOGL_ImageFacade.h>

#include "MenuBase.h"
#include "Brique.h"
#include "TableauEditeur.h"
#include "MenuPrincipal.h"

#define TAILLE 10

class CasseBrique: public MOGL_Action
{
  protected:
    MOGL_Fenetre _Fenetre;

    Brique * _Brique[TAILLE*TAILLE];

    TableauBase * _Tableau;
    TableauBase * _Editeur;
    MOGL_Facade _Facade;
    MOGL_Polygone * _RectangleFond;
    MOGL_Police * _PoliceVie;
    MOGL_Texte _TexteFPS, _TexteNombreVie;
    bool _Pause;
    MenuBase * _MenuBase;
    MenuPrincipal * _MenuPrincipal;
    bool _FinJeu;
    enum {MENU, JEU, EDITEUR} _EnumAction;

    float r[4], g[4], b[4];

    MOGL_ImageFacade * _ImagePlateau;
    bool _CreateOk;

  protected:
    //Signaux
    void _Idle();
    void _ClavierUp(SDL_keysym * key);
    void _ClavierDown(SDL_keysym * key);
    void _DeplacementSouris(SDL_MouseMotionEvent * p_Event);
    void _BoutonSourisUp(SDL_MouseButtonEvent * p_Event);
    void _BoutonSourisDown(SDL_MouseButtonEvent * p_Event);
    void _OnFinPause();
    void _OnCommencerJeu();
    void _OnEditer();
    void _OnQuitter();
    void _OnSauvegarder(const string & p_NomGroupe, const string & p_NomFichier);
    void _OnCharger(const string & p_Groupe, const string & p_NomFichier);
    void _OnFinTableau();
    void _OnFinBalle();
    void _OnDestruction();
    void _OnChangementResolution(int p_ResolutionX, int p_ResolutionY, int p_BitParPixel);

    bool _ChargerTableau(const string & p_Groupe, const string & p_Nom);
    bool _SauverTableau(const string & p_Groupe, const string & p_Nom);

  public:
    CasseBrique();
    ~CasseBrique();

    void main();
};

#endif
