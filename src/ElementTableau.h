/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _ELEMENTTABLEAU
#define _ELEMENTTABLEAU

#include <MOGL_ElementArbre.h>
#include "Generateur.h"
#include "ElementCollision.h"
#include "Balle.h"
#include "Son.h"

#include <string>

using namespace std;

class TableauBase;

class ElementTableau: public ElementCollision
{
  protected:
    unsigned int _Id; // Identifiant utilis� dans l'�diteur pour la sauvegarde afin de connaitre le num�ro identifiant de l'�l�ment
    bool _Decors; // Indique si c'est un �l�ment du d�cors, dans ce cas il n'a pas besoin d'�tre d�truit pour terminer le tableau
    TableauBase * _TableauBase;

  public:
    virtual Struct_Collision TesterCollision(Balle * p_Balle)=0;
    virtual Generateur * CreerGenerateurParticules()=0;

    virtual void Maj();
    virtual void InitialiserPosition();

    void SetId(unsigned int p_Id);
    unsigned int GetId() const;
    bool EstDecors() const;
    TableauBase * GetTableau() const;

    void SetSelectionner(bool p_Selectionner);

    ElementTableau(TableauBase * p_TableauBase);
    ~ElementTableau();
};





#endif
