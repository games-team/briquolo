/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#include "Brique.h"
#include "ExplosionBrique.h"
#include "Constante.h"
#include <MOGL_GestionnaireObjet.h>
#include <iostream>
#include "BibliothequeSon.h"

MOGL_Objet * Brique::_ObjetSource=NULL;

Brique::Brique(TableauBase * p_TableauBase): BriqueBase(p_TableauBase)
{
  if (_ObjetSource==NULL)
  {
    PreCharger();
  }
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

Brique::Brique(const Brique & p_Brique): BriqueBase(p_Brique)
{
  _ElementArbre=new MOGL_Objet(*_ObjetSource);
}

Brique::~Brique()
{
  delete _ElementArbre;
}

void Brique::PreCharger()
{
  _ObjetSource=new MOGL_Objet;
  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/brique.tri").c_str(),
                                                  *Texture::GetGestionnaireTexture(), *_ObjetSource);
//  MOGL_GestionnaireObjet::ChargerObjetASCTriangle((Constante::GetGlobalDir()+Constante::GetDataDir()+"/flip.tri").c_str(),
//                                                  *Texture::GetGestionnaireTexture(), *_ObjetSource);
}

Struct_Collision Brique::TesterCollision(Balle * p_Balle)
{
  Struct_Collision col=BriqueBase::TesterCollision(p_Balle);
  return col;
}

Generateur * Brique::CreerGenerateurParticules()
{
  return new ExplosionBrique(this);
}

