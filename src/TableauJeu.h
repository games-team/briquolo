/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _TABLEAUJEU
#define _TABLEAUJEU

#include <set>
#include "ElementTableau.h"
#include "Balle.h"
#include "Plateau.h"
#include "Bonus.h"
#include "Generateur.h"
#include "TableauBase.h"
#include <MOGL_Temporisation.h>

using namespace std;

class TableauJeu: public TableauBase
{
  // **** Types locaux � la classe ****
  public:
    typedef set<Balle *> Set_Balle;
    typedef set<Plateau *> Set_Plateau;
    typedef set<Bonus *> Set_Bonus;
    typedef set<Generateur *> Set_Generateur;
  // **********************************

  protected:
    Set_Balle _SetBalle;
    Set_Plateau _SetPlateau;
    Set_Bonus _SetBonus;
    Set_Generateur _SetGenerateur, _SetGenerateurADetruire; // Les g�n�rateur � d�truire
    Camera * _Camera;
    
    const float _ProbaBonus;

    Struct_Collision _CollisionBord(Balle * p_Balle);
    Bonus * _CreerBonus();
    MOGL_Temporisation *  _TemporisationFinTableau;
    MOGL_Temporisation *  _TemporisationDepartBalle;
    bool _FinTableau;
    bool _BallePartie;
    int _NombreVie;
    float _VibrationX, _VibrationY;
    float _CummulDep;

  protected: // ACTION
    void _OnFinTemporisation();
    void _OnDepartBalle();

  public: //SIGNAUX
    MOGL_Signal0 FinTableau;
    MOGL_Signal0 FinBalle;

  public:
    TableauJeu(MOGL_Fenetre * p_Fenetre);
    ~TableauJeu();
    
    void AjouterBalle(Balle * p_Balle);
    bool RetirerBalle(Balle * p_Balle);

    void AjouterPlateau(Plateau * p_Plateau);
    bool RetirerPlateau(Plateau * p_Plateau);

    void DeplacerPlateaux(float p_Distance);
    void Maj();

    void AjouterGenerateur(Generateur * p_Generateur);

    void CreerBonus(float p_X, float p_Y);

    void OnFinGenerateur(Generateur * p_Generateur);
    Camera * GetCamera();
    Set_ElementTableau * GetSetElementTableau();
    Set_Balle * GetSetBalle();
    Set_Plateau * GetSetPlateau();
    Set_Bonus * GetSetBonus();
    Set_Generateur * GetSetGenerateur();

    int GetNombreVie() const;
    void SetNombreVie(int p_NombreVie);

    void DepartTableau();

    void ClavierUp(SDL_keysym * key);
    void ClavierDown(SDL_keysym * key);
    void DeplacementSouris(SDL_MouseMotionEvent * p_Event);
    void BoutonSourisDown(SDL_MouseButtonEvent * p_Event);
    void SetVibration(float pVibrationX, float pVibrationY);
};
 
#endif
