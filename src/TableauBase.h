/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _TABLEAUBASE
#define _TABLEAUBASE

#include <MOGL_Univers.h>
#include <MOGL_Direct.h>
#include <MOGL_Action.h>
#include "Camera.h"
#include <MOGL_Fenetre.h>
#include "ElementTableau.h"
#include <vector>
#include <set>
#include <list>
#include <string>
#include "Texture.h"

using namespace std;

class TableauBase: public MOGL_Action
{
    // **** Types locaux � la classe ****
  public:
    typedef set<ElementTableau *> Set_ElementTableau;
    typedef vector<string> Vect_NomGroupe;
    // **********************************

  protected:
    Set_ElementTableau _SetElementTableau;
    MOGL_Fenetre * _Fenetre;
    MOGL_Univers * _Univers;
    MOGL_Direct _Lumiere;
    MOGL_Objet * _Fond;
    float _XMin, _XMax, _YMin, _YMax;
    MOGL_Objet * _TableauMOGL;
    bool _Activation;
    bool _Pause;
    string _NomTableau;
  protected:
    static Vect_NomGroupe _VectNomGroupe;

  public:
    TableauBase(MOGL_Fenetre * p_Fenetre);
    ~TableauBase();

    virtual void AjouterElementTableau(ElementTableau * p_ElementTableau);
    virtual bool RetirerElementTableau(ElementTableau * p_ElementTableau);
    virtual void EffacerTousElementTableau();

    virtual void Activer(bool p_Activation);
    virtual void ClavierUp(SDL_keysym * key);
    virtual void ClavierDown(SDL_keysym * key);
    virtual void DeplacementSouris(SDL_MouseMotionEvent * p_Event);
    virtual void BoutonSourisUp(SDL_MouseButtonEvent * p_Event);
    virtual void BoutonSourisDown(SDL_MouseButtonEvent * p_Event);
    virtual float GetXMin() const;
    virtual float GetXMax() const;
    virtual float GetYMin() const;
    virtual float GetYMax() const;
    virtual void SetPause(bool p_Pause);
    virtual bool GetPause() const;
    virtual void Maj();
    virtual bool Sauvegarder(const char * p_NomFichier);
    virtual bool Charger(const char * p_NomFichier);
    virtual string GetNom() const;
    virtual void SetNom(const string & p_Nom);

    static list<string> GetListeGroupeTableau();
    static set<string> GetListeTableau(const string & p_NomGroupe);
    static string GetGroupeTableauDesc(const string & base, const string & dir);
    static string GetRepGroupeFromId(unsigned int p_Id);
};

#endif
