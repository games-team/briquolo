/*****************************************************************************
 *
 *  Copyright (C) 2003 C�dric Br�gardis <cedric.bregardis@free.fr>
 *
 *  This file is part of BRIQUOLO
 *
 *  BRIQUOLO is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  BRIQUOLO is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BRIQUOLO; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *****************************************************************************/
#ifndef _BRIQUEPOLYGONE
#define _BRIQUEPOLYGONE

#include <MOGL_Objet.h>

#include <MOGL_GestionnaireTexture.h>
#include <list>
#include "ElementTableau.h"

using namespace std;

class BriquePolygone: public ElementTableau
{
  protected:
    // **** Types locaux � la classe ****
    typedef list<Point> List_Point;
    typedef List_Point::iterator ListIt_Point;
    // **********************************

  protected:
    List_Point _ListPoint;
    float _Z, _VitesseZ;
    float _Rotation;
    float _Rayon;
    float _VitesseRotation;

    void _SetCollisionCoin(Balle * p_Balle, double p_CentreX, double p_CentreY,
                           double p_T, double p_ResX, double p_ResY,
                           Struct_Collision & p_Collision);

    void _AnalyserLigne();

    void _AjouterPoint(float p_X, float p_Y);

  public:
    BriquePolygone(TableauBase * p_TableauBase);
    BriquePolygone(const BriquePolygone & p_BriquePolygone);
    ~BriquePolygone();

    void Maj();
    void InitialiserPosition();
    void SetPosition(double p_X, double p_Y);
    Point GetVisualCenter();

    Struct_Collision TesterCollision(Balle * p_Balle);
    bool Toucher(Balle * p_Balle, double p_Temps);

};

#endif
