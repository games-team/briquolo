# Microsoft Developer Studio Project File - Name="briquolo" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=briquolo - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "briquolo.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "briquolo.mak" CFG="briquolo - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "briquolo - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "briquolo - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "briquolo - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "../src/MOGL/" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib SDLmain.lib SDL.lib SDL_mixer.lib opengl32.lib glu32.lib libpng.a libz.a gnu_gettext.lib /nologo /subsystem:console /machine:I386 /force

!ELSEIF  "$(CFG)" == "briquolo - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "../src/MOGL/" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 SDL_mixer.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib SDLmain.lib SDL.lib opengl32.lib glu32.lib libpng.a libz.a gnu_gettext.lib /nologo /subsystem:console /debug /machine:I386 /force /pdbtype:sept

!ENDIF 

# Begin Target

# Name "briquolo - Win32 Release"
# Name "briquolo - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\src\Balle.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BibliothequeSon.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Bonus.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusDoubleBalle.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusFast.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusGros.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusGrosPlateau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusPetit.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusPetitPlateau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusSlow.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusSuperPuissance.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BonusVueSubjective.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Brique.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BriqueBase.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BriqueDeuxCoups.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BriqueIncassable.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BriquePetite.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Camera.cpp
# End Source File
# Begin Source File

SOURCE=..\src\CasseBrique.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Charger.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Constante.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Curseur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\EditeurTableau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementGraphique.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementOption.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionBool.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionInt.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionString.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ElementTableau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBreche.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBrique.cpp
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBriqueSouffle.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Generateur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Gestionnaire.cpp
# End Source File
# Begin Source File

SOURCE=..\src\I18n.cpp
# End Source File
# Begin Source File

SOURCE=..\src\main.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MenuBase.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MenuEditeur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MenuJeu.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MenuOption.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MenuPrincipal.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Action.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Afficheur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Animation.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Armature.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Bibliotheque.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Bouton.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Camera.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Choix.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ChoixNombre.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Console.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Constante.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Direct.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementArbre.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementFacade.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementPanneau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_EnsembleObjet.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Facade.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Fenetre.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_FenetreKit.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenerateurParticule.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenPartFeu.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenPartFume.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GestionnaireObjet.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GestionnaireTexture.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GroupeAnimation.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Image.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ImageFacade.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Interface.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlare.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareFond.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareNormal.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareSoleil.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Lumiere.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_MatriceTransformation.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Noeud.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_NoeudOpenGL.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Objet.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Omni.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Panneau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Particule.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Peau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Police.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Polygone.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_SaisieChaine.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Separateur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Spot.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Strip.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Temporisation.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Temps.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Texte.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Texture.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Triangle.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_TriangleOmbre.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_TrianglePeau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Univers.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Option.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PanneauOutilsEditeur.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Plateau.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Sauvegarder.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Son.cpp
# End Source File
# Begin Source File

SOURCE=..\src\TableauBase.cpp
# End Source File
# Begin Source File

SOURCE=..\src\TableauJeu.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Texture.cpp
# End Source File
# Begin Source File

SOURCE=..\src\TraceBalle.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\src\Balle.h
# End Source File
# Begin Source File

SOURCE=..\src\BibliothequeSon.h
# End Source File
# Begin Source File

SOURCE=..\src\Bonus.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusDoubleBalle.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusFast.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusGros.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusGrosPlateau.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusPetit.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusPetitPlateau.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusSlow.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusSuperPuissance.h
# End Source File
# Begin Source File

SOURCE=..\src\BonusVueSubjective.h
# End Source File
# Begin Source File

SOURCE=..\src\Brique.h
# End Source File
# Begin Source File

SOURCE=..\src\BriqueBase.h
# End Source File
# Begin Source File

SOURCE=..\src\BriqueDeuxCoups.h
# End Source File
# Begin Source File

SOURCE=..\src\BriqueIncassable.h
# End Source File
# Begin Source File

SOURCE=..\src\BriquePetite.h
# End Source File
# Begin Source File

SOURCE=..\src\Camera.h
# End Source File
# Begin Source File

SOURCE=..\src\CasseBrique.h
# End Source File
# Begin Source File

SOURCE=..\src\Charger.h
# End Source File
# Begin Source File

SOURCE=..\src\Constante.h
# End Source File
# Begin Source File

SOURCE=..\src\Curseur.h
# End Source File
# Begin Source File

SOURCE=..\src\EditeurTableau.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementGraphique.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementOption.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionBool.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionInt.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementOptionString.h
# End Source File
# Begin Source File

SOURCE=..\src\ElementTableau.h
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBreche.h
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBrique.h
# End Source File
# Begin Source File

SOURCE=..\src\ExplosionBriqueSouffle.h
# End Source File
# Begin Source File

SOURCE=..\src\Generateur.h
# End Source File
# Begin Source File

SOURCE=..\src\Gestionnaire.h
# End Source File
# Begin Source File

SOURCE=..\src\I18n.h
# End Source File
# Begin Source File

SOURCE=..\src\MenuBase.h
# End Source File
# Begin Source File

SOURCE=..\src\MenuEditeur.h
# End Source File
# Begin Source File

SOURCE=..\src\MenuJeu.h
# End Source File
# Begin Source File

SOURCE=..\src\MenuOption.h
# End Source File
# Begin Source File

SOURCE=..\src\MenuPrincipal.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Action.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Afficheur.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Animation.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Armature.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Bibliotheque.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Bouton.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Camera.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Choix.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ChoixNombre.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Console.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Constante.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Direct.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementArbre.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementFacade.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ElementPanneau.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_EnsembleObjet.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Facade.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Fenetre.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_FenetreKit.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenerateurParticule.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenPartFeu.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GenPartFume.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GestionnaireObjet.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GestionnaireTexture.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_GroupeAnimation.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Image.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_ImageFacade.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Interface.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlare.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareFond.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareNormal.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_LensFlareSoleil.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Lumiere.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_MatriceTransformation.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Noeud.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_NoeudOpenGL.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Objet.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Omni.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Panneau.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Particule.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Peau.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Police.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Polygone.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_SaisieChaine.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Separateur.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Signal.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_SignalBase.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Spot.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Strip.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Structure.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Temporisation.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Temps.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Texte.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Texture.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Triangle.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_TriangleOmbre.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_TrianglePeau.h
# End Source File
# Begin Source File

SOURCE=..\src\MOGL\MOGL_Univers.h
# End Source File
# Begin Source File

SOURCE=..\src\Option.h
# End Source File
# Begin Source File

SOURCE=..\src\PanneauOutilsEditeur.h
# End Source File
# Begin Source File

SOURCE=..\src\Plateau.h
# End Source File
# Begin Source File

SOURCE=..\src\Sauvegarder.h
# End Source File
# Begin Source File

SOURCE=..\src\Son.h
# End Source File
# Begin Source File

SOURCE=..\src\TableauBase.h
# End Source File
# Begin Source File

SOURCE=..\src\TableauJeu.h
# End Source File
# Begin Source File

SOURCE=..\src\Texture.h
# End Source File
# Begin Source File

SOURCE=..\src\TraceBalle.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
